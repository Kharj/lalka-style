#pragma once


class DXClass{
public:
	IDirect3D9 *pD3D;
	IDirect3DDevice9 *d3ddev;
	ID3DXFont * m_font;
	D3DPRESENT_PARAMETERS d3dpp;
//	RectGraphicsClass graphics;
	static const int fontSize = 18;
//	TexCubeGraphicsClass graphics2;
	MeshSystemClass meshSystem;
	SettingsClass *lpSettings;
	bool MultiSamplingSupport[17];
	bool deviceLost;
	HWND ourHwnd;
	D3DXVECTOR2 mousePos;
	RECT windowRect;
	void SetWindowRect(RECT& a, int w, int h);
	void SetMouseLocation(int x,int y);
	void ProcessKeyboard(const unsigned char keyboardState[256]);
	void Dispose();
	bool Init(HWND hw, SettingsClass *settings);
	void RenderStates();
	void Draw(void);
	void Update();
	void init_light(IDirect3DDevice9 * d3ddev);
};