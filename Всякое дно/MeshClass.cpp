#include "StdAfx.h"
#include "MeshClass.h"

MeshClass::MeshClass(void)
{
	Name="";
	loadingStat.Mesh=false; loadingStat.texturesCount=0; loadingStat.texturesLoaded=0;
	numMaterials=0;
	meshMaterials=NULL;
	meshTextures= NULL;
	materialBuffer=NULL;
	mesh=NULL;
	//prop=DefaultVector3x3;
	//defaultProp=DefaultVector3x3;
}

MeshClass::~MeshClass(void)
{
	Release();
}
bool MeshClass::Draw(LPDIRECT3DDEVICE9 d3ddev){
	HRESULT hr=0;
	D3DXMATRIX matRotateY, matRotateX, matRotateZ, matTranslate, matScale;

	for (DWORD j=0; j<numMaterials; j++)
	{

		D3DXMatrixScaling(&matScale, defaultProp.Scale.x * prop.Scale.x, defaultProp.Scale.y * prop.Scale.y, defaultProp.Scale.z * prop.Scale.z);
		D3DXMatrixRotationX(&matRotateX, defaultProp.Rotation.x + prop.Rotation.x);
		D3DXMatrixRotationY(&matRotateY, defaultProp.Rotation.y + prop.Rotation.y);
		D3DXMatrixRotationZ(&matRotateZ, defaultProp.Rotation.z + prop.Rotation.z);
		D3DXMatrixTranslation(&matTranslate, defaultProp.Position.x + prop.Position.x, defaultProp.Position.y + prop.Position.y, defaultProp.Position.z + prop.Position.z);
		// Set the material and texture for this subset
		if(&meshMaterials[j]){
			hr = d3ddev->SetMaterial(&meshMaterials[j]);
		}
		if(meshTextures[j]){
			hr = d3ddev->SetTexture(0,meshTextures[j]);
		}else{
			d3ddev->SetTexture(0,NULL);
		}
		d3ddev->SetTransform(D3DTS_WORLD, &(matScale * matRotateX * matRotateY * matRotateZ * matTranslate));
		hr = mesh->DrawSubset(j);// Draw the mesh subset
		d3ddev->SetTexture(0,NULL);

	}
	return SUCCEEDED(hr);
}
bool MeshClass::LoadFromMesh(LPD3DXMESH m, LPDIRECT3DDEVICE9 d3ddev){

	loadingStat.Mesh=false; loadingStat.texturesCount=0; loadingStat.texturesLoaded=0;
	Release();
	HRESULT hr = D3DXCreateMeshFVF(0,0,D3DPOOL_MANAGED,m->GetFVF(),d3ddev, &mesh);
	hr = m->CloneMeshFVF(D3DPOOL_MANAGED,m->GetFVF(),d3ddev,&mesh);
	if(FAILED(hr)){
		return false;
	}
	loadingStat.Mesh=true;
	numMaterials=0;
	meshMaterials = NULL;
	meshTextures  = NULL;
	return true;
}
bool MeshClass::LoadMaterials(int num, D3DMATERIAL9 * mi){
	if(meshMaterials){//release materials
		delete [] meshMaterials;
		meshMaterials=NULL;
	}
	if(meshTextures){//release textures
		for(DWORD i=0;i<(int)numMaterials;++i)
			if(meshTextures[i]) meshTextures[i]->Release();
		delete [] meshTextures;
		meshTextures= NULL;
	}
	if(num==0 || num<0) return false;
	numMaterials=num;
	loadingStat.texturesCount=numMaterials; loadingStat.texturesLoaded=0;
	meshMaterials = new D3DMATERIAL9 [numMaterials];
	meshTextures  = new LPDIRECT3DTEXTURE9[numMaterials];
	for(DWORD i=0;i<numMaterials;++i){
		meshTextures[i]=NULL;
		meshMaterials[i]=mi[i];
		meshMaterials[i].Ambient = meshMaterials[i].Diffuse;

		//loadingStat.texturesLoaded++;
	}
	return true;

}
bool MeshClass::LoadFromX(LPCWSTR filename, LPDIRECT3DDEVICE9 d3ddev){
	loadingStat.Mesh=false; loadingStat.texturesCount=0; loadingStat.texturesLoaded=0;
	Release();
	HRESULT hr = D3DXLoadMeshFromX(filename, D3DXMESH_SYSTEMMEM, 
		d3ddev, NULL, 
		&materialBuffer,NULL, &numMaterials, 
		&mesh );
	if(FAILED(hr)){
		Log(filename,L" not loaded");
		return false;
	}
	loadingStat.Mesh=true;
	D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)materialBuffer->GetBufferPointer();
	meshMaterials = new D3DMATERIAL9[numMaterials];
	meshTextures  = new LPDIRECT3DTEXTURE9[numMaterials];
	loadingStat.texturesCount=numMaterials;
	for (DWORD i=0; i<numMaterials; i++){
		meshMaterials[i] = d3dxMaterials[i].MatD3D;
		meshMaterials[i].Ambient = meshMaterials[i].Diffuse;
		meshTextures[i] = NULL;
		if (d3dxMaterials[i].pTextureFilename){
			hr = D3DXCreateTextureFromFileA(d3ddev, d3dxMaterials[i].pTextureFilename, &meshTextures[i]); 
			if(hr!=D3D_OK){
				Log(d3dxMaterials[i].pTextureFilename," not loaded, using default");//log
				meshTextures[i]=NULL;
				hr = D3DXCreateTextureFromFileInMemory(d3ddev, (LPCVOID)Default_Texture_Mas,(UINT)Default_Texture_Mas_Size, &meshTextures[i]);
			}
			loadingStat.texturesLoaded++;

		}
	}
	//meshMaterials->Release();
	return true;
}
void MeshClass::Release(){
	if(meshMaterials){
		delete [] meshMaterials;
		meshMaterials=NULL;
	}
	if(meshTextures){
		for(DWORD i=0;i<(int)numMaterials;++i)
			if(meshTextures[i]) meshTextures[i]->Release();
		delete [] meshTextures;
		meshTextures= NULL;
	}
	if(mesh){
		mesh->Release();
		mesh=NULL;
	}
	if(materialBuffer){
		materialBuffer->Release();
		materialBuffer=NULL;
	}
	numMaterials=0;
}

#pragma region Properties_methods
void MeshClass::SetDefaultProps(const VECTOR3X3 &a){
	defaultProp=a;
}

void MeshClass::SetDefaultProps(D3DXVECTOR3 p, D3DXVECTOR3 r, D3DXVECTOR3 s){
	defaultProp.Position=p;
	defaultProp.Rotation=r;
	defaultProp.Scale=s;
}
void MeshClass::SetDefaultProps(float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z, float sc_x, float sc_y, float sc_z){
	defaultProp.Position.x=pos_x; defaultProp.Position.y=pos_y; defaultProp.Position.z=pos_z; 
	defaultProp.Rotation.x=rot_x; defaultProp.Rotation.y=rot_y; defaultProp.Rotation.z=rot_z; 
	defaultProp.Scale.x=sc_x; defaultProp.Scale.y=sc_y; defaultProp.Scale.z=sc_z; 

}
void MeshClass::SetProps(const VECTOR3X3 &a){
	prop=a;
}

void MeshClass::SetProps(D3DXVECTOR3 p, D3DXVECTOR3 r, D3DXVECTOR3 s){
	prop.Position=p;
	prop.Rotation=r;
	prop.Scale=s;
}
void MeshClass::SetProps(float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z, float sc_x, float sc_y, float sc_z){
	prop.Position.x=pos_x; prop.Position.y=pos_y; prop.Position.z=pos_z; 
	prop.Rotation.x=rot_x; prop.Rotation.y=rot_y; prop.Rotation.z=rot_z; 
	prop.Scale.x=sc_x; prop.Scale.y=sc_y; prop.Scale.z=sc_z; 

}
void MeshClass::SetPosition(D3DXVECTOR3 a){
	prop.Position=a;
}
void MeshClass::SetPosition(float x, float y, float z){
	prop.Position=D3DXVECTOR3(x,y,z);
}
void MeshClass::SetRotation(D3DXVECTOR3 a){
	prop.Rotation=a;
}
void MeshClass::SetRotation(float x, float y, float z){
	prop.Rotation=D3DXVECTOR3(x,y,z);
}
void MeshClass::SetScale(D3DXVECTOR3 a){
	prop.Scale=a;
}
void MeshClass::SetScale(float x, float y, float z){
	prop.Scale=D3DXVECTOR3(x,y,z);
}
#pragma endregion 


void MeshClass::SetName(string a){
	Name=a;
}
string MeshClass::GetName(){
	return Name;
}