#pragma once

class MeshSystemClass
{
	vector <MeshClass*> Meshes;
	SettingsClass *settings;
	bool Inited;
	//VECTOR3X3 worldProp;
	D3DXVECTOR3 mouseRotation;
	CameraClass camera;
public:
		
	MeshClass * MeshByName(string a);
	virtual bool Init(IDirect3DDevice9 * d3ddev, SettingsClass *_settings);
	bool Draw(IDirect3DDevice9 * d3ddev);
	void Reset(IDirect3DDevice9 * d3ddev, SettingsClass *_settings);
	void ControlInput(float mouse_x, float mouse_y, const unsigned char keyboardState[256]);
	void Walk(D3DXVECTOR3 walkVector, float step);
	virtual void Dispose();
	virtual void Update(IDirect3DDevice9 * d3ddev);
	MeshSystemClass(void);
	~MeshSystemClass(void);
};
