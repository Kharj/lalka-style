#include "StdAfx.h"
#include "SettingsClass.h"


SettingsClass::SettingsClass(void)
{
	Windowed=TRUE;
	Height=480;
	Width=640;
	MultiSamplingType=0;
	TextureFilter=-2;
	MouseScaleX=1.0f;
	MouseScaleY=1.0f;
	TextureMinFilter=D3DTEXF_LINEAR;
	TextureMaxAnisotropy=1;
	ZeroMemory(&MultiSamplingSupport,sizeof(MultiSamplingSupport));
	MultiSamplingSupport[0]=true;
	showSettings=false;
	//res
	static const Resolution ResList[ALL_RESOLUTIONS_COUNT]={640,480,false,false,800,480,false,false,800,600,false,false,1024,576,false,false,1024,600,false,false,1024,768,false,false,1152,864,false,false,1280,720,false,false,1280,768,false,false,1360,768,false,false,1366,768,false,false,1280,1024,false,false,1440,900,false,false,1600,1200,false,false,1680,1050,false,false,1920,1080,false,false,1920,1200,false,false};
	memcpy(Resolutions,ResList,sizeof(ResList));
}
SettingsClass::~SettingsClass(void)
{
}
SettingsClass SettingsClass::self;
bool SettingsClass::GetFromFile(){
	FILE* fp;
	fopen_s(&fp,SETTINGS_FILENAME,"r");
	if(!fp) return false;
	int win,h,w,mt,tf,sx,sy;
	if(7 != fscanf_s(fp,"Windowed %d\nHeight %d\nWidth %d\nMultiSamplingType %d\nTextureFilter %d\nMouseScaleX %d\nMouseScaleY %d\nEnd",&win,&h,&w,&mt,&tf,&sx,&sy)){
		return false;
	}
	if(win)
		Windowed=TRUE;
	else
		Windowed=FALSE;
	if(h>0)
		Height=h;
	if(w>0)
		Width=w;
	if(mt>1)
		MultiSamplingType=mt;
	if(tf>-3 && tf<17){
		switch(TextureFilter){
			case 0:
				TextureMinFilter = D3DTEXF_NONE; break;
			case -1:
				TextureMinFilter = D3DTEXF_POINT; break;
			case -2:
				TextureMinFilter = D3DTEXF_LINEAR; break;
			case 1: case 2: case 4: case 8: case 16: 
				TextureMinFilter = D3DTEXF_ANISOTROPIC; 
				TextureMaxAnisotropy = TextureFilter; break;
		}
		TextureFilter=tf;
		TextureMinFilter=D3DTEXF_NONE;
		TextureMaxAnisotropy=1;
	}
	if(sx>0 && sx<1001 && sy>0 && sy<1001){
		MouseScaleX=(float)sx/100.0f;
		MouseScaleY=(float)sy/100.0f;
	}
	fclose(fp);
	return true;
}
bool SettingsClass::SaveToFile(){
	FILE* fp;
	fopen_s(&fp,SETTINGS_FILENAME,"w");
	if(!fp) return false;
	int win;
	if(Windowed)
		win=1;
	else
		win=0;
	int sx=100,sy=100;
	if(MouseScaleX>0.01f && MouseScaleX<10.1f && MouseScaleY>0.01f && MouseScaleY<10.1f){
		sx=(MouseScaleX<1.01f && MouseScaleX>0.99f)?100:(int)(MouseScaleX*100.0f+0.51f);
		sy=(MouseScaleY<1.01f && MouseScaleY>0.99f)?100:(int)(MouseScaleY*100.0f+0.51f);

	}
	fprintf(fp,"Windowed %d\nHeight %d\nWidth %d\nMultiSamplingType %d\nTextureFilter %d\nMouseScaleX %d\nMouseScaleY %d\nEnd",win,Height,Width,MultiSamplingType,TextureFilter, sx, sy);
	fclose(fp);
	return true;
}
void SettingsClass::SetDefault(){
	Windowed=TRUE;
	Height=480;
	Width=640;
	MultiSamplingType=0;
	TextureFilter=-2;
	TextureMinFilter=D3DTEXF_LINEAR;
	TextureMaxAnisotropy=1;
	MouseScaleX=1.0f;;
	MouseScaleY=1.0f;
};
bool SettingsClass::operator != (const SettingsClass &a){
	return !(a.Height==Height && a.Width==Width && a.MultiSamplingType==MultiSamplingType && a.Windowed==Windowed && a.MouseScaleX==MouseScaleX && a.MouseScaleY==MouseScaleY);
}
bool SettingsClass::ShowSettingsBox(HWND hw){
	self=*this;
	DialogBox(NULL, MAKEINTRESOURCE(IDD_ABOUTBOX), hw, About);
	if(!self.Ignore && self!=*this){
		this->Height=self.Height;
		this->Width=self.Width;
		this->MultiSamplingType=self.MultiSamplingType;
		this->Windowed=self.Windowed;
		this->MouseScaleX=self.MouseScaleX;
		this->MouseScaleY=self.MouseScaleY;
		SaveToFile();
		return true;
	}
	return false;
}
INT_PTR CALLBACK SettingsClass::About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND hCombo1 = GetDlgItem(hDlg, IDC_COMBO1);
	HWND hCombo2 = GetDlgItem(hDlg, IDC_COMBO2);
	HWND hRadio1 = GetDlgItem(hDlg, IDC_RADIO1);
	HWND hRadio2 = GetDlgItem(hDlg, IDC_RADIO2);
	HWND hSlider1 = GetDlgItem(hDlg, 1006);
	HWND hSlider2 = GetDlgItem(hDlg, 1007);
	bool foundresolution=false;
	int hsx=100, hsy=100;
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:

		if(self.Windowed){
			SendMessage(hRadio2,BM_SETCHECK,(WPARAM)BST_CHECKED,0);//W
		}else{
			SendMessage(hRadio1,BM_SETCHECK,(WPARAM)BST_CHECKED,0);//FS
		}
		for(int i=0;i<ALL_RESOLUTIONS_COUNT;++i){
			if((self.Windowed && self.Resolutions[i].windowed) || (!self.Windowed && self.Resolutions[i].fullscreen)){
				//��������
				char t1[15];
				char t2[6];
				_itoa_s(self.Resolutions[i].Width,t1,10);					
				_itoa_s(self.Resolutions[i].Height,t2,10);
				strcat_s(t1,sizeof(t1),"x");
				strcat_s(t1,sizeof(t1),t2);
				SendMessageA(hCombo1, CB_ADDSTRING, 0, (LPARAM) t1);
			}
		}
		//Select current res to combo
		foundresolution=false;
		for(int i=0,j=0;i<ALL_RESOLUTIONS_COUNT;++i){
			if((self.Windowed && self.Resolutions[i].windowed) || (!self.Windowed && self.Resolutions[i].fullscreen)){
				if(self.Resolutions[i].Width==self.Width && self.Resolutions[i].Height==self.Height){//������� j-�� � ������
					SendMessage (hCombo1, CB_SETCURSEL, (WPARAM)j, 0);
					foundresolution=true;
					break;
				}
				j++;
			}

		}
		if(!foundresolution){
			SendMessage (hCombo1, CB_SETCURSEL, 0, 0); 
			self.Width=640;
			self.Height=480;
		}
		//load aa types
		SendMessageA(hCombo2, CB_ADDSTRING, 0, (LPARAM) "OFF");
		for(int i=2;i<MULTISAMPLING_TYPES_COUNT;++i){
			if(!self.MultiSamplingSupport[i]) continue;
			char t1[10]="x";
			char t2[5];
			_itoa_s(i,t2,10);
			strcat_s(t1,sizeof(t1),t2);			
			SendMessageA(hCombo2, CB_ADDSTRING, 0, (LPARAM) t1);
		}
		if(self.MultiSamplingType && self.MultiSamplingSupport[self.MultiSamplingType]){//���� �������� �������������� � �� 0
			for(int i=2,j=0;i<MULTISAMPLING_TYPES_COUNT;++i){
				if(self.MultiSamplingSupport[i]) j++;
				if(i==self.MultiSamplingType){//������� j-�� � ������
					SendMessage (hCombo2, CB_SETCURSEL, (WPARAM)j, 0);
					break;
				}
			}
		}else{
			SendMessage (hCombo2, CB_SETCURSEL, (WPARAM)0, 0);
			self.MultiSamplingType=0;
		}
		//set sliders
		hsx=((self.MouseScaleX<1.01f && self.MouseScaleX>0.99f) || self.MouseScaleX<0.1f || self.MouseScaleX>1.9f)?100:(int)(self.MouseScaleX*100.0f+0.51f);
		hsy=((self.MouseScaleY<1.01f && self.MouseScaleY>0.99f) || self.MouseScaleY<0.1f || self.MouseScaleY>1.9f)?100:(int)(self.MouseScaleY*100.0f+0.51f);
		SendMessage(hSlider1, 1030, (WPARAM) TRUE, (LPARAM) MAKELONG(10, 190));  // min. & max. positions
		SendMessage(hSlider1, 0x0405,(WPARAM) TRUE,(LPARAM) hsx); 
		SendMessage(hSlider2, 1030, (WPARAM) TRUE, (LPARAM) MAKELONG(10, 190));  // min. & max. positions
		SendMessage(hSlider2, 0x0405,(WPARAM) TRUE,(LPARAM) hsy); 
		return (INT_PTR)TRUE;
	case WM_HSCROLL:
			if (hSlider1 == (HWND)lParam){
				int newPos    = SendMessage(hSlider1, 1024, 0, 0);
				if(newPos<191 && newPos>9) self.MouseScaleX=((float)newPos+0.001f)/100.0f;
				wchar_t t1[15]=L"��� X (";
			wchar_t t2[5];
			_itow_s(newPos,t2,5,10);
			wcscat_s(t1,15,t2);	
			wcscat_s(t1,15,L")");	
				SetDlgItemText(hDlg,1009,t1);
			}else if (hSlider2 == (HWND)lParam){
				int newPos    = SendMessage(hSlider2, 1024, 0, 0);
				if(newPos<191 && newPos>9) self.MouseScaleY=((float)newPos+0.001f)/100.0f;
				wchar_t t1[15]=L"��� Y (";
			wchar_t t2[5];
			_itow_s(newPos,t2,5,10);
			wcscat_s(t1,15,t2);	
			wcscat_s(t1,15,L")");	
				SetDlgItemText(hDlg,1008,t1);
			}
			return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDCANCEL){//cancel
			//index_of_selection = SendMessage (combo_box_handle, CB_GETCURSEL, 0, 0);
			self.Ignore=true;//not save
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}else if (LOWORD(wParam) == IDOK){//ok
			self.Ignore=false;//save
			//SAVE
			//index_of_selection = SendMessage (combo_box_handle, CB_GETCURSEL, 0, 0);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}else if((LOWORD(wParam) == IDC_RADIO1 || LOWORD(wParam) == IDC_RADIO2) && HIWORD(wParam)==BN_CLICKED){//radio 1 (fullscreen)
			if(LOWORD(wParam) == IDC_RADIO1){
				self.Windowed=false;
			}else{
				self.Windowed=true;
			}
			SendMessage (hCombo1, CB_RESETCONTENT, 0, 0);//������� ����������
			for(int i=0;i<ALL_RESOLUTIONS_COUNT;++i){
				if((self.Windowed && self.Resolutions[i].windowed) || (!self.Windowed && self.Resolutions[i].fullscreen)){
					//��������
					char t1[15];
					char t2[6];
					_itoa_s(self.Resolutions[i].Width,t1,10);					
					_itoa_s(self.Resolutions[i].Height,t2,10);
					strcat_s(t1,sizeof(t1),"x");
					strcat_s(t1,sizeof(t1),t2);
					SendMessageA(hCombo1, CB_ADDSTRING, 0, (LPARAM) t1);
					self.Width=640;
					self.Height=480;
				}
			}
			SendMessage (hCombo1, CB_SETCURSEL, (WPARAM)0, 0);
		}else if(LOWORD(wParam)==IDC_COMBO1 && HIWORD(wParam)==CBN_SELENDOK){//���������� �������
			LRESULT lr = SendMessage(hCombo1, CB_GETCURSEL, 0, 0);
			if(lr!=CB_ERR){
				for(int i=0,j=0;i<ALL_RESOLUTIONS_COUNT;++i){
					if((self.Windowed && self.Resolutions[i].windowed) || (!self.Windowed && self.Resolutions[i].fullscreen)){
						if(j==(int)lr){//selected
							self.Width=self.Resolutions[i].Width;
							self.Height=self.Resolutions[i].Height;
							break;
						}
						j++;
					}
				}					
			}
		}else if(LOWORD(wParam)==IDC_COMBO2 && HIWORD(wParam)==CBN_SELENDOK){//�������������� �������
			LRESULT lr = SendMessage(hCombo2, CB_GETCURSEL, 0, 0);
			if(lr!=CB_ERR){
				for(int i=0,j=0;i<MULTISAMPLING_TYPES_COUNT;++i){
					if(self.MultiSamplingSupport[i]){
						if(j==(int)lr){//selected
							self.MultiSamplingType=i;
							break;
						}
						j++;
					}
				}					
			}
		}

		break;
	}
	return (INT_PTR)FALSE;
}