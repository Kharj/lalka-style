
#include "stdafx.h"
#include "DirectX9_YO.h"

#define IDC_TIMER0 101
#define IDC_TIMER1 102
#define UPDATE_FPS 120
#define MAX_LOADSTRING 100
// ���������� ����������:

#pragma region deriv
class DMeshSystemClass : public MeshSystemClass{
	float boxDelta;
public:
	virtual void ControlInput(float mouse_x, float mouse_y, const unsigned char keyboardState[256]){
		//D3DXVECTOR3 walkVector = D3DXVECTOR3(0.0f,0.0f,0.0f);
		const float LookAtMultiply=0.5f;
		bool w,a,s,d;
		w=(keyboardState[DIK_W]&0x80)!=0;
		a=(keyboardState[DIK_A]&0x80)!=0;
		s=(keyboardState[DIK_S]&0x80)!=0;
		d=(keyboardState[DIK_D]&0x80)!=0;

		mouseRotation.y=mouse_x*settings->MouseScaleX;
		mouseRotation.x=mouse_y*settings->MouseScaleY;
		mouseRotation.z=0;

		if(w^s || a^d) {
			if(boxDelta<2.5f) boxDelta+=0.06f;
			else if(boxDelta<3.0f) boxDelta+=0.02f;
		}
		camera.SetRotation( - LookAtMultiply * mouseRotation.x, - LookAtMultiply * mouseRotation.y, 0.0f);
	}


	DMeshSystemClass(){
		Inited=false;
		mouseRotation=D3DXVECTOR3(0.0f,0.0f,0.0f);
		boxDelta=0.0f;
	}
	virtual void Update(IDirect3DDevice9 * d3ddev){		
		if(boxDelta>0.001 && boxDelta<2.5f) boxDelta-=0.04f;
		if(boxDelta>2.5f) boxDelta-=0.01f;
		postTransform.Rotation.x+=0.1f*settings->MouseScaleX;
		postTransform.Rotation.y+=0.1f*settings->MouseScaleY;

		MeshClass * tmpmesh;
		tmpmesh = MeshByName("Box0");
		if(tmpmesh){//������
			tmpmesh->SetPosition(0.0f,0.0f,-boxDelta);
		}
		tmpmesh = MeshByName("Box1");
		if(tmpmesh){//������
			tmpmesh->SetPosition(0.0f,0.0f,boxDelta);
		}
		tmpmesh = MeshByName("Box2");
		if(tmpmesh){//������
			tmpmesh->SetPosition(boxDelta,0.0f,0.0f);
		}
		tmpmesh = MeshByName("Box3");
		if(tmpmesh){//������
			tmpmesh->SetPosition(-boxDelta,0.0f,0.0f);
		}
		tmpmesh = MeshByName("Box4");
		if(tmpmesh){//������
			tmpmesh->SetPosition(0.0f,boxDelta,0.0f);
		}
		tmpmesh = MeshByName("Box5");
		if(tmpmesh){//������
			tmpmesh->SetPosition(0.0f,-boxDelta,0.0f);
		}
		//
		tmpmesh=NULL;
	}
	virtual bool Init(IDirect3DDevice9 * d3ddev, SettingsClass *_settings){
		HRESULT hr = 0;

		//worldProp=DefaultVector3x3;
		settings=_settings;
		camera.SetProjection(D3DXToRadian(45),((FLOAT)settings->Width/(FLOAT)settings->Height),1.0f,100.0f);
		camera.SetUpDirection(D3DXVECTOR3(0.0f,1.0f,0.0f));
		camera.SetEyePosition(D3DXVECTOR3(0.0f,5.0f,-7.0f));
		MeshClass *tmpmesh=new MeshClass;

		//2


		//	
		D3DMATERIAL9 tmpmat[1];
		ZeroMemory(&tmpmat[0],sizeof(tmpmat[0]));
		tmpmat[0].Diffuse=D3DXCOLOR(0.9f,1.0f,0.0f,1.0f);
		//
		const float boxSize=2.0f;
		for(int i=0;i<6;i++){
			tmpmesh=new MeshClass;
			LPD3DXMESH tmpd3dmesh;
			hr = D3DXCreateBox(d3ddev,boxSize, boxSize,0.02f, &tmpd3dmesh,NULL);
			if(FAILED(hr)) {
				return false;
			}
			tmpmesh->LoadFromMesh(tmpd3dmesh,d3ddev);
			tmpd3dmesh->Release();
			if(tmpmesh->loadingStat.Mesh){
				if(i==5) tmpmat[0].Diffuse.b=1.0f;
				tmpmesh->LoadMaterials(1, tmpmat);
				Meshes.push_back(tmpmesh);
			}
		}

		Meshes[0]->SetName("Box0");
		Meshes[0]->SetDefaultProps(0.0f,0.0f,0.0f-(boxSize/2.0f),0.0f,0.0f,0.0f,1.0f,1.0f,1.0f);
		Meshes[1]->SetName("Box1");
		Meshes[1]->SetDefaultProps(0.0f,0.0f,boxSize-(boxSize/2.0f),0.0f,0.0f,D3DXToRadian(180),1.0f,1.0f,1.0f);
		Meshes[2]->SetName("Box2");
		Meshes[2]->SetDefaultProps(boxSize/2.0f,0.0f,boxSize/2.0f-(boxSize/2.0f),0.0f,D3DXToRadian(90),0.0f,1.0f,1.0f,1.0f);
		Meshes[3]->SetName("Box3");
		Meshes[3]->SetDefaultProps(-boxSize/2.0f,0.0f,boxSize/2.0f-(boxSize/2.0f),0.0f,D3DXToRadian(-90),0.0f,1.0f,1.0f,1.0f);
		Meshes[4]->SetName("Box4");
		Meshes[4]->SetDefaultProps(0.0f,boxSize/2.0f,boxSize/2.0f-(boxSize/2.0f),D3DXToRadian(90),0.0f,0.0f,1.0f,1.0f,1.0f);
		Meshes[5]->SetName("Box5");
		Meshes[5]->SetDefaultProps(0.0f,-boxSize/2.0f,boxSize/2.0f-(boxSize/2.0f),D3DXToRadian(-90),0.0f,0.0f,1.0f,1.0f,1.0f);

		//3

		tmpmesh=NULL;

		Inited=true;
		return true;
	}
};
class DDXClass : public DXClass{
protected:
	DMeshSystemClass DmeshSystem;
public:
	void MeshSystemDraw(){
		DmeshSystem.Draw(d3ddev);
	}
	void MeshSystemInit(){
		DmeshSystem.Init(d3ddev, lpSettings);
	}
	void MeshSystemReset(){
		DmeshSystem.Reset(d3ddev,lpSettings);
	}
	void MeshSystemDispose(){
		DmeshSystem.Dispose();
	}
	void MeshSystemUpdate(){
		DmeshSystem.Update(d3ddev);
	}
	void MeshSystemControlInput(float x, float y, const unsigned char keyboardState[256]){
		DmeshSystem.ControlInput(x,y,keyboardState);
	}
};
#pragma endregion //������������ ������� ��� ����� ���������
HINSTANCE hInst;								// ������� ���������


SettingsClass mainSettings;// Settings
InputClass dInput;// Direct Input
DDXClass gDX;// Direct 3D
SoundClass dSound;// Direct Sound

TCHAR szTitle[MAX_LOADSTRING];					// ����� ������ ���������
TCHAR szWindowClass[MAX_LOADSTRING];			// ��� ������ �������� ����
HWND hWnd;
// ��������� ���������� �������, ���������� � ���� ������ ����:
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void __stdcall timerFunc (HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);


int APIENTRY _tWinMain(HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPTSTR    lpCmdLine,
					   int       nCmdShow)
{

	if(!mainSettings.GetFromFile())
		mainSettings.SaveToFile();
	if(wcscmp(lpCmdLine,L"-set")==0) mainSettings.showSettings=true;
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	// ������������� ���������� �����
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_DIRECTX9_YO, szWindowClass, MAX_LOADSTRING);
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= DefWindowProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDC_DIRECTX9_YO));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_DIRECTX9_YO);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassEx(&wcex);
	// ��������� ������������� ����������:
	hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������
	RECT wr = {0, 0, mainSettings.Width, mainSettings.Height};    // set the size, but not the position
	AdjustWindowRect(&wr, MY_WS, FALSE);    // adjust the size

	hWnd = CreateWindow(szWindowClass, szTitle, MY_WS,0, 0,  wr.right - wr.left,  wr.bottom - wr.top, NULL, NULL, hInstance, NULL);
	if (!hWnd)
	{
		MessageBox(NULL,L"Error Create Window",L"DX Error",MB_OK);
		return 1;
	}

	if(!gDX.Init(hWnd,&mainSettings)){// CREATE gDX
		MessageBox(hWnd,L"Error Init Direct3D",L"DX Error",MB_OK);
		return 2;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	if(!dInput.Initialize(hInstance,hWnd,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN))){// CREATE gDX
		MessageBox(hWnd,L"Error Init DirectInput",L"DX Error",MB_OK);
		//return 3;
	}
	if(!dSound.Initialize(hWnd)){// CREATE gDX
		MessageBox(hWnd,L"Error Init Direct Sound",L"DX Error",MB_OK);
		//return 4;
	}
	SetTimer(hWnd,             // handle to main window 
		IDC_TIMER0,            // timer identifier 
		1000/UPDATE_FPS,                 // interval 
		timerFunc);     // no timer callback 
	SetTimer(hWnd,             // handle to main window  // SLOW_TIMER
		IDC_TIMER1,            // timer identifier 
		2000,                 // interval 
		timerFunc);     // no timer callback 
	timerFunc(NULL,NULL,IDC_TIMER1,NULL);//�������������� ���� ������

	// ���� ��������� ���������:
	for(;;){
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if(msg.message==WM_QUIT || msg.message==WM_NCLBUTTONDOWN || msg.message==WM_CLOSE || (msg.message==WM_SYSCOMMAND && msg.wParam==SC_CLOSE)){
			break;
		}
		gDX.ProcessKeyboard(dInput.m_keyboardState);
		gDX.Update();
		gDX.Draw();
		dInput.Frame();
		if(dInput.IsEscapePressed()){
			break;
		}
		//int mouseX,mouseY;
		//dInput.GetMouseLocation(mouseX,mouseY);
		POINT CursorPos;
		GetCursorPos(&CursorPos);
		gDX.SetMouseLocation(CursorPos.x,CursorPos.y);
	}
	gDX.Dispose();
	dInput.Shutdown();
	dSound.Shutdown();
	DestroyWindow(hWnd);

	return 0;
}

void __stdcall timerFunc (HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
	if(idEvent==IDC_TIMER0){//UPDATE_FPS
		//gDX.ProcessKeyboard(dInput.m_keyboardState);
		//gDX.Update();
	}
	if(idEvent==IDC_TIMER1){//SLOW_TIMER
		//_CrtDumpMemoryLeaks();
		RECT curWindowRect, adjWindowRect;
		GetWindowRect(hWnd,&curWindowRect);//get rect
		adjWindowRect=curWindowRect;
		AdjustWindowRect(&adjWindowRect, MY_WS, FALSE);    // adjust the size
		curWindowRect.top+=(curWindowRect.top-adjWindowRect.top);
		curWindowRect.left+=(curWindowRect.left-adjWindowRect.left);
		gDX.SetWindowRect(curWindowRect, GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN));
		//
	}
}







