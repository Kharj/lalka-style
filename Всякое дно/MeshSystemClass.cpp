#include "StdAfx.h"
#include "MeshSystemClass.h"

MeshSystemClass::MeshSystemClass(void)
{
	Inited=false;
	//worldProp=DefaultVector3x3;
	mouseRotation=D3DXVECTOR3(0.0f,0.0f,0.0f);
}

MeshSystemClass::~MeshSystemClass(void)
{
}
struct AssCube{ 
	D3DXVECTOR3 pos, norm; FLOAT u,v;
	AssCube(D3DXVECTOR3 p, D3DXVECTOR3 n, FLOAT a, FLOAT b){
		pos=p; norm=n; u=a,v=b;
	}
	AssCube(){}
};

bool MeshSystemClass::Init(IDirect3DDevice9 * d3ddev, SettingsClass *_settings){
	HRESULT hr = 0;

	//worldProp=DefaultVector3x3;
	settings=_settings;
	camera.SetProjection(D3DXToRadian(45),((FLOAT)settings->Width/(FLOAT)settings->Height),1.0f,100.0f);
	camera.SetUpDirection(D3DXVECTOR3(0.0f,1.0f,0.0f));
	MeshClass *tmpmesh=new MeshClass;

	tmpmesh->LoadFromX(L"cot_chevy_1.X",d3ddev);
	if(tmpmesh->loadingStat.Mesh){
		tmpmesh->SetName("Car");
		tmpmesh->SetDefaultProps(0.0f, 0.0f, 0.0f, 0.0f,D3DXToRadian(180),0.0f, 1.0f, 1.0f, 1.0f);
		tmpmesh->SetPosition(0.0f,1.0f,0.0f);
		Meshes.push_back(tmpmesh);
	}
	//2
	tmpmesh=new MeshClass;
	LPD3DXMESH tmpd3dmesh;
	D3DXCreateTorus(d3ddev,1.0f,4.0f,12,32,&tmpd3dmesh,NULL);
	tmpmesh->LoadFromMesh(tmpd3dmesh,d3ddev);
	D3DMATERIAL9 tmpmat[1];
	ZeroMemory(&tmpmat[0],sizeof(tmpmat[0]));
	tmpmat[0].Diffuse=D3DXCOLOR(0.9f,1.0f,0.0f,0.9f);
	if(tmpmesh->loadingStat.Mesh){
		tmpmesh->LoadMaterials(1, tmpmat);
		tmpmesh->SetName("Torus");
		tmpmesh->SetPosition(0.0f,-2.0f,4.0f);
		Meshes.push_back(tmpmesh);
	}

	//3

	tmpmesh=NULL;

	Inited=true;
	return true;
}
bool MeshSystemClass::Draw(IDirect3DDevice9 * d3ddev){
	HRESULT hr=0;
	d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);    // turn off the 3D lighting
	//d3ddev->SetLight(0,NULL);
	//d3ddev->LightEnable(0,TRUE);	
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);

	d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1,D3DTA_TEXTURE);
	d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);
	d3ddev->SetTextureStageState(0, D3DTSS_COLOROP,  D3DTOP_SELECTARG1);

	d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
	d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );
	d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP,  D3DTOP_MODULATE);

	d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, settings->TextureMinFilter); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, settings->TextureMaxAnisotropy);


	d3ddev->SetTransform(D3DTS_VIEW, camera.GetTransformedViewMatrix());    // set the view transform to matView
	d3ddev->SetTransform(D3DTS_PROJECTION, camera.GetProjectionMatrix());    // set the projection
	//
	for(int i=0;i<(int)Meshes.size();i++){
		Meshes[i]->Draw(d3ddev);
	}

	return true;
}

void MeshSystemClass::Update(IDirect3DDevice9 * d3ddev){
	if(!Inited) return;
	/*static float index = 0.0f; 
	index+=0.005f;    // an ever-increasing float value

	Rotation.y=index;*/
}


void MeshSystemClass::ControlInput(float mouse_x, float mouse_y, const unsigned char keyboardState[256]){
	D3DXVECTOR3 walkVector = D3DXVECTOR3(0.0f,0.0f,0.0f);
	const float LookAtMultiply=0.5f;
	bool w,a,s,d;
	w=(keyboardState[DIK_W]&0x80)!=0;
	a=(keyboardState[DIK_A]&0x80)!=0;
	s=(keyboardState[DIK_S]&0x80)!=0;
	d=(keyboardState[DIK_D]&0x80)!=0;

	mouseRotation.y=mouse_x*settings->MouseScaleX;
	mouseRotation.x=mouse_y*settings->MouseScaleY;
	mouseRotation.z=0;
	if(w^s){
		walkVector.z=(w)?-1.0f:1.0f;
	}
	if(a^d){
		walkVector.x=(d)?-1.0f:1.0f;
	}
	if(w^s || a^d) Walk(walkVector,0.1f);
	camera.SetRotation( - LookAtMultiply * mouseRotation.x, - LookAtMultiply * mouseRotation.y, 0.0f);

}
void MeshSystemClass::Walk(D3DXVECTOR3 walkVector, float step){
	//LET MESH[0] BE PLAYER
	const float mouseDeadZone = 0.15f;
	MeshClass * tmpmesh;
	tmpmesh = MeshByName("Car");
	if(tmpmesh){//������

		//if(mouseRotation.x>mouseDeadZone || mouseRotation.x<-mouseDeadZone || mouseRotation.y>mouseDeadZone || mouseRotation.y<-mouseDeadZone ){
		tmpmesh->prop.Rotation.y+=-walkVector.z* -walkVector.x * D3DXToRadian(40)*0.03f;
		//}
		tmpmesh->prop.Position.x+=step*walkVector.z*sin(tmpmesh->prop.Rotation.y);
		tmpmesh->prop.Position.z+=step*walkVector.z*cos(tmpmesh->prop.Rotation.y);
	}
	//
	tmpmesh=NULL;
	tmpmesh = MeshByName("Torus");
	if(tmpmesh){//������
		tmpmesh->prop.Rotation.x+=0.03f;

		tmpmesh->prop.Rotation.y+=0.01f;
	}
}
void MeshSystemClass::Reset(IDirect3DDevice9 * d3ddev, SettingsClass *_settings){
	Log(L"Doing reset meshSystem after devicelost...");
	Dispose();
	Init(d3ddev,_settings);
}

void MeshSystemClass::Dispose(){
	for(int i=0;i<(int)Meshes.size();++i){
		if(Meshes[i]){
			Meshes[i]->Release();
			delete Meshes[i];
			Meshes[i]=NULL;
		}
	}
	Meshes.clear();

}
MeshClass * MeshSystemClass::MeshByName(string a){
	for(unsigned int i=0;i<Meshes.size();++i){
		if(Meshes[i])
			if(Meshes[i]->GetName()==a)
				return Meshes[i];
	}
	return NULL;
}