#pragma once
#define SETTINGS_FILENAME "settings.ini"
#define MULTISAMPLING_TYPES_COUNT 17
#define ALL_RESOLUTIONS_COUNT 17

#define MY_WS (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX )
#include "Resource.h"

//���������
const unsigned char Default_Texture_Mas[104]={66,77,104,0,0,0,0,0,0,0,54,0,0,0,40,0,0,0,4,0,0,0,4,0,0,0,1,0,24,0,0,0,0,0,50,0,0,0,18,11,0,0,18,11,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,255,0,0,0,255,0,255,255,0,255,0,0,0,255,0,255,0,0,0,0,0,0,255,0,255,0,0,0,255,0,255,255,0,255,0,0,0,255,0,255,0,0,0,0,0};
const unsigned int Default_Texture_Mas_Size=104;
const float SQRT2 = 1.4142135f;
struct Resolution{
	int Width;
	int Height;
	bool windowed;
	bool fullscreen;
};
struct LoadingStat{
	int texturesLoaded;
	int texturesCount;
	bool Mesh;
};
struct VECTOR3X3{
	D3DXVECTOR3 Position;
	D3DXVECTOR3 Rotation;
	D3DXVECTOR3 Scale;
	VECTOR3X3(){
		Position=D3DXVECTOR3(0.0f,0.0f,0.0f);
		Rotation=D3DXVECTOR3(0.0f,0.0f,0.0f);
		Scale=D3DXVECTOR3(1.0f,1.0f,1.0f);
	}
};
class SettingsClass
{
	int TextureFilter;
	bool Ignore;//for settings
public:
	bool Windowed;
	int Height;
	int Width;
	int MultiSamplingType;
	float MouseScaleX;
	float MouseScaleY;
	
	DWORD TextureMinFilter;
	DWORD TextureMaxAnisotropy;
	bool MultiSamplingSupport[17];
	Resolution Resolutions[ALL_RESOLUTIONS_COUNT];
	bool showSettings;
	bool operator!= (const SettingsClass &a);
	
	SettingsClass(void);
	~SettingsClass(void);
	bool GetFromFile();
	
	bool SaveToFile();
	void SetDefault();
	bool ShowSettingsBox(HWND hw);
	static INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);
	static SettingsClass self;
	//static Resolution FullResolutionList[15];
};
