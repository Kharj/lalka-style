#include "StdAfx.h"
#include "DXClass.h"



bool DXClass:: Init(HWND hw,SettingsClass *settings){
	ourHwnd=hw;
	lpSettings=settings;
	HRESULT hr;
	pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	deviceLost=false;
	D3DDISPLAYMODE DisplayMode;
	ZeroMemory(&d3dpp, sizeof(d3dpp));//memclear
	ZeroMemory(&DisplayMode, sizeof(DisplayMode));//memclear
	ZeroMemory(&windowRect, sizeof(windowRect));//memclear
	hr=pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT,&DisplayMode);	
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	D3DFORMAT DisplayFormat;
	D3DFORMAT BackBufferFormat;
	DisplayFormat=DisplayMode.Format;
	BackBufferFormat=DisplayMode.Format;
	d3dpp.BackBufferFormat=DisplayMode.Format;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	mousePos.x=0;mousePos.y=0;



	/*hr=pD3D->CheckDeviceType(D3DADAPTER_DEFAULT, 
	D3DDEVTYPE_HAL, 
	DisplayFormat, 
	BackBufferFormat, 
	settings.Windowed);
	if(FAILED(hr)){
	MessageBox(NULL,L"Video Mode not supported!",L"Fatal error",0);
	return false;
	}*/
	//GET RESOLUTIONS LIST
	for(int i=0;i<ALL_RESOLUTIONS_COUNT;++i){
		if(lpSettings->Resolutions[i].Height<=(int)DisplayMode.Height && lpSettings->Resolutions[i].Width<=(int)DisplayMode.Width){
			lpSettings->Resolutions[i].fullscreen=true;
		}
		RECT wr = {0, 0, lpSettings->Resolutions[i].Width, lpSettings->Resolutions[i].Height};
		AdjustWindowRect(&wr, MY_WS, FALSE);
		if(wr.right - wr.left<=(int)DisplayMode.Width && wr.bottom - wr.top<=(int)DisplayMode.Height){
			lpSettings->Resolutions[i].windowed=true;
		}
	}
	//GET MULTISAMPLING LIST
	for(int i=2;i<MULTISAMPLING_TYPES_COUNT;++i){//Get supported AA modes
		lpSettings->MultiSamplingSupport[i]=false;
		hr=pD3D->CheckDeviceMultiSampleType( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL , DisplayFormat, lpSettings->Windowed, 
			(D3DMULTISAMPLE_TYPE)i, NULL ) ;
		if(SUCCEEDED(hr)) lpSettings->MultiSamplingSupport[i]=true;
	}
	//Settings Dialog
	if(lpSettings->showSettings || 1){//////ALWAYS
		if(lpSettings->ShowSettingsBox(hw) && lpSettings->Windowed){
			//change hwnd size
			MoveWindow(hw,10,10,lpSettings->Width,lpSettings->Height,FALSE);
		}
	}
	//settings apply
	if(lpSettings->Height>(int)DisplayMode.Height || lpSettings->Width>(int)DisplayMode.Width){
		lpSettings->SetDefault();
	}
	d3dpp.Windowed = lpSettings->Windowed;
	d3dpp.BackBufferHeight=lpSettings->Height;
	d3dpp.BackBufferWidth=lpSettings->Width;
	//
	hr=pD3D->CheckDeviceMultiSampleType( D3DADAPTER_DEFAULT, 
		D3DDEVTYPE_HAL , DisplayFormat, lpSettings->Windowed, 
		(D3DMULTISAMPLE_TYPE)lpSettings->MultiSamplingType, NULL ) ;
	if(SUCCEEDED(hr)){
		d3dpp.MultiSampleType = (D3DMULTISAMPLE_TYPE)lpSettings->MultiSamplingType;
	}else{
		d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
		//MessageBox(NULL,L"Selected MultiSampling type not supported, using default (0).",L"",0);
	}

	hr = pD3D->CreateDevice(
		D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,
		hw,
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		&d3dpp,
		&d3ddev);
	if(hr!=D3D_OK) return false;
	hr = D3DXCreateFont( d3ddev, fontSize, 0, FW_BOLD, 0, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, 
		DEFAULT_PITCH | FF_DONTCARE, TEXT("Arial"), &m_font );
	if(FAILED(hr)) Log(L"Error creating font.");
	meshSystem.Init(d3ddev, lpSettings);
	init_light(d3ddev);
	RenderStates();

	return true;
}
void DXClass::RenderStates(){
	d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);    // turn off the 3D lighting
	//d3ddev->LightEnable(0,TRUE);
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	//d3ddev->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(50, 50, 50));    // ambient light
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);// Alpha blending
	d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}
void DXClass:: Draw(void){
	HRESULT hr;
	if(deviceLost){
		if(D3DERR_DEVICENOTRESET == d3ddev->TestCooperativeLevel()){//reset on focus

			d3ddev->Reset(&d3dpp);
			init_light(d3ddev);
			RenderStates();
			meshSystem.Reset(d3ddev,lpSettings);
			deviceLost=false;
		}else{
			return;
		}
	}
	d3ddev->Clear(0,NULL,D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 0.0f,0);
	d3ddev->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
	//begin
	d3ddev->BeginScene();

	meshSystem.Draw(d3ddev);
	//end
	d3ddev->EndScene();
	//present
	hr = d3ddev->Present(NULL,NULL,NULL,NULL);
	if(hr==D3DERR_DEVICELOST || hr==D3DERR_INVALIDCALL){
		deviceLost=true;
	}
}

void DXClass::Dispose(){
	if(pD3D) pD3D->Release();
	if(d3ddev) d3ddev->Release();
	meshSystem.Dispose();
}

void DXClass::Update(){
	meshSystem.Update(d3ddev);
}

void DXClass::SetMouseLocation(int x,int y){
	if(x<windowRect.left || x>windowRect.right || y<windowRect.top || y>windowRect.bottom) return;
	y-=windowRect.top;
	x-=windowRect.left;
	mousePos.x=-1.0f+2.0f*(float)x/(float)lpSettings->Width;
	mousePos.y=-1.0f+2.0f*(float)y/(float)lpSettings->Height;
}
void DXClass::ProcessKeyboard(const unsigned char keyboardState[256]){
	meshSystem.ControlInput(mousePos.x,mousePos.y,keyboardState);
}
void DXClass::SetWindowRect(RECT& a, int w, int h){
	if(lpSettings->Windowed){
		windowRect.top=a.top;
		windowRect.bottom=a.bottom;
		windowRect.left=a.left;
		windowRect.right=a.right;
	}else{
		windowRect.top=0;
		windowRect.bottom=lpSettings->Height;
		windowRect.left=0;
		windowRect.right=lpSettings->Width;
		w=lpSettings->Width;
		h=lpSettings->Height;
	}
}

// this is the function that sets up the lights and materials
void DXClass::init_light(IDirect3DDevice9 * d3ddev)
{
	//return;/////////////////////
	D3DLIGHT9 light;    // create the light struct
	//D3DMATERIAL9 material;    // create the material struct
	ZeroMemory(&light, sizeof(light));    // clear out the light struct for use
	light.Type = D3DLIGHT_DIRECTIONAL;    // make the light type 'directional light'
	light.Position=D3DXVECTOR3(0.0f,40.0f,-7.0f);
	light.Diffuse = D3DXCOLOR(0.7f, 0.7f, 0.7f, 1.0f);    // set the light's color
	light.Direction = D3DXVECTOR3(0.0f, -1.0f, 0.0f);

	d3ddev->SetLight(0, &light);    // send the light struct properties to light #0
	d3ddev->LightEnable(0, TRUE);    // turn on light #0

	//ZeroMemory(&material, sizeof(D3DMATERIAL9));    // clear out the struct for use
	//material.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set diffuse color to white
	//material.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set ambient color to white

	//d3ddev->SetMaterial(&material);    // set the globably-used material to &material
}
