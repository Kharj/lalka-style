#pragma once

class GeometrySystemClass
{
	struct IVVECTOR{int v,i;};
	vector <GeometryObject*> Objects;
	vector <MeshClass*> Meshes;
	SettingsClass *settings;
	LPDIRECT3DVERTEXBUFFER9 v_buffer;
	LPDIRECT3DINDEXBUFFER9 i_buffer;
	IVVECTOR *objectsOffsets;

public:
	virtual bool Init(IDirect3DDevice9 * d3ddev, SettingsClass *_settings);
	bool Draw(IDirect3DDevice9 * d3ddev);
	void Reset(IDirect3DDevice9 * d3ddev, SettingsClass *_settings);
	virtual void Dispose();
	//void Update(IDirect3DDevice9 * d3ddev);
	GeometrySystemClass(void);
	~GeometrySystemClass(void);
};
