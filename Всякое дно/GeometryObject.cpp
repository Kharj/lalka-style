#include "StdAfx.h"
#include "GeometryObject.h"
#define GEOMETRYOBJECT_DEFAULTCOLOR D3DCOLOR_XRGB(255,255,255)
GeometryObject::GeometryObject()
{
	pTexture=NULL;
	RenderStateMas = NULL;
	TextureStageStateMas = NULL;
	vertices = NULL;
	v_size = 0;
	indexes = NULL;
	i_size = 0;
}
GeometryObject::GeometryObject(const GeometryObject& a)
{
	
	loadingStat.Mesh=false; loadingStat.texturesCount=0; loadingStat.texturesLoaded=0;
	RenderStateMas = NULL;
	TextureStageStateMas = NULL;
	pTexture=NULL;
	vertices = NULL;	
	indexes = NULL;
	v_size = 0;
	i_size = 0;
	//if(a.RenderStateMas) memcpy(RenderStateMas, a.RenderStateMas, sizeof(a.RenderStateMas));
	//if(a.TextureStageStateMas) memcpy(TextureStageStateMas, a.TextureStageStateMas, sizeof(a.TextureStageStateMas));
	v_size = a.v_size;
	i_size= a.i_size;
	if(a.vertices){
		vertices = new UNIVERSALVERTEX[v_size];
		for(int i=0;i<v_size;++i) 
			vertices[i]=a.vertices[i];
	}
	if(a.indexes){
		indexes = new int[i_size];
		for(int i=0;i<i_size;++i) indexes[i]=a.indexes[i];
	}
	//pTexture->=*new LPDIRECT3DTEXTURE9(a.pTexture);
	//IDirect3DDevice9::UpdateTexture(pTexture,a.pTexture);

}

GeometryObject::~GeometryObject(void)
{
	//if(RenderStateMas) delete [] RenderStateMas;
	//RenderStateMas = NULL;
	//if(TextureStageStateMas) delete [] TextureStageStateMas;
	//TextureStageStateMas = NULL;

	if(vertices) delete [] vertices;
	vertices = NULL;
	v_size = 0;
	if(indexes) delete [] indexes;
	indexes = NULL;
	i_size = 0;
	if(pTexture){ 
		pTexture->Release();
		pTexture=NULL; 
	}
}

void GeometryObject::SetVetrices(UNIVERSALVERTEX *mas, int size, int offset){
	if(vertices){ delete [] vertices; vertices = NULL; }
	v_size=size-offset;
	vertices = new UNIVERSALVERTEX[v_size];
	for(int i=offset, j=0;i<size;++i, ++j){
		vertices[j]=mas[i];
	}
}
void GeometryObject::SetVetrices(DIFFUSEVERTEX *mas, int size, int offset){
	if(vertices){ delete [] vertices; vertices = NULL; }
	v_size=size-offset;
	vertices = new UNIVERSALVERTEX[v_size];
	for(int i=offset, j=0;i<size;++i, ++j){
		vertices[j].pos=mas[i].pos;
		vertices[j].COLOR=mas[i].COLOR;
		vertices[j].norm=D3DXVECTOR3(0.0f,0.0f,0.0f);//���������
		vertices[j].u=0.0f;
		vertices[j].v=0.0f;
	}
}
void GeometryObject::SetVetrices(DIFFUSENORMVERTEX *mas, int size, int offset){
	if(vertices){ delete [] vertices; vertices = NULL; }
	v_size=size-offset;
	vertices = new UNIVERSALVERTEX[v_size];
	for(int i=offset, j=0;i<size;++i, ++j){
		vertices[j].pos=mas[i].pos;
		vertices[j].COLOR=mas[i].COLOR;
		vertices[j].norm=mas[i].norm;
		vertices[j].u=0.0f;
		vertices[j].v=0.0f;
	}
}
void GeometryObject::SetVetrices(TEXTUREDVERTEX *mas, int size, int offset){
	if(vertices){ delete [] vertices; vertices = NULL; }
	v_size=size-offset;
	vertices = new UNIVERSALVERTEX[v_size];
	for(int i=offset, j=0;i<size;++i, ++j){
		vertices[j].pos=mas[i].pos;
		vertices[j].COLOR=GEOMETRYOBJECT_DEFAULTCOLOR;
		vertices[j].norm=D3DXVECTOR3(0.0f,0.0f,0.0f);//���������
		vertices[j].u=mas[i].u;
		vertices[j].v=mas[i].v;
	}
}
void GeometryObject::SetVetrices(TEXTUREDNORMVERTEX *mas, int size, int offset){
	if(vertices){ delete [] vertices; vertices = NULL; }
	v_size=size-offset;
	vertices = new UNIVERSALVERTEX[v_size];
	for(int i=offset, j=0;i<size;++i, ++j){
		vertices[j].pos=mas[i].pos;
		vertices[j].COLOR=GEOMETRYOBJECT_DEFAULTCOLOR;
		vertices[j].norm=mas[i].norm;
		vertices[j].u=mas[i].u;
		vertices[j].v=mas[i].v;
	}
}
void GeometryObject::SetIndexes(int* mas,int size, int offset){
	if(indexes){ delete [] indexes; indexes=NULL; }
	i_size=size-offset;
	indexes = new int[i_size];
	for(int i=offset, j=0;i<size;++i, ++j){
		indexes[j]=mas[i];
	}
}
void GeometryObject::SetIndexes(){
	if(indexes){ delete [] indexes; indexes=NULL; }
	i_size=v_size;
	indexes = new int[i_size];
	for(int i=0;i<i_size;++i){
		indexes[i]=i;
	}
}
void GeometryObject::SetRenderState(D3DRENDERSTATETYPE State, DWORD Value){
	RenderStateStruct tmpstruct;
	tmpstruct.State=State;
	tmpstruct.Value=Value;
	RenderStateTmpVector.push_back(tmpstruct);
}
void GeometryObject::SetTextureStageState(DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD Value){
	TextureStageStateStruct tmpstruct;
	tmpstruct.Stage=Stage;
	tmpstruct.Type=Type;
	tmpstruct.Value=Value;
	TextureStageStateTmpVector.push_back(tmpstruct);
}
void GeometryObject::Init(){
	/*if(RenderStateTmpVector.size()){
	RenderStateMas=new RenderStateStruct[RenderStateTmpVector.size()];
	for(int i=0;i<(int)RenderStateTmpVector.size();++i)
	RenderStateMas[i]=RenderStateTmpVector[i];
	}
	if(TextureStageStateTmpVector.size()){
	TextureStageStateMas=new TextureStageStateStruct[TextureStageStateTmpVector.size()];
	for(int i=0;i<(int)TextureStageStateTmpVector.size();++i)
	TextureStageStateMas[i]=TextureStageStateTmpVector[i];
	}
	*/
	//���������� �������������
	//�������� ��������
}
bool GeometryObject::CreateTextureFromResource(LPDIRECT3DDEVICE9 d3ddev,LPCWSTR ResName){
	loadingStat.texturesCount=1;
	if(pTexture){ 
		pTexture->Release(); 
		pTexture=NULL; }
	HRESULT hr = D3DXCreateTextureFromResource(d3ddev,NULL,ResName,&pTexture);
		if(hr!=D3D_OK){
				Log(ResName,L" not loaded, using default");//log
				pTexture=NULL;
				hr = D3DXCreateTextureFromFileInMemory(d3ddev, (LPCVOID)Default_Texture_Mas,Default_Texture_Mas_Size, &pTexture);
			}
			loadingStat.texturesLoaded++;
	return (hr==D3D_OK);
}
bool GeometryObject::CreateTextureFromFile(LPDIRECT3DDEVICE9 d3ddev,LPCWSTR FileName){
	loadingStat.texturesCount=1;
	if(pTexture){ pTexture->Release(); pTexture=NULL; }
	HRESULT hr = D3DXCreateTextureFromFile(d3ddev,FileName,&pTexture);
	if(hr!=D3D_OK){
				Log(FileName,L" not loaded, using default");//log
				pTexture=NULL;
				hr = D3DXCreateTextureFromFileInMemory(d3ddev, (LPCVOID)Default_Texture_Mas,Default_Texture_Mas_Size, &pTexture);
			}
			loadingStat.texturesLoaded++;
	return (hr==D3D_OK);
}

bool GeometryObject::LoadFromFile(const char *filename){
	bool boolres=false;
	UNIVERSALVERTEX *Umas=NULL;
	DIFFUSEVERTEX *Dmas=NULL;
	DIFFUSENORMVERTEX *DNmas=NULL;
	TEXTUREDVERTEX *Tmas=NULL;
	TEXTUREDNORMVERTEX *TNmas=NULL;
	int *tmpi=NULL;
	const int max_len=200;
	FILE* fp=NULL;
	fopen_s(&fp,filename,"r");
	if(!fp) goto Bad_close;
	int v_type=0,v_count=0,i_count=0,res=0;
	char tmp1[max_len]={0};
	char pattern[max_len];
	res = fscanf_s(fp,"UNIVERSALVERTEX=1_DIFFUSEVERTEX=2_DIFFUSENORMVERTEX=3_TEXTUREDVERTEX=4_TEXTUREDNORMVERTEX=5;\nVERTICES TYPE %d\nVERTICES COUNT %d\nINDEXES COUNT %d\nBEGIN\n",&v_type,&v_count,&i_count);
	if(res!=3 || v_count<=0 || v_type<=0 || v_type>5) 
		goto Bad_close;//v_type
	if(i_count<0) i_count=0;
	switch(v_type){
		case 1://uni
			Umas = new UNIVERSALVERTEX[v_count];
			strcpy(pattern,"V: (%f, %f, %f), (%f, %f, %f), (%d, %d, %d), %f, %f;\n");
			break;
		case 2://dif
			Dmas = new DIFFUSEVERTEX[v_count];
			strcpy(pattern,"V: (%f, %f, %f), (%d, %d, %d);\n");
			break;
		case 3://dif norm
			DNmas = new DIFFUSENORMVERTEX[v_count];
			strcpy(pattern,"V: (%f, %f, %f), (%f, %f, %f), (%d, %d, %d);\n");
			break;
		case 4://tex
			Tmas = new TEXTUREDVERTEX[v_count];
			strcpy(pattern,"V: (%f, %f, %f), %f, %f;\n");
			break;
		case 5://tex norm
			TNmas = new TEXTUREDNORMVERTEX[v_count];
			strcpy(pattern,"V: (%f, %f, %f), (%f, %f, %f), %f, %f;\n");
			break;
		default:
			goto Bad_close; break;
	}
	for(int i=0;i<v_count;++i){
		float tmpv[3][3];
		int r,g,b;
		switch(v_type){
			case 1://uni
				res = fscanf_s(fp,"V: (%f, %f, %f), (%f, %f, %f), (%d, %d, %d), %f, %f;\n", &tmpv[0][0], &tmpv[0][1], &tmpv[0][2], &tmpv[1][0], &tmpv[1][1], &tmpv[1][2], &r, &g, &b, &tmpv[2][0], &tmpv[2][1]);
				if(res!=11) goto Bad_close;
				Umas[i].pos=D3DXVECTOR3(tmpv[0][0],tmpv[0][1],tmpv[0][2]);
				Umas[i].norm=D3DXVECTOR3(tmpv[1][0],tmpv[1][1],tmpv[1][2]);
				Umas[i].COLOR=D3DCOLOR_XRGB(r,g,b);
				Umas[i].u=tmpv[2][0];
				Umas[i].v=tmpv[2][1];
				break;
			case 2://dif
				res = fscanf_s(fp,"V: (%f, %f, %f), (%d, %d, %d);\n", &tmpv[0][0], &tmpv[0][1],&tmpv[0][2], &r, &g, &b);
				if(res!=6) goto Bad_close;
				Dmas[i].pos=D3DXVECTOR3(tmpv[0][0],tmpv[0][1],tmpv[0][2]);
				Dmas[i].COLOR=D3DCOLOR_XRGB(r,g,b);
				break;
			case 3://dif norm
				res = fscanf_s(fp,"V: (%f, %f, %f), (%f, %f, %f), (%d, %d, %d);\n", &tmpv[0][0], &tmpv[0][1], &tmpv[0][2], &tmpv[1][0], &tmpv[1][1], &tmpv[1][2], &r, &g, &b);
				if(res!=9) goto Bad_close;
				DNmas[i].pos=D3DXVECTOR3(tmpv[0][0],tmpv[0][1],tmpv[0][2]);
				DNmas[i].norm=D3DXVECTOR3(tmpv[1][0],tmpv[1][1],tmpv[1][2]);
				DNmas[i].COLOR=D3DCOLOR_XRGB(r,g,b);
				break;
			case 4://tex
				res = fscanf_s(fp,"V: (%f, %f, %f), %f, %f;\n", &tmpv[0][0], &tmpv[0][1], &tmpv[0][2], &tmpv[2][0], &tmpv[2][1]);
				if(res!=5) goto Bad_close;
				Tmas[i].pos=D3DXVECTOR3(tmpv[0][0],tmpv[0][1],tmpv[0][2]);
				Tmas[i].u=tmpv[2][0];
				Tmas[i].v=tmpv[2][1];
				break;
			case 5://tex norm
				res = fscanf_s(fp,"V: (%f, %f, %f), (%f, %f, %f), %f, %f;\n", &tmpv[0][0], &tmpv[0][1], &tmpv[0][2], &tmpv[1][0], &tmpv[1][1], &tmpv[1][2], &tmpv[2][0], &tmpv[2][1]);
				if(res!=8) goto Bad_close;
				TNmas[i].pos=D3DXVECTOR3(tmpv[0][0],tmpv[0][1],tmpv[0][2]);
				TNmas[i].norm=D3DXVECTOR3(tmpv[1][0],tmpv[1][1],tmpv[1][2]);
				TNmas[i].u=tmpv[2][0];
				TNmas[i].v=tmpv[2][1];
				break;
			default:
				goto Bad_close; break;
		}
	}
	switch(v_type){
		case 1://uni
			SetVetrices(Umas,v_count); break;
		case 2://dif
			SetVetrices(Dmas,v_count); break;
		case 3://dif norm
			SetVetrices(DNmas,v_count); break;
		case 4://tex
			SetVetrices(Tmas,v_count); break;
		case 5://tex norm
			SetVetrices(TNmas,v_count); break;
		default:
			goto Bad_close; break;
	}
	res=fscanf_s(fp,"END\n");
	if(-1==res) goto Bad_close;
	if(!i_count){
		SetIndexes();//default
	}else{
		tmpi = new int[i_count];
		res=fscanf_s(fp,"INDEXES\n");
		if(res==-1) goto Bad_close;
		for(int i=0;i<i_count;++i){
			res=fscanf_s(fp,"%d\n",&tmpi[i]);
			if(res!=1) goto Bad_close;
		}
		SetIndexes(tmpi,i_count);
	}
	boolres=true;
Bad_close:
	if(Umas){ delete [] Umas; Umas=NULL;}
	if(Dmas){ delete [] Dmas; Dmas=NULL;}
	if(DNmas){ delete [] DNmas; DNmas=NULL;}
	if(Tmas){ delete [] Tmas; Tmas=NULL;}
	if(TNmas){ delete [] TNmas; TNmas=NULL;}
	if(tmpi){delete [] tmpi; tmpi=NULL;}

	fclose(fp);
	return boolres;
}