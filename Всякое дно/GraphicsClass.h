#pragma once


#define CUSTOMFVF (D3DFVF_XYZ | D3DFVF_DIFFUSE)

struct CUSTOMVERTEX {FLOAT X, Y, Z; DWORD COLOR;};

class GraphicsClass
{
public:
	virtual bool Init(IDirect3DDevice9 * d3ddev, SettingsClass _settings);
	void Reset(IDirect3DDevice9 * d3ddev, SettingsClass _settings);

	virtual void Dispose();
	bool Draw(IDirect3DDevice9 * d3ddev);
	void Update(IDirect3DDevice9 * d3ddev);
	GraphicsClass(void);
	~GraphicsClass(void);
protected:
	SettingsClass settings;
	LPDIRECT3DVERTEXBUFFER9 v_buffer;
};

#include "GraphicsInheritance.cpp"