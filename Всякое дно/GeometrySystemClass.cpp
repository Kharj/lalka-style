#include "StdAfx.h"
#include "GeometryObject.h"
#include "GeometrySystemClass.h"

GeometrySystemClass::GeometrySystemClass(void)
{
}

GeometrySystemClass::~GeometrySystemClass(void)
{
}

bool GeometrySystemClass::Init(IDirect3DDevice9 * d3ddev, SettingsClass *_settings){

	GeometryObject *tmpobj=new GeometryObject;
	GeometryObject *tmpobj1=new GeometryObject;
	MeshClass *tmpmesh=new MeshClass;
	tmpobj->LoadFromFile("model1.txt");
	tmpobj1->LoadFromFile("model2.txt");
	tmpobj1->CreateTextureFromResource(d3ddev,MAKEINTRESOURCE(145));
	if(tmpmesh->LoadFromX(L"cot_chevy.X",d3ddev)) Meshes.push_back(tmpmesh);

	Objects.push_back(tmpobj);
	Objects.push_back(tmpobj1);

	settings=_settings;
	//TEXTURE

	if(!Objects.size()){
		MessageBox(NULL,L"No GeometryObject loaded!!!",L"AERROR!",MB_OK);
		return false;
	}
	int v_count=0, i_count=0;
	for(int i=0;i<(int)Objects.size();++i){
		v_count += Objects[i]->v_size;
		i_count += Objects[i]->i_size;
	}

	UNIVERSALVERTEX *vertices=new UNIVERSALVERTEX[v_count];
	int *indexes=new int[i_count];
	objectsOffsets=new IVVECTOR[Objects.size()];

	for(int i=0,j=0,ki=0,kv=0;i<(int)Objects.size();++i){//��� ������� � ����

		objectsOffsets[i].i=ki;
		for(j=0;j<Objects[i]->i_size;++j,ki++){//index
			indexes[ki]=Objects[i]->indexes[j]/*+kv*/;
		}
		objectsOffsets[i].v=kv;
		for(j=0;j<Objects[i]->v_size;++j,kv++){//vertex
			vertices[kv]=Objects[i]->vertices[j];
		}

	}

	d3ddev->CreateVertexBuffer(v_count*sizeof(UNIVERSALVERTEX),
		0,
		UNIVERSALFVF,
		D3DPOOL_MANAGED,
		&v_buffer,
		NULL);

	VOID* pVoid;    // a void pointer

	// lock v_buffer and load the vertices into it
	v_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, v_count*sizeof(UNIVERSALVERTEX));
	v_buffer->Unlock();


	if(vertices) delete []vertices;
	vertices=NULL;
	// create an index buffer interface called i_buffer
	d3ddev->CreateIndexBuffer(i_count*sizeof(int),
		0,
		D3DFMT_INDEX32,
		D3DPOOL_MANAGED,
		&i_buffer,
		NULL);

	// lock i_buffer and load the indices into it
	i_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, indexes, i_count*sizeof(int));
	i_buffer->Unlock();
	if(indexes) delete []indexes;	
	indexes=NULL;
	return true;
}
bool GeometrySystemClass::Draw(IDirect3DDevice9 * d3ddev){
	//HRESULT hr = d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);
	HRESULT hr=0;
	d3ddev->SetFVF(UNIVERSALFVF);
	//hr = d3ddev->SetTexture(0,pTexture);

	d3ddev->SetRenderState(D3DRS_LIGHTING, FALSE);    // turn off the 3D lighting
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	//d3ddev->SetTextureStageState(0,D3DTSS_TEXCOORDINDEX,0);
	d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1,D3DTA_TEXTURE);
	d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);
	d3ddev->SetTextureStageState(0, D3DTSS_COLOROP,  D3DTOP_MODULATE);
	d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP,  D3DTOP_DISABLE);

	d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, settings->TextureMinFilter); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, settings->TextureMaxAnisotropy);

	// SET UP THE PIPELINE

	D3DXMATRIX matRotateY, matRotateX, matTranslate, matScale, matProjection;    // a matrix to store the rotation information

	static float index = 0.0f; 
	index+=0.01f;    // an ever-increasing float value
	D3DXMATRIX matView;    // the view transform matrix

	D3DXMatrixLookAtLH(&matView,
		&D3DXVECTOR3 (0.0f, 0.0f, 20.0f),    // the camera position
		&D3DXVECTOR3 (0.0f, 0.0f, 0.0f),    // the look-at position
		&D3DXVECTOR3 (0.0f, 1.0f, 0.0f));    // the up direction

	d3ddev->SetTransform(D3DTS_VIEW, &matView);    // set the view transform to matView

	D3DXMatrixPerspectiveFovLH(&matProjection,
		D3DXToRadian(45),    // the horizontal field of view
		(FLOAT) settings->Width/ (FLOAT)settings->Height, // aspect ratio
		1.0f,    // the near view-plane
		100.0f);    // the far view-plane

	d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);    // set the projection

	// select the vertex buffer to display

	d3ddev->SetStreamSource(0, v_buffer, 0, sizeof(UNIVERSALVERTEX));
	D3DXMatrixTranslation(&matTranslate, 0.0f, -4.0f, 0.0f);
    D3DXMatrixScaling(&matScale, 0.1f, 0.1f, 0.1f);
	D3DXMatrixRotationY(&matRotateY, index);
	D3DXMatrixRotationX(&matRotateX, D3DXToRadian(-90));

	// tell Direct3D about each world transform, and then draw another triangle
	d3ddev->SetTransform(D3DTS_WORLD, &(matRotateY * /* matScale **/ matTranslate));
	//d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);
	d3ddev->SetIndices(i_buffer);
	for(int i=0;i<(int)Objects.size();++i){
		if(Objects[i]->pTexture){
			d3ddev->SetTexture(0,Objects[i]->pTexture);
		}else{
			d3ddev->SetTexture(0,NULL);
		}
		hr = d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,objectsOffsets[i].v,0,Objects[i]->v_size,objectsOffsets[i].i,Objects[i]->i_size/3);
		hr =0;//d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,0,0,3,0,1);
		//d3ddev->DrawPrimitive(D3DPT_TRIANGLELIST,objectsOffsets[i].v,Objects[i]->v_size/3);
	}
	
	D3DXMatrixRotationY(&matRotateY, -index);
	D3DXMatrixTranslation(&matTranslate, 0.0f, 2.0f, 0.0f);
	d3ddev->SetTransform(D3DTS_WORLD, &(matRotateX * matRotateY * matScale * matTranslate));
	for(int i=0;i<(int)Meshes.size();i++){
		for (DWORD j=0; j<Meshes[i]->numMaterials; j++)
		{
			// Set the material and texture for this subset

			d3ddev->SetMaterial(&Meshes[i]->meshMaterials[j]);
			d3ddev->SetTexture(0,Meshes[i]->meshTextures[j]);
			
			// Draw the mesh subset
			Meshes[i]->mesh->DrawSubset(j);
		}
	}

	//	d3ddev->DrawPrimitive(D3DPT_TRIANGLELIST,0,1);
	//	d3ddev->SetTransform(D3DTS_WORLD, &(matTranslate * matRotateX));
	//	d3ddev->DrawPrimitive(D3DPT_TRIANGLELIST,3,8);
	return true;
}
void GeometrySystemClass::Reset(IDirect3DDevice9 * d3ddev, SettingsClass *_settings){
	Dispose();
	Init(d3ddev,_settings);
}
void GeometrySystemClass::Dispose(){

	if(v_buffer) v_buffer->Release();
	v_buffer=NULL;
	if(i_buffer) i_buffer->Release();
	i_buffer=NULL;
	for(int i=0;i<(int)Objects.size();i++)
		if(Objects[i]) delete Objects[i];

	//if(pTexture) pTexture->Release();
	//pTexture=NULL;
}