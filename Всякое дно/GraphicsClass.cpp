#include "StdAfx.h"
//#include "GraphicsClass.h"
//#include "GraphicsInheritance.h"
GraphicsClass::GraphicsClass(void)
{
	v_buffer=NULL;
}

GraphicsClass::~GraphicsClass(void)
{
}


bool GraphicsClass::Init(IDirect3DDevice9 * d3ddev, SettingsClass _settings){
	settings=_settings;
	// create the vertices using the CUSTOMVERTEX struct
	CUSTOMVERTEX vertices[] =
	{
		{ 2.5f, -3.0f, 0.0f, D3DCOLOR_XRGB(0, 0, 255), },
		{ 0.0f, 3.0f, 0.0f, D3DCOLOR_XRGB(0, 255, 0), },
		{ -2.5f, -3.0f, 0.0f, D3DCOLOR_XRGB(255, 0, 0), },
	};

	// create a vertex buffer interface called v_buffer
	d3ddev->CreateVertexBuffer(3*sizeof(CUSTOMVERTEX),
		0,
		CUSTOMFVF,
		D3DPOOL_MANAGED,
		&v_buffer,
		NULL);

	VOID* pVoid;    // a void pointer

	// lock v_buffer and load the vertices into it
	v_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	v_buffer->Unlock();
	return true;
}
void GraphicsClass::Reset(IDirect3DDevice9 * d3ddev, SettingsClass _settings){
	Dispose();
	Init(d3ddev,_settings);
}
bool GraphicsClass::Draw(IDirect3DDevice9 * d3ddev){
	d3ddev->SetFVF(CUSTOMFVF);
	// SET UP THE PIPELINE

	D3DXMATRIX matRotateY;    // a matrix to store the rotation information

	static float index = 0.0f; 
	index+=0.01f;    // an ever-increasing float value

	// build a matrix to rotate the model based on the increasing float value
	D3DXMatrixRotationY(&matRotateY, index);

	// tell Direct3D about our matrix
	d3ddev->SetTransform(D3DTS_WORLD, &matRotateY);

	D3DXMATRIX matView;    // the view transform matrix

	D3DXMatrixLookAtLH(&matView,
		&D3DXVECTOR3 (0.0f, 0.0f, 10.0f),    // the camera position
		&D3DXVECTOR3 (0.0f, 0.0f, 0.0f),    // the look-at position
		&D3DXVECTOR3 (0.0f, 1.0f, 0.0f));    // the up direction

	d3ddev->SetTransform(D3DTS_VIEW, &matView);    // set the view transform to matView

	D3DXMATRIX matProjection;     // the projection transform matrix

	D3DXMatrixPerspectiveFovLH(&matProjection,
		D3DXToRadian(45),    // the horizontal field of view
		(FLOAT) settings.Width/ (FLOAT)settings.Height, // aspect ratio
		1.0f,    // the near view-plane
		100.0f);    // the far view-plane

	d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);    // set the projection

	// select the vertex buffer to display
	d3ddev->SetStreamSource(0, v_buffer, 0, sizeof(CUSTOMVERTEX));

	D3DXMATRIX matTranslateA;    // a matrix to store the translation for triangle A
	D3DXMATRIX matTranslateB;    // a matrix to store the translation for triangle B
	//   D3DXMATRIX matRotateY;    // a matrix to store the rotation for each triangle
	//   static float index = 0.0f; index+=0.05f; // an ever-increasing float value

	// build MULTIPLE matrices to translate the model and one to rotate
	D3DXMatrixTranslation(&matTranslateA, 0.0f, 0.0f, 2.0f);
	D3DXMatrixTranslation(&matTranslateB, 0.0f, 0.0f, -2.0f);
	D3DXMatrixRotationY(&matRotateY, index);    // the front side

	// tell Direct3D about each world transform, and then draw another triangle
	d3ddev->SetTransform(D3DTS_WORLD, &(matTranslateA * matRotateY));
	d3ddev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

	d3ddev->SetTransform(D3DTS_WORLD, &(matTranslateB * matRotateY));
	d3ddev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);
	return true;
}

void GraphicsClass::Update(IDirect3DDevice9 * d3ddev){

}
void GraphicsClass::Dispose(){
	if(v_buffer) v_buffer->Release();
}