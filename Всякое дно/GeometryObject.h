#pragma once

#define UNIVERSALFVF ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1 )
#define DIFUSEFVF ( D3DFVF_XYZ | D3DFVF_DIFFUSE )
#define DIFFUSENORMFVF ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE )
#define TEXTUREDFVF ( D3DFVF_XYZ | D3DFVF_TEX1 )
#define TEXTUREDNORMFVF ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 )

struct UNIVERSALVERTEX {D3DXVECTOR3 pos; D3DXVECTOR3 norm; DWORD COLOR; FLOAT u,v; };
struct DIFFUSEVERTEX {D3DXVECTOR3 pos; DWORD COLOR;};
struct DIFFUSENORMVERTEX {D3DXVECTOR3 pos; D3DXVECTOR3 norm; DWORD COLOR;};
struct TEXTUREDVERTEX {D3DXVECTOR3 pos; FLOAT u,v; };
struct TEXTUREDNORMVERTEX {D3DXVECTOR3 pos; D3DXVECTOR3 norm; FLOAT u,v; };

class GeometryObject
{
	struct RenderStateStruct{ D3DRENDERSTATETYPE State; DWORD Value;};
	struct TextureStageStateStruct{ DWORD Stage;D3DTEXTURESTAGESTATETYPE Type; DWORD Value;};
	vector<RenderStateStruct> RenderStateTmpVector;
	vector<TextureStageStateStruct> TextureStageStateTmpVector;
	RenderStateStruct * RenderStateMas;
	TextureStageStateStruct * TextureStageStateMas;
public:
	
	UNIVERSALVERTEX * vertices;
	int * indexes;
	int v_size;
	int i_size;
	LPDIRECT3DTEXTURE9 pTexture;
	LoadingStat loadingStat;

	void Init();
	//setup methods
	void SetRenderState(D3DRENDERSTATETYPE State, DWORD Value);
	void SetTextureStageState(DWORD Stage,D3DTEXTURESTAGESTATETYPE Type, DWORD Value);
	void SetVetrices(UNIVERSALVERTEX *mas, int size, int offset=0);
	void SetVetrices(DIFFUSEVERTEX *mas, int size, int offset=0);
	void SetVetrices(DIFFUSENORMVERTEX *mas, int size, int offset=0);
	void SetVetrices(TEXTUREDVERTEX *mas, int size, int offset=0);
	void SetVetrices(TEXTUREDNORMVERTEX *mas, int size, int offset=0);
	void SetIndexes(int *mas, int size, int offset=0);
	bool LoadFromFile(const char *filename);
	void SetIndexes();
	bool CreateTextureFromResource(LPDIRECT3DDEVICE9 d3ddev,LPCWSTR ResName);
	bool CreateTextureFromFile(LPDIRECT3DDEVICE9 d3ddev,LPCTSTR FileName);
	GeometryObject(void);
	GeometryObject(const GeometryObject& a);
	~GeometryObject(void);
};
