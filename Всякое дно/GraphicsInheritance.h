#pragma once


class RectGraphicsClass : public GraphicsClass{
public:

	bool Init(IDirect3DDevice9 * d3ddev, SettingsClass _settings){
		settings=_settings;
		// create the vertices using the CUSTOMVERTEX struct
		CUSTOMVERTEX vertices[] = 
		{
			{ -3.0f, 4.0f, 4.0f, D3DCOLOR_ARGB(192, 255, 128, 0), },
			{ -3.0f, -3.0f, 4.0f, D3DCOLOR_ARGB(192, 255, 128, 0), },
			{ 3.0f, 3.0f, 4.0f, D3DCOLOR_ARGB(192, 255, 128, 0), },
			{ 3.0f, -3.0f, 4.0f, D3DCOLOR_ARGB(192, 255, 128, 50), },
		}; 

		// create a vertex buffer interface called v_buffer
		d3ddev->CreateVertexBuffer(4*sizeof(CUSTOMVERTEX),
			0,
			CUSTOMFVF,
			D3DPOOL_MANAGED,
			&v_buffer,
			NULL);

		VOID* pVoid;    // a void pointer

		// lock v_buffer and load the vertices into it
		v_buffer->Lock(0, 0, (void**)&pVoid, 0);
		memcpy(pVoid, vertices, sizeof(vertices));
		v_buffer->Unlock();
		return true;
	}
	bool Draw(IDirect3DDevice9 * d3ddev){
		d3ddev->SetFVF(CUSTOMFVF);
		// SET UP THE PIPELINE

		D3DXMATRIX matRotateX;    // a matrix to store the rotation information

		static float index = 0.0f; 
		index+=0.01f;    // an ever-increasing float value

		// build a matrix to rotate the model based on the increasing float value
		D3DXMatrixRotationX(&matRotateX, index);

		// tell Direct3D about our matrix
		d3ddev->SetTransform(D3DTS_WORLD, &matRotateX);

		D3DXMATRIX matView;    // the view transform matrix

		D3DXMatrixLookAtLH(&matView,
			&D3DXVECTOR3 (0.0f, 0.0f, 10.0f),    // the camera position
			&D3DXVECTOR3 (0.0f, 0.0f, 0.0f),    // the look-at position
			&D3DXVECTOR3 (0.0f, 1.0f, 0.0f));    // the up direction

		d3ddev->SetTransform(D3DTS_VIEW, &matView);    // set the view transform to matView

		D3DXMATRIX matProjection;     // the projection transform matrix

		D3DXMatrixPerspectiveFovLH(&matProjection,
			D3DXToRadian(45),    // the horizontal field of view
			(FLOAT) settings.Width/ (FLOAT)settings.Height, // aspect ratio
			1.0f,    // the near view-plane
			100.0f);    // the far view-plane

		d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);    // set the projection

		// select the vertex buffer to display
		d3ddev->SetStreamSource(0, v_buffer, 0, sizeof(CUSTOMVERTEX));

		D3DXMATRIX matTranslateA;    // a matrix to store the translation for triangle A
		//   D3DXMATRIX matRotateY;    // a matrix to store the rotation for each triangle
		//   static float index = 0.0f; index+=0.05f; // an ever-increasing float value

		// build MULTIPLE matrices to translate the model and one to rotate
		D3DXMatrixTranslation(&matTranslateA, 0.0f, 0.0f, 2.0f);
		D3DXMatrixRotationY(&matRotateX, index);    // the front side

		// tell Direct3D about each world transform, and then draw another triangle
		d3ddev->SetTransform(D3DTS_WORLD, &(matTranslateA * matRotateX));
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
		return true;
	}
};

class CubeGraphicsClass : public GraphicsClass{
	LPDIRECT3DINDEXBUFFER9 i_buffer;
public:

	bool Init(IDirect3DDevice9 * d3ddev, SettingsClass _settings){
		settings=_settings;
		//TEXTURE


		// create the vertices using the CUSTOMVERTEX struct
		CUSTOMVERTEX vertices[] =
		{
			{ -3.0f, -3.0f, 3.0f, D3DCOLOR_ARGB(128, 0, 0, 255), },    // side 1
			{ 3.0f, -3.0f, 3.0f, D3DCOLOR_ARGB(128, 0, 255, 0), },
			{ -3.0f, 3.0f, 3.0f, D3DCOLOR_ARGB(128, 255, 0, 0), },
			{ 3.0f, 3.0f, 3.0f, D3DCOLOR_ARGB(128, 0, 255, 255), },

			{ -3.0f, -3.0f, -3.0f, D3DCOLOR_ARGB(192, 0, 0, 255), },    // side 2
			{ -3.0f, 3.0f, -3.0f, D3DCOLOR_ARGB(192, 0, 255, 0), },
			{ 3.0f, -3.0f, -3.0f, D3DCOLOR_ARGB(192, 255, 0, 0), },
			{ 3.0f, 3.0f, -3.0f, D3DCOLOR_ARGB(192, 0, 255, 255), },

			{ -3.0f, 3.0f, -3.0f, D3DCOLOR_ARGB(255, 0, 0, 255), },    // side 3
			{ -3.0f, 3.0f, 3.0f, D3DCOLOR_ARGB(255, 0, 255, 0), },
			{ 3.0f, 3.0f, -3.0f, D3DCOLOR_ARGB(255, 255, 0, 0), },
			{ 3.0f, 3.0f, 3.0f, D3DCOLOR_ARGB(255, 0, 255, 255), },

			{ -3.0f, -3.0f, -3.0f, D3DCOLOR_ARGB(255, 0, 0, 255), },    // side 4
			{ 3.0f, -3.0f, -3.0f, D3DCOLOR_ARGB(255, 0, 255, 0), },
			{ -3.0f, -3.0f, 3.0f, D3DCOLOR_ARGB(255, 255, 0, 0), },
			{ 3.0f, -3.0f, 3.0f, D3DCOLOR_ARGB(255, 0, 255, 255), },

			{ 3.0f, -3.0f, -3.0f, D3DCOLOR_ARGB(255, 0, 0, 255), },    // side 5
			{ 3.0f, 3.0f, -3.0f, D3DCOLOR_ARGB(255, 0, 255, 0), },
			{ 3.0f, -3.0f, 3.0f, D3DCOLOR_ARGB(255, 255, 0, 0), },
			{ 3.0f, 3.0f, 3.0f, D3DCOLOR_ARGB(255, 0, 255, 255), },

			{ -3.0f, -3.0f, -3.0f, D3DCOLOR_ARGB(255, 0, 0, 255), },    // side 6
			{ -3.0f, -3.0f, 3.0f, D3DCOLOR_ARGB(255, 0, 255, 0), },
			{ -3.0f, 3.0f, -3.0f, D3DCOLOR_ARGB(255, 255, 0, 0), },
			{ -3.0f, 3.0f, 3.0f, D3DCOLOR_ARGB(255, 0, 255, 255), },
		};

		// create a vertex buffer interface called v_buffer
		d3ddev->CreateVertexBuffer(24*sizeof(CUSTOMVERTEX),
			0,
			CUSTOMFVF,
			D3DPOOL_MANAGED,
			&v_buffer,
			NULL);

		VOID* pVoid;    // a void pointer

		// lock v_buffer and load the vertices into it
		v_buffer->Lock(0, 0, (void**)&pVoid, 0);
		memcpy(pVoid, vertices, sizeof(vertices));
		v_buffer->Unlock();

		//INDEX BUFFER
		// create the indices using an int array
		short indices[] =
		{
			0, 1, 2,    // side 1
			2, 1, 3,
			4, 5, 6,    // side 2
			6, 5, 7,
			8, 9, 10,    // side 3
			10, 9, 11,
			12, 13, 14,    // side 4
			14, 13, 15,
			16, 17, 18,    // side 5
			18, 17, 19,
			20, 21, 22,    // side 6
			22, 21, 23,
		};

		// create an index buffer interface called i_buffer
		d3ddev->CreateIndexBuffer(36*sizeof(short),
			0,
			D3DFMT_INDEX16,
			D3DPOOL_MANAGED,
			&i_buffer,
			NULL);

		// lock i_buffer and load the indices into it
		i_buffer->Lock(0, 0, (void**)&pVoid, 0);
		memcpy(pVoid, indices, sizeof(indices));
		i_buffer->Unlock();

		return true;
	}
	bool Draw(IDirect3DDevice9 * d3ddev){
		d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);
		d3ddev->SetFVF(CUSTOMFVF);
		// SET UP THE PIPELINE

		D3DXMATRIX matRotateY, matRotateX;    // a matrix to store the rotation information

		static float index = 0.0f; 
		index+=0.01f;    // an ever-increasing float value

		// build a matrix to rotate the model based on the increasing float value


		// tell Direct3D about our matrix
		//d3ddev->SetTransform(D3DTS_WORLD, &matRotate);

		D3DXMATRIX matView;    // the view transform matrix

		D3DXMatrixLookAtLH(&matView,
			&D3DXVECTOR3 (0.0f, 0.0f, 15.0f),    // the camera position
			&D3DXVECTOR3 (0.0f, 0.0f, 0.0f),    // the look-at position
			&D3DXVECTOR3 (0.0f, 1.0f, 0.0f));    // the up direction

		d3ddev->SetTransform(D3DTS_VIEW, &matView);    // set the view transform to matView

		D3DXMATRIX matProjection;     // the projection transform matrix

		D3DXMatrixPerspectiveFovLH(&matProjection,
			D3DXToRadian(45),    // the horizontal field of view
			(FLOAT) settings.Width/ (FLOAT)settings.Height, // aspect ratio
			1.0f,    // the near view-plane
			100.0f);    // the far view-plane

		d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);    // set the projection

		// select the vertex buffer to display
		d3ddev->SetStreamSource(0, v_buffer, 0, sizeof(CUSTOMVERTEX));
		d3ddev->SetIndices(i_buffer);
		D3DXMATRIX matTranslateA;    // a matrix to store the translation for triangle A
		//   D3DXMATRIX matRotateY;    // a matrix to store the rotation for each triangle
		//   static float index = 0.0f; index+=0.05f; // an ever-increasing float value

		// build MULTIPLE matrices to translate the model and one to rotate
		D3DXMatrixTranslation(&matTranslateA, 0.0f, 0.0f, 0.0f);
		D3DXMatrixRotationY(&matRotateY, index);
		D3DXMatrixRotationX(&matRotateX, index/2.0f);

		// tell Direct3D about each world transform, and then draw another triangle
		d3ddev->SetTransform(D3DTS_WORLD, &(matTranslateA * matRotateY * matRotateX));
		d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,0, 0, 8, 0, 12);
		return true;
	}

	void Dispose(){
		v_buffer->Release();
		i_buffer->Release();
	}
};


class TexCubeGraphicsClass : public CubeGraphicsClass{
	LPDIRECT3DTEXTURE9 pTexture;
	LPDIRECT3DINDEXBUFFER9 i_buffer;
public:

#define TEXCUSTOMFVF ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)

	struct TEXCUSTOMVERTEX {D3DXVECTOR3 pos, norm; FLOAT u,v; };
	bool Init(IDirect3DDevice9 * d3ddev, SettingsClass _settings){
		settings=_settings;
		//TEXTURE
		HRESULT hr = D3DXCreateTextureFromResource(d3ddev,NULL,MAKEINTRESOURCE(IDR_RCDATA1),&pTexture);
		//HRESULT hr = D3DXCreateTextureFromFile(d3ddev,L"ChessRook.dds",&pTexture);
		// create the vertices using the CUSTOMVERTEX struct
		TEXCUSTOMVERTEX vertices[] =
		{
			{  D3DXVECTOR3(-3.0f, -3.0f, 3.0f),D3DXVECTOR3(0.0f,0.0f,1.0f), 0.0f,0.0f/6.0f, },    // side 1
			{  D3DXVECTOR3(3.0f, -3.0f, 3.0f),D3DXVECTOR3(0.0f,0.0f,1.0f), 1.0f,0.0f/6.0f, },
			{  D3DXVECTOR3(-3.0f, 3.0f, 3.0f),D3DXVECTOR3(0.0f,0.0f,1.0f), 0.0f,1.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, 3.0f, 3.0f),D3DXVECTOR3(0.0f,0.0f,1.0f), 1.0f,1.0f/6.0f, },

			{  D3DXVECTOR3(-3.0f, -3.0f, -3.0f),D3DXVECTOR3(0.0f,0.0f,-1.0f), 0.0f,1.0f/6.0f, },    // side 2
			{  D3DXVECTOR3(-3.0f, 3.0f, -3.0f),D3DXVECTOR3(0.0f,0.0f,-1.0f), 1.0f,1.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, -3.0f, -3.0f),D3DXVECTOR3(0.0f,0.0f,-1.0f), 0.0f,2.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, 3.0f, -3.0f),D3DXVECTOR3(0.0f,0.0f,-1.0f), 1.0f,2.0f/6.0f, },

			{  D3DXVECTOR3(-3.0f, 3.0f, -3.0f),D3DXVECTOR3(0.0f,1.0f,0.0f), 0.0f,2.0f/6.0f, },    // side 3
			{  D3DXVECTOR3(-3.0f, 3.0f, 3.0f),D3DXVECTOR3(0.0f,1.0f,0.0f), 1.0f,2.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, 3.0f, -3.0f),D3DXVECTOR3(0.0f,1.0f,0.0f), 0.0f,3.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, 3.0f, 3.0f),D3DXVECTOR3(0.0f,1.0f,0.0f), 1.0f,3.0f/6.0f, },

			{  D3DXVECTOR3(-3.0f, -3.0f, -3.0f),D3DXVECTOR3(0.0f,-1.0f,0.0f), 0.0f,3.0f/6.0f, },    // side 4
			{  D3DXVECTOR3(3.0f, -3.0f, -3.0f),D3DXVECTOR3(0.0f,-1.0f,0.0f), 1.0f,3.0f/6.0f, },
			{  D3DXVECTOR3(-3.0f, -3.0f, 3.0f),D3DXVECTOR3(0.0f,-1.0f,0.0f), 0.0f,4.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, -3.0f, 3.0f),D3DXVECTOR3(0.0f,0-1.0f,0.0f), 1.0f,4.0f/6.0f, },

			{  D3DXVECTOR3(3.0f, -3.0f, -3.0f),D3DXVECTOR3(1.0f,0.0f,0.0f), 0.0f,4.0f/6.0f, },    // side 5
			{  D3DXVECTOR3(3.0f, 3.0f, -3.0f),D3DXVECTOR3(1.0f,0.0f,0.0f), 1.0f,4.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, -3.0f, 3.0f),D3DXVECTOR3(1.0f,0.0f,0.0f), 0.0f,5.0f/6.0f, },
			{  D3DXVECTOR3(3.0f, 3.0f, 3.0f),D3DXVECTOR3(1.0f,0.0f,0.0f), 1.0f,5.0f/6.0f, },

			{  D3DXVECTOR3(-3.0f, -3.0f, -3.0f),D3DXVECTOR3(-1.0f,0.0f,0.0f), 0.0f,5.0f/6.0f, },    // side 6
			{  D3DXVECTOR3(-3.0f, -3.0f, 3.0f),D3DXVECTOR3(-1.0f,0.0f,0.0f), 1.0f,5.0f/6.0f, },
			{  D3DXVECTOR3(-3.0f, 3.0f, -3.0f),D3DXVECTOR3(-1.0f,0.0f,0.0f), 0.0f,6.0f/6.0f, },
			{  D3DXVECTOR3(-3.0f, 3.0f, 3.0f),D3DXVECTOR3(-1.0f,0.0f,0.0f), 1.0f,6.0f/6.0f, },
		};

		// create a vertex buffer interface called v_buffer
		d3ddev->CreateVertexBuffer(24*sizeof(TEXCUSTOMVERTEX),
			0,
			TEXCUSTOMFVF,
			D3DPOOL_MANAGED,
			&v_buffer,
			NULL);

		VOID* pVoid;    // a void pointer

		// lock v_buffer and load the vertices into it
		v_buffer->Lock(0, 0, (void**)&pVoid, 0);
		memcpy(pVoid, vertices, sizeof(vertices));
		v_buffer->Unlock();

		//INDEX BUFFER
		// create the indices using an int array
		short indices[] =
		{
			0, 1, 2,    // side 1
			2, 1, 3,
			4, 5, 6,    // side 2
			6, 5, 7,
			8, 9, 10,    // side 3
			10, 9, 11,
			12, 13, 14,    // side 4
			14, 13, 15,
			16, 17, 18,    // side 5
			18, 17, 19,
			20, 21, 22,    // side 6
			22, 21, 23,
		};

		// create an index buffer interface called i_buffer
		d3ddev->CreateIndexBuffer(36*sizeof(short),
			0,
			D3DFMT_INDEX16,
			D3DPOOL_MANAGED,
			&i_buffer,
			NULL);

		// lock i_buffer and load the indices into it
		i_buffer->Lock(0, 0, (void**)&pVoid, 0);
		memcpy(pVoid, indices, sizeof(indices));
		i_buffer->Unlock();

		return true;
	}
	bool Draw(IDirect3DDevice9 * d3ddev){
		//HRESULT hr = d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);

		d3ddev->SetFVF(TEXCUSTOMFVF);
		HRESULT hr = d3ddev->SetTexture(0,pTexture);

		d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);    // turn off the 3D lighting
		d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
		d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
		//d3ddev->SetTextureStageState(0,D3DTSS_TEXCOORDINDEX,0);
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1,D3DTA_TEXTURE);
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);
		d3ddev->SetTextureStageState(0, D3DTSS_COLOROP,  D3DTOP_MODULATE);
		d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP,  D3DTOP_DISABLE);
		//Get Filtrating Settings
		switch(settings.TextureFilter){
			case 0:
				d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE); break;
			case -1:
				d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT); break;
			case -2:
				d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); break;
			case 1: case 2: case 4: case 8: case 16: 
				d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC); 
				d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, settings.TextureFilter); break;
		}
	// SET UP THE PIPELINE

	D3DXMATRIX matRotateY, matRotateX;    // a matrix to store the rotation information

	static float index = 0.0f; 
	index+=0.01f;    // an ever-increasing float value

	// build a matrix to rotate the model based on the increasing float value


	// tell Direct3D about our matrix
	//d3ddev->SetTransform(D3DTS_WORLD, &matRotate);

	D3DXMATRIX matView;    // the view transform matrix

	D3DXMatrixLookAtLH(&matView,
		&D3DXVECTOR3 (0.0f, 0.0f, 15.0f),    // the camera position
		&D3DXVECTOR3 (0.0f, 0.0f, 0.0f),    // the look-at position
		&D3DXVECTOR3 (0.0f, 1.0f, 0.0f));    // the up direction

	d3ddev->SetTransform(D3DTS_VIEW, &matView);    // set the view transform to matView

	D3DXMATRIX matProjection;     // the projection transform matrix

	D3DXMatrixPerspectiveFovLH(&matProjection,
		D3DXToRadian(45),    // the horizontal field of view
		(FLOAT) settings.Width/ (FLOAT)settings.Height, // aspect ratio
		1.0f,    // the near view-plane
		100.0f);    // the far view-plane

	d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);    // set the projection

	// select the vertex buffer to display
	d3ddev->SetStreamSource(0, v_buffer, 0, sizeof(TEXCUSTOMVERTEX));
	d3ddev->SetIndices(i_buffer);
	D3DXMATRIX matTranslateA;    // a matrix to store the translation for triangle A
	//   D3DXMATRIX matRotateY;    // a matrix to store the rotation for each triangle
	//   static float index = 0.0f; index+=0.05f; // an ever-increasing float value

	// build MULTIPLE matrices to translate the model and one to rotate
	D3DXMatrixTranslation(&matTranslateA, 0.0f, 0.0f, 0.0f);
	D3DXMatrixRotationY(&matRotateY, index);
	D3DXMatrixRotationX(&matRotateX, index/2.0f);

	// tell Direct3D about each world transform, and then draw another triangle
	d3ddev->SetTransform(D3DTS_WORLD, &(matTranslateA * matRotateY * matRotateX));
	d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);
	return true;
}

void Dispose(){
	if(v_buffer) v_buffer->Release();
	v_buffer=NULL;
	if(i_buffer) i_buffer->Release();
	i_buffer=NULL;
	if(pTexture) pTexture->Release();
	pTexture=NULL;
}
};