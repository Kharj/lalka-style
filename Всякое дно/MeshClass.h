#pragma once

class MeshClass
{
	string Name;
public:

	LPD3DXBUFFER materialBuffer;
	DWORD numMaterials; 
	LPD3DXMESH mesh;
	D3DMATERIAL9 *meshMaterials;
	LPDIRECT3DTEXTURE9 *meshTextures;
	VECTOR3X3 prop;
	VECTOR3X3 defaultProp;
	LoadingStat loadingStat;


	void SetName(string a);
	string GetName();
#pragma region props
	void SetDefaultProps(const VECTOR3X3 &a);
	void SetDefaultProps(D3DXVECTOR3 p, D3DXVECTOR3 r, D3DXVECTOR3 s);
	void SetDefaultProps(float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z, float sc_x, float sc_y, float sc_z);
	void SetProps(const VECTOR3X3 &a);
	void SetProps(D3DXVECTOR3 p, D3DXVECTOR3 r, D3DXVECTOR3 s);
	void SetProps(float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z, float sc_x, float sc_y, float sc_z);

	void SetPosition(D3DXVECTOR3 a);
	void SetPosition(float x, float y, float z);
	void SetRotation(D3DXVECTOR3 a);
	void SetRotation(float x, float y, float z);
	void SetScale(D3DXVECTOR3 a);
	void SetScale(float x, float y, float z);
#pragma endregion
	bool Draw(LPDIRECT3DDEVICE9 d3ddev);
	bool LoadMaterials(int num, D3DMATERIAL9 * mi);
	bool LoadFromMesh(LPD3DXMESH m, LPDIRECT3DDEVICE9 d3ddev);
	bool LoadFromX(LPCWSTR filename, LPDIRECT3DDEVICE9 d3ddev);
	void Release();
	MeshClass(void);
	~MeshClass(void);
};
