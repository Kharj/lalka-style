#include "StdAfx.h"
#include "CameraClass.h"

CameraClass::CameraClass(void)
{
	changedView=true;
	changedProjection=true;
	changedTransform=true;
	//prop=DefaultVector3x3;//defaultprop
	Eye = D3DXVECTOR3 (0.0f, 0.0f, -20.0f);// the camera position
	At = D3DXVECTOR3 (0.0f, 0.0f, 0.0f);// the look-at position
	Up = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);// the up direction
	fovy = D3DXToRadian(45); // the horizontal field of view
	Aspect = 4.0f/ 3.0f; // aspect ratio
	zn = 1.0f; // the near view-plane
	zf = 100.0f; // the far view-plane
}

CameraClass::~CameraClass(void)
{
}

D3DXMATRIX * CameraClass::GetViewMatrix(){
	if(changedView){
		D3DXMatrixLookAtLH(&matView, &Eye, &At, &Up);    // the up direction
		changedView=false;
	}
	return &matView;
}
D3DXMATRIX * CameraClass::GetProjectionMatrix(){
	if(changedProjection){
		D3DXMatrixPerspectiveFovLH(&matProjection, fovy, Aspect, zn, zf);
		changedProjection=false;
	}
	return &matProjection;
}
void CameraClass::SetView(D3DXVECTOR3 _Eye, D3DXVECTOR3 _At, D3DXVECTOR3 _Up ){
	Eye=_Eye;
	At=_At;
	Up=_Up;
	changedView=true;
}
void CameraClass::SetLookAt(D3DXVECTOR3 _At){
	At=_At;
	changedView=true;
}
void CameraClass::SetEyePosition(D3DXVECTOR3 _Eye){
	Eye=_Eye;
	changedView=true;
}
void CameraClass::SetUpDirection(D3DXVECTOR3 _Up){
	Up=_Up;
	changedView=true;
}
void CameraClass::SetProjection(FLOAT _fovy, FLOAT _Aspect, FLOAT _zn, FLOAT _zf ){
	fovy=_fovy;
	Aspect=_Aspect;
	zn=_zn;
	zf=_zf;
	changedProjection=true;
}
void CameraClass::SetFovy(FLOAT _fovy){
	fovy=_fovy;
	changedView=true;
}
#pragma region Properties_methods

void CameraClass::SetProps(const VECTOR3X3 &a){
	prop=a;
	changedTransform=true;
}

void CameraClass::SetProps(D3DXVECTOR3 p, D3DXVECTOR3 r, D3DXVECTOR3 s){
	prop.Position=p;
	prop.Rotation=r;
	prop.Scale=s;
	changedTransform=true;
}
void CameraClass::SetProps(float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z, float sc_x, float sc_y, float sc_z){
	prop.Position.x=pos_x; prop.Position.y=pos_y; prop.Position.z=pos_z; 
	prop.Rotation.x=rot_x; prop.Rotation.y=rot_y; prop.Rotation.z=rot_z; 
	prop.Scale.x=sc_x; prop.Scale.y=sc_y; prop.Scale.z=sc_z; 
	changedTransform=true;
}
void CameraClass::SetPosition(D3DXVECTOR3 a){
	prop.Position=a;
	changedTransform=true;
}
void CameraClass::SetPosition(float x, float y, float z){
	prop.Position=D3DXVECTOR3(x,y,z);
	changedTransform=true;
}
void CameraClass::SetRotation(D3DXVECTOR3 a){
	prop.Rotation=a;
	changedTransform=true;
}
void CameraClass::SetRotation(float x, float y, float z){
	prop.Rotation=D3DXVECTOR3(x,y,z);
	changedTransform=true;
}
void CameraClass::SetScale(D3DXVECTOR3 a){
	prop.Scale=a;
	changedTransform=true;
}
void CameraClass::SetScale(float x, float y, float z){
	prop.Scale=D3DXVECTOR3(x,y,z);
	changedTransform=true;
}
#pragma endregion 
VECTOR3X3 CameraClass::GetProps(){
	return prop;
}
D3DXMATRIX * CameraClass::GetTransformedViewMatrix(){
	GetViewMatrix();//����� ���� �������� ���_����
	if(changedTransform){
		D3DXMATRIX matRotateX, matRotateY, matRotateZ, matScale, matTranslate;

		D3DXMatrixScaling(&matScale, prop.Scale.x, prop.Scale.y, prop.Scale.z);
		D3DXMatrixRotationX(&matRotateX, prop.Rotation.x);
		D3DXMatrixRotationY(&matRotateY, prop.Rotation.y);
		D3DXMatrixRotationZ(&matRotateZ, prop.Rotation.z);
		D3DXMatrixTranslation(&matTranslate, prop.Position.x, prop.Position.y, prop.Position.z);
		D3DXMatrixMultiply(&matTransform, &matScale, &matRotateX);
		D3DXMatrixMultiply(&matTransform, &matTransform, &matRotateY);
		D3DXMatrixMultiply(&matTransform, &matTransform, &matRotateZ);
		D3DXMatrixMultiply(&matTransform, &matTransform, &matTranslate);
		changedTransform=false;
	}
	D3DXMatrixMultiply(&matViewTransformed, &matView, &matTransform);
	return &matViewTransformed;
}