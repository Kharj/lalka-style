#pragma once

class CameraClass
{
	D3DXMATRIX matProjection;    // a matrix to store the rotation information
	D3DXMATRIX matView;    // the view transform matrix
	D3DXMATRIX matViewTransformed;    // the view transform matrix transformed
	D3DXVECTOR3 Eye;// the camera position
	D3DXVECTOR3 At;// the look-at position
	D3DXVECTOR3 Up;// the up direction
	D3DXMATRIX matTransform;
	VECTOR3X3 prop;
	FLOAT fovy; // the horizontal field of view
	FLOAT Aspect; // aspect ratio
	FLOAT zn; // the near view-plane
	FLOAT zf; // the far view-plane
	bool changedView, changedProjection, changedTransform;

public:

	D3DXMATRIX * GetViewMatrix();
	D3DXMATRIX * GetTransformedViewMatrix();
	D3DXMATRIX * GetProjectionMatrix();
	
	void SetView(D3DXVECTOR3 _Eye, D3DXVECTOR3 _At, D3DXVECTOR3 _Up );
	void SetLookAt(D3DXVECTOR3 _At);
	void SetEyePosition(D3DXVECTOR3 _Eye);
	void SetUpDirection(D3DXVECTOR3 _Up);
	void SetProjection(FLOAT _fovy, FLOAT _Aspect, FLOAT _zn, FLOAT _zf );
	void SetFovy(FLOAT _fovy);
	// prop methods
	void SetPosition(D3DXVECTOR3 a);
	void SetPosition(float x, float y, float z);
	void SetRotation(D3DXVECTOR3 a);
	void SetRotation(float x, float y, float z);
	void SetScale(D3DXVECTOR3 a);
	void SetScale(float x, float y, float z);
	void SetProps(const VECTOR3X3 &a);
	void SetProps(D3DXVECTOR3 p, D3DXVECTOR3 r, D3DXVECTOR3 s);
	void SetProps(float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z, float sc_x, float sc_y, float sc_z);
	VECTOR3X3 GetProps();
	CameraClass(void);
	~CameraClass(void);
};
