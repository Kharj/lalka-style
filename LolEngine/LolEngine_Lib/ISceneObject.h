#pragma once

namespace LolEngine{
	class ISceneObject
	{
	protected:
		CGraphicsEngine* graphicsEngine;
		CSoundEngine* soundEngine;
		CInputEngine* inputEngine;
		CNetworkEngine* networkEngine;
		//properties
		D3DXVECTOR3 position;
		D3DXVECTOR3 rotation;
		D3DXVECTOR3 scale;
		std::string name;
		std::vector <IMesh*> meshes;//to delete objects
	public:
		ISceneObject(void);
		virtual ~ISceneObject(void);
		bool visible;
		void AddMesh(IMesh *mesh);
		int GetMeshCount();
		IMesh* GetMesh(int num);
		virtual bool Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, LolEngine::CNetworkEngine* _networkEngine);
		virtual void Update(float deltaTime);
		virtual void Draw();
		virtual void Dispose();
#pragma region Transform
		D3DXVECTOR3 GetPosition();
		D3DXVECTOR3 GetRotation();
		D3DXVECTOR3 GetScale();
		void SetPosition(D3DXVECTOR3 _position);
		void SetRotation(D3DXVECTOR3 _rotation);
		void SetScale(D3DXVECTOR3 _scale);
#pragma endregion
	};
}
