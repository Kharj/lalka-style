#include "stdafx.h"
#include "LolEngine.h"
#include "ILight.h"


namespace LolEngine{
	ILight::ILight(void)
	{
		ZeroMemory(&dLight, sizeof(dLight));
		On = true;

	}
	//full light
	ILight::ILight(D3DLIGHTTYPE Type, D3DVECTOR Position, D3DVECTOR Direction, D3DCOLORVALUE Diffuse, D3DCOLORVALUE Specular, D3DCOLORVALUE Ambient, float Falloff, float Range, float Phi, float Theta){
		ZeroMemory(&dLight, sizeof(dLight));
		dLight.Type=Type;
		dLight.Position=Position;
		dLight.Direction=Direction;
		dLight.Diffuse=Diffuse;
		dLight.Specular=Specular;
		dLight.Ambient=Ambient;
		dLight.Falloff = Falloff;
		dLight.Range=Range;
		dLight.Phi=Phi;
		dLight.Theta=Theta;
		On = true;
	}
	//simple directional
	ILight::ILight(D3DVECTOR Position, D3DVECTOR Direction, D3DCOLORVALUE allcolors){
		ZeroMemory(&dLight, sizeof(dLight));
		dLight.Type=D3DLIGHT_DIRECTIONAL;
		dLight.Position=Position;
		dLight.Direction=Direction;
		dLight.Diffuse=allcolors;
		dLight.Specular=allcolors;
		dLight.Ambient=allcolors;
		dLight.Range=100.0f;
		On = true;
	}

	ILight::~ILight(void)
	{
	}
	D3DLIGHT9 * ILight::GetLightPointer(){
		return &dLight;
	}
	void ILight::SetAmbientColor(D3DXCOLOR color){
		dLight.Ambient=color;
	}
	void ILight::SetDiffuseColor(D3DXCOLOR color){
		dLight.Diffuse=color;
	}
	void ILight::SetSpecularColor(D3DXCOLOR color){
		dLight.Specular=color;
	}
	void ILight::SetAllColors(D3DXCOLOR color){
		dLight.Ambient=color;
		dLight.Diffuse=color;
		dLight.Specular=color;
	}

	void ILight::SetPosition(D3DXVECTOR3 pos){
		dLight.Position = pos;
	}
	void ILight::SetDirection(D3DXVECTOR3 dir){
		dLight.Direction = dir;
	}
	D3DXVECTOR3 ILight::GetPosition(){
		return dLight.Position;
	}
	D3DXVECTOR3 ILight::GetDirection(){
		return dLight.Direction;
	}
}