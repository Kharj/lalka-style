#pragma once
namespace LolEngine{
class CGraphicsEngine
{
protected:
	CSettings *pSettings;
	D3DPRESENT_PARAMETERS d3dpp;
	IDirect3D9 *pD3D;
	IDirect3DDevice9 *d3ddev;
	D3DCAPS9 deviceCaps;
	bool deviceLost;
	ENG_STATUS status;

	bool Reset();
public:
	CGraphicsEngine(void);
	~CGraphicsEngine(void);
	bool Init(HWND hWnd, CSettings *_pSetings);
	void Dispose();
	void BeginDraw();
	void EndDraw();
	void Update(float deltaTime);
	LPDIRECT3DDEVICE9 GetDevice();
	D3DCAPS9 GetDeviceCaps();

};
}

