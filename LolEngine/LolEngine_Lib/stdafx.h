// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // ��������� ����� ������������ ���������� �� ���������� Windows

//Windows
#include <windows.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <process.h>
#include <string.h>
#include <time.h>
//STL
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <sstream>
//DX
#include <D3D9.h>
#include <D3DX9Shader.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <Xinput.h>
//Sound
#include <mmsystem.h>
//Net
#include <WinSock2.h>
#include <WS2tcpip.h>
//Libs
#pragma comment(lib, "D3d9.lib")
#pragma comment(lib, "D3dx9.lib")
#pragma comment(lib, "Winmm.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "XInput.lib")
#pragma comment(lib, "Ws2_32.lib")

//Engine Status Enum
namespace LolEngine{
#define ASPECT_RATIO_DEFAULT_MULTIPLIER 1.77f
	enum ENG_STATUS{
		ENG_OK = 0, ENG_EXITAPP, ENG_NEXTSCENE,
		ENG_NOTINIT, ENG_NEEDRESET, ENG_NEXTSCENE_TYPE1, ENG_NEXTSCENE_TYPE2, ENG_TOMENU, ENG_CONNECTED,
		ENG_NOTCONNECTED, ENG_CONNECTWAIT};
}
// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
