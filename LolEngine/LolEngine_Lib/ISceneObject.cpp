#include "stdafx.h"
#include "LolEngine.h"
#include "ISceneObject.h"

namespace LolEngine{
	ISceneObject::ISceneObject(void)
	{	
		//vectors
		position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
		visible = true;
		graphicsEngine = NULL;
		soundEngine = NULL;
		inputEngine = NULL;
		networkEngine = NULL;
	}

	ISceneObject::~ISceneObject(void)
	{
	}

	void ISceneObject::AddMesh(IMesh *mesh){
		if(!mesh) return;
		meshes.push_back(mesh);
	}

	int ISceneObject::GetMeshCount(){
		return meshes.size();
	}
	IMesh* ISceneObject::GetMesh(int num){
		if(num >= (int)meshes.size()) return NULL;
		return meshes[num];
	}
	bool ISceneObject::Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, LolEngine::CNetworkEngine* _networkEngine){
		//Save engines poiners
		graphicsEngine = _graphicsEngine;
		soundEngine = _soundEngine;
		inputEngine = _inputEngine;
		return true;
	}
	void ISceneObject::Update(float deltaTime){

	}
	void ISceneObject::Draw(){
		//Set enviroment
		HRESULT hr=0;
		LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();	
		d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
		d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);

		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1,D3DTA_TEXTURE);
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);
		d3ddev->SetTextureStageState(0, D3DTSS_COLOROP,  D3DTOP_SELECTARG1);

		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );
		d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP,  D3DTOP_MODULATE);




		//Meshes
		if(!visible || meshes.empty()) return;
		for(std::vector<IMesh*>::iterator i=meshes.begin(); i!=meshes.end(); ++i){
			IMesh *currentMesh = (*i);
			if(!currentMesh) continue;
			currentMesh->Draw(graphicsEngine->GetDevice(), position, rotation, scale);		
		}

	}
	void ISceneObject::Dispose(){
		//delet all added meshes
		if(!meshes.empty()){
			for(std::vector<IMesh*>::iterator i=meshes.begin(); i!=meshes.end(); ++i){
				if(!(*i)) continue;
				(*i)->Dispose();			
			}
			meshes.clear();
		}
	}
#pragma region Transform
	D3DXVECTOR3 ISceneObject::GetPosition(){
		return position;
	}
	D3DXVECTOR3 ISceneObject::GetRotation(){
		return rotation;
	}
	D3DXVECTOR3 ISceneObject::GetScale(){
		return scale;
	}
	void ISceneObject::SetPosition(D3DXVECTOR3 _position){
		position = _position;
	}
	void ISceneObject::SetRotation(D3DXVECTOR3 _rotation){
		rotation = _rotation;
	}
	void ISceneObject::SetScale(D3DXVECTOR3 _scale){
		scale = _scale;
	}
#pragma endregion
}