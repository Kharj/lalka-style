#pragma once
namespace LolEngine{
	class CInputEngine
	{
	public:
		CInputEngine(void);
		~CInputEngine(void);
		
		static const int xinput_dead_zone = 5000; //dead zone for xinput sticks
		bool Init(HWND hWnd, CSettings *_pSetings);
		void Dispose();
		void GetMouseLocation(int&, int&);
		bool Update();
		bool IsKeyPressed(int diK);
		bool IsMouseButtonPressed(int num);
		XINPUT_STATE GetXInputState(int playerNum);
		bool IsXInputButtonPressed(int playerNum, WORD xinput_gamepad_button);
		void XInputVibrate(int playerNum, WORD leftVal = 0, WORD rightVal = 0);
		bool IsXInputDeviceConnected(int playerNum);
		//

	protected:
		static const int xinput_device_count = 4;
		IDirectInput8* directInput;
		IDirectInputDevice8* keyboard;
		IDirectInputDevice8* mouse;
		CSettings *settings;

		DIMOUSESTATE mouseState;
		unsigned char keyboardState[256];
		int mouseX, mouseY;
		CXInputController *xinput_controller[xinput_device_count];
		XINPUT_STATE xinput_state[xinput_device_count];
	};
}

