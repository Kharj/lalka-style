#pragma once
namespace LolEngine{
	class ICamera
	{
	protected:
		D3DXMATRIX matProjection;    // a matrix to store the rotation information
		D3DXMATRIX matView;    // the view transform matrix
		D3DXMATRIX matViewTransformed;    // the view transform matrix transformed
		D3DXVECTOR3 Eye;// the camera position
		D3DXVECTOR3 At;// the look-at position
		D3DXVECTOR3 Up;// the up direction
		D3DXMATRIX matTransform;
		D3DXVECTOR3 position;
		D3DXVECTOR3 rotation;
		D3DXVECTOR3 scale;
		FLOAT fovy; // the horizontal field of view
		FLOAT Aspect; // aspect ratio
		FLOAT zn; // the near view-plane
		FLOAT zf; // the far view-plane
	public:
		ICamera(void);
		~ICamera(void);

		D3DXMATRIX * GetViewMatrix();
		D3DXMATRIX * GetTransformedViewMatrix();
		D3DXMATRIX * GetProjectionMatrix();
		D3DXVECTOR3  GetLookAt();
		D3DXVECTOR3  GetEyePosition();
		void SetView(D3DXVECTOR3 _Eye, D3DXVECTOR3 _At, D3DXVECTOR3 _Up );
		void SetLookAt(D3DXVECTOR3 _At);
		void SetEyePosition(D3DXVECTOR3 _Eye);
		void SetUpDirection(D3DXVECTOR3 _Up);
		void SetProjection(FLOAT _fovy, FLOAT _Aspect, FLOAT _zn, FLOAT _zf );
		void SetFovy(FLOAT _fovy);
		void Reset();

#pragma region Transform
		D3DXVECTOR3 GetPosition();
		D3DXVECTOR3 GetRotation();
		D3DXVECTOR3 GetScale();
		void SetPosition(D3DXVECTOR3 _position);
		void SetRotation(D3DXVECTOR3 _rotation);
		void SetScale(D3DXVECTOR3 _scale);
#pragma endregion
	};
}

