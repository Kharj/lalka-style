#pragma once
namespace LolEngine{
	class IMesh
	{
	protected:
		//main
		LPD3DXBUFFER materialBuffer;
		DWORD numMaterials; 
		LPD3DXMESH mesh;
		D3DMATERIAL9 *meshMaterials;
		LPDIRECT3DTEXTURE9 *meshTextures;
		DWORD textureState_colorop, textureState_colorarg1, textureState_colorarg2, textureState_alphaop, textureState_alphaarg1, textureState_alphaarg2;
		//properties
		D3DXVECTOR3 position;
		D3DXVECTOR3 rotation;
		D3DXVECTOR3 scale;
		std::string name;
	public:
		IMesh(void);
		virtual ~IMesh(void);
		virtual void Dispose();

		bool visible;

		virtual bool Draw(LPDIRECT3DDEVICE9 d3ddev, D3DXVECTOR3 objectPosition, D3DXVECTOR3 objectRotation, D3DXVECTOR3 objectScale);
		bool LoadMaterials(int num, D3DMATERIAL9 * mi);
		bool LoadMaterials(LPDIRECT3DDEVICE9 d3ddev, int num, D3DXMATERIAL * mi);//with texture filenames
		bool SetTexture(int num, LPDIRECT3DTEXTURE9 tex);
		bool LoadFromMesh(LPD3DXMESH m, LPDIRECT3DDEVICE9 d3ddev);
		bool LoadTexture(LPCSTR filename, LPDIRECT3DDEVICE9 d3ddev, int textureNum, int subset=-1);
		bool LoadFromX(LPCSTR filename, LPDIRECT3DDEVICE9 d3ddev);
		D3DMATERIAL9 *GetMaterial(int num);
		void SetStateColor(DWORD colorarg1, DWORD colorarg2, DWORD colorop);
		void SetStateAlpha(DWORD alphaarg1, DWORD alphaarg2, DWORD alphaop);

#pragma region Transform
		D3DXVECTOR3 GetPosition();
		D3DXVECTOR3 GetRotation();
		D3DXVECTOR3 GetScale();
		void SetPosition(D3DXVECTOR3 _position);
		void SetRotation(D3DXVECTOR3 _rotation);
		void SetScale(D3DXVECTOR3 _scale);
#pragma endregion
		//Name
		std::string GetName();
		void SetName(std::string);
	};
}
