#include "stdafx.h"
#include "LolEngine.h"
#include "InputEngine.h"

namespace LolEngine{
	CInputEngine::CInputEngine(void)
	{
		mouseX = 0;
		mouseY = 0;
		directInput = NULL;
		keyboard = NULL;
		mouse = NULL;
		settings = NULL;
		ZeroMemory(&keyboardState, sizeof(keyboardState));
		ZeroMemory(xinput_controller, sizeof(xinput_controller));
		ZeroMemory(xinput_state, sizeof(xinput_state));
	}


	CInputEngine::~CInputEngine(void)
	{
	}

	bool CInputEngine::Init(HWND hWnd, CSettings *_pSetings){
		HINSTANCE hInstance = (HINSTANCE)GetModuleHandle(NULL);
		HRESULT result;
		settings = _pSetings;
		mouseX = 0;
		mouseY = 0;

		// Initialize the main direct input interface.
		result = DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directInput, NULL);
		if(FAILED(result)){
			return false;
		}

		// Initialize the direct input interface for the keyboard.
		result = directInput->CreateDevice(GUID_SysKeyboard, &keyboard, NULL);
		if(FAILED(result)){
			return false;
		}

		// Set the data format.  In this case since it is a keyboard we can use the predefined data format.
		result = keyboard->SetDataFormat(&c_dfDIKeyboard);
		if(FAILED(result)){
			return false;
		}

		// Set the cooperative level of the keyboard to not share with other programs.
		result = keyboard->SetCooperativeLevel(hWnd, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
		if(FAILED(result)){
			return false;
		}

		// Now acquire the keyboard.
		result = keyboard->Acquire();
		if(FAILED(result)){
			Log::Write(L"Keybord Acquire error");
			return false;
		}

		// Initialize the direct input interface for the mouse.
		result = directInput->CreateDevice(GUID_SysMouse, &mouse, NULL);
		if(FAILED(result)){
			return false;
		}

		// Set the data format for the mouse using the pre-defined mouse data format.
		result = mouse->SetDataFormat(&c_dfDIMouse);
		if(FAILED(result)){
			return false;
		}

		// Set the cooperative level of the mouse to share with other programs.
		result = mouse->SetCooperativeLevel(hWnd, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
		if(FAILED(result)){
			return false;
		}

		// Acquire the mouse.
		result = mouse->Acquire();
		if(FAILED(result)){
			Log::Write(L"Mouse Acquire error");
			return false;
		}
		//init XInput
		if(settings->XInput){
			for(int i=0;i<xinput_device_count;++i)
			xinput_controller[i] = new CXInputController(i);
		}
		return true;
	}
	void CInputEngine::Dispose(){

		if(mouse){
			mouse->Unacquire();
			mouse->Release();
			mouse = 0;
		}

		if(keyboard){
			keyboard->Unacquire();
			keyboard->Release();
			keyboard = 0;
		}

		// Release the main interface to direct input.
		if(directInput){
			directInput->Release();
			directInput = 0;
		}
		//release xinput
		for(int i=0;i<xinput_device_count;++i){
			if(xinput_controller[i]){
				delete xinput_controller[i];
				xinput_controller[i] = NULL;
			}
		}
	}

	void CInputEngine::GetMouseLocation(int& x, int& y){
		x = mouseX;
		y = mouseY;
	}
	bool CInputEngine::Update(){
		if(!directInput) return false;

		HRESULT result;
		// Read the keyboard device.
		result = keyboard->GetDeviceState(sizeof(keyboardState), (LPVOID)&keyboardState);
		if(FAILED(result)){
			// If the keyboard lost focus or was not acquired then try to get control back.
			if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED)){
				keyboard->Acquire();
			}else{
				return false;
			}
		}
		// Read the mouse device.
		result = mouse->GetDeviceState(sizeof(mouseState), (LPVOID)&mouseState);
		if(FAILED(result)){
			// If the mouse lost focus or was not acquired then try to get control back.
			if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED)){
				mouse->Acquire();
			}else{
				return false;
			}
		}
		// Update the location of the mouse cursor based on the change of the mouse location during the frame.
		mouseX = mouseState.lX;
		mouseY = mouseState.lY;

		// Ensure the mouse location doesn't exceed the screen width or height.
		//if(mouseX < 0) mouseX = 0;
		//if(mouseY < 0) mouseY = 0;
		//if(mouseX > screenWidth)  { mouseX = screenWidth; }
		//if(mouseY > screenHeight) { mouseY = screenHeight; }
		
		//Read XInput
		for(int i=0;i<xinput_device_count;++i){
			ZeroMemory(&xinput_state[i], sizeof(XINPUT_STATE));
			if(xinput_controller[i]){ //if inited
				if(xinput_controller[i]->IsConnected()){
					xinput_state[i] = xinput_controller[i]->GetState();
				}
			}
		}

		return true;
	}

	bool CInputEngine::IsKeyPressed(int diK){
		if(keyboardState[diK] & 0x80){
			return true;
		}
		return false;
	}
	bool CInputEngine::IsXInputButtonPressed(int playerNum, WORD xinput_gamepad_button){
		if(playerNum>xinput_device_count) return false;
		if(!xinput_controller[playerNum]) return false;//not inited
		if(xinput_state[playerNum].Gamepad.wButtons & xinput_gamepad_button) return true;
		return false;
	}
	XINPUT_STATE CInputEngine::GetXInputState(int playerNum){
		if(playerNum>xinput_device_count) playerNum=0;
		return xinput_state[playerNum];
	}
	void CInputEngine::XInputVibrate(int playerNum, WORD leftVal, WORD rightVal){
		if(playerNum>xinput_device_count) return;
		if(!xinput_controller[playerNum]) return;//not inited
		if(xinput_controller[playerNum]->IsConnected()){
			xinput_controller[playerNum]->Vibrate(leftVal, rightVal);
		}
	}
	bool CInputEngine::IsXInputDeviceConnected(int playerNum){
		if(playerNum>xinput_device_count) playerNum=0;
		if(!xinput_controller[playerNum]) return false;//not inited
		return xinput_controller[playerNum]->IsConnected();
	}
	
	bool CInputEngine::IsMouseButtonPressed(int num){
		if(num < 0 && num>3) return false;
		return ((mouseState.rgbButtons[num] & 0x80) != 0);
	}
}//namespace