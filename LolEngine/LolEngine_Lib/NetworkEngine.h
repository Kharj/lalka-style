#pragma once
namespace LolEngine{
	const int DEFAULT_BUF_SIZE = 1024;
	const char IP_FILENAME[] = "ip.txt";
	class CNetworkEngine
	{
	protected:
		//ping
		float pingValue;
		std::mutex pingMutex;

		std::string serverAddress;
		CSettings *settings;
		std::thread *clientThread;
		ENG_STATUS status;

		//Shared
		SOCKET clientSocket;
		struct sockaddr_in serverAddr;
		char sendBuf[DEFAULT_BUF_SIZE];
		char recvBuf[DEFAULT_BUF_SIZE];
		bool waitingSend, waitingRecv;
		std::mutex sendMutex, recvMutex;

		//End Shared

		//networkFunc
		static void NetworkFunc(CNetworkEngine * _engine);
	public:
		CNetworkEngine(void);
		~CNetworkEngine(void);


		std::string GetServerAddress();
		ENG_STATUS GetStatus();
		void SetStatus(ENG_STATUS _status);
		void GetConectionData(SOCKET & _clintSocket, struct sockaddr_in & _serverAddr, char *& _sendBuf, char *& _recvBuf, bool *& _waitingSend, bool *& _waitingRecv, std::mutex *& _sendMutex, std::mutex *& _recvMutex, float *& _pingValue, std::mutex *& _pingMutex);
		bool Connect(std::string address);
		void Disconnect();
		bool Send(std::string str);
		bool Read(std::string &res);

		bool Init(HWND hWnd, CSettings *_pSettings);
		void Update(float deltaTime);
		void Dispose();
		int GetPing();
	};
}

