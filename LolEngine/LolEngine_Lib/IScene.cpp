
#include "stdafx.h"
#include "LolEngine.h"
#include "IScene.h"

namespace LolEngine{
	IScene::IScene(void)
	{
		graphicsEngine = NULL;
		soundEngine = NULL;
		inputEngine = NULL;
		networkEngine = NULL;
		settings = NULL;
		//currentCamera = NULL;
		status = ENG_NOTINIT;
		lightCounter = 0;

	}
	IScene::~IScene(void)
	{
	}

	void IScene::SetCamera(ICamera *pCamera){
		if(pCamera){
			LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();
			d3ddev->SetTransform(D3DTS_VIEW, pCamera->GetTransformedViewMatrix());    // set the view transform to matView
			d3ddev->SetTransform(D3DTS_PROJECTION, pCamera->GetProjectionMatrix());    // set the projection
		}
	}

	void IScene::SetLight(ILight* iLight){
		if(iLight->On){
			LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();
			d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);    // turn off the 3D lighting
			d3ddev->SetLight(lightCounter, iLight->GetLightPointer());
			d3ddev->LightEnable(lightCounter,TRUE);
			lightCounter++;
		}
	}
	void IScene::SetLight(D3DLIGHT9* d3dLight){
		LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();
		d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);    // turn off the 3D lighting
		d3ddev->SetLight(lightCounter, d3dLight);
		d3ddev->LightEnable(lightCounter,TRUE);
		lightCounter++;
	}
	bool IScene::Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, CNetworkEngine* _networkEngine, CSettings* _pSettings){
		//Save engines poiners
		graphicsEngine = _graphicsEngine;
		soundEngine = _soundEngine;
		inputEngine = _inputEngine;
		networkEngine = _networkEngine;
		settings = _pSettings;

		return true;
	}
	void IScene::Update(float deltaTime){

	}

	/*void IScene::Draw(){
	graphicsEngine->BeginDraw();
	//render states
	SetCamera(camera);
	//Draw nothing
	graphicsEngine->EndDraw();
	}*/
	void IScene::Dispose(){
		//Save engines poiners
		graphicsEngine = NULL;
		soundEngine = NULL;
		inputEngine = NULL;
		networkEngine = NULL;

	}
	ENG_STATUS IScene::GetStatus(){
		return status;
	}
}