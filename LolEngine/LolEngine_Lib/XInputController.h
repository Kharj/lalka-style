#pragma once
namespace LolEngine{
	class CXInputController
	{
	private:
		XINPUT_STATE _controllerState;
		int _controllerNum;
	public:
		CXInputController(int playerNumber);
		XINPUT_STATE GetState();
		bool IsConnected();
		void Vibrate(WORD leftVal = 0, WORD rightVal = 0);
	};
}
