#include "stdafx.h"
#include "LolEngine.h"
#include "NetworkEngine.h"

namespace LolEngine{
	CNetworkEngine::CNetworkEngine(void)
	{
		settings = NULL;
		clientThread = NULL;
		clientSocket = NULL;
		waitingSend = false;
		waitingRecv = false;
		serverAddress = "127.0.0.1";
		status = ENG_NOTCONNECTED;
		pingValue = 0.0f;
	}


	CNetworkEngine::~CNetworkEngine(void)
	{
	}

	bool CNetworkEngine::Init(HWND hWnd, CSettings *_pSettings){
		settings = _pSettings;
		//read saved ip.
		std::ifstream fs(IP_FILENAME);
		if(fs){ //opened
			char tmpStr[100];
			fs.getline(tmpStr, 100);
			fs.close();
			//check ip format
			int ip_0, ip_1, ip_2, ip_3;
			sscanf_s(tmpStr, "%d%*c%d%*c%d%*c%d", &ip_0, &ip_1, &ip_2, &ip_3);

			if(ip_0<=0 || ip_0>255 ||ip_1<0 || ip_1>255 || ip_2<0 || ip_2>255 || ip_3<=0 || ip_3>255 ){
				//Wrong ip
				Log::Write("Wrong IP readed from ip.txt." );
			}else{
				//ok, use readed ip
				serverAddress = tmpStr;
			}
		}
		return true;
	}
	void CNetworkEngine::Update(float deltaTime){

	}
	void CNetworkEngine::Dispose(){
		Disconnect();
		settings = NULL;
	}
	int CNetworkEngine::GetPing(){
		if(status!=ENG_CONNECTED) return -1;
		int res = 0;
		pingMutex.lock();
		res = (int)(pingValue*1000.0f);
		pingMutex.unlock();
		return res;
	}

	std::string CNetworkEngine::GetServerAddress(){
		return serverAddress;
	}
	ENG_STATUS CNetworkEngine::GetStatus(){
		return status;
	}
	void CNetworkEngine::SetStatus(ENG_STATUS _status){
		status = _status;
	}
	bool CNetworkEngine::Connect(std::string address){
		//Disconnect old
		Disconnect();
		
		//check ip format
		int ip_0, ip_1, ip_2, ip_3;
		sscanf_s(address.c_str(), "%d%*c%d%*c%d%*c%d", &ip_0, &ip_1, &ip_2, &ip_3);

		if(ip_0<=0 || ip_0>255 ||ip_1<0 || ip_1>255 || ip_2<0 || ip_2>255 || ip_3<=0 || ip_3>255 ){
			//Wrong ip
			Log::Write("Wrong IP: " + address);
			return false;
		}
		//Wait status
		status = ENG_CONNECTWAIT;
		serverAddress = address;

		//Save IP to file
		std::ofstream fs(IP_FILENAME);
		if(fs){ //opened
			fs<<serverAddress;
			fs.close();
		}
		//start		
		WSADATA wsaData;

		// Initialize Winsock
		int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
		if(iResult != 0)
		{
			Log::Write("Wsastartup failed. error: ", iResult);
			status = ENG_NOTCONNECTED;
			return false;
		}

		// Create a SOCKET for connecting to server
		ZeroMemory(&serverAddr, sizeof(serverAddr));
		serverAddr.sin_family=AF_INET;//��� �������� ��������-����������
		serverAddr.sin_port=htons (settings->Port); //����
		serverAddr.sin_addr.S_un.S_un_b.s_b1 = ip_0;
		serverAddr.sin_addr.S_un.S_un_b.s_b2 = ip_1;
		serverAddr.sin_addr.S_un.S_un_b.s_b3 = ip_2;
		serverAddr.sin_addr.S_un.S_un_b.s_b4 = ip_3;
		clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		if (clientSocket == INVALID_SOCKET)
		{
			Log::Write("Error at socket(): ", WSAGetLastError());
			Disconnect();
			return false;
		}

		//Create thread
		clientThread = new std::thread(&CNetworkEngine::NetworkFunc, this);
		//Don't need pointer. Thread will auto close at ENG_NOTCONNECTED status
		//�� ������ �������

		//Ok
		//status = ENG_CONNECTED; By thread
		return true;


	}
	void CNetworkEngine::NetworkFunc(CNetworkEngine * _engine){
		//Shared
		char *_sendBuf = NULL, *_recvBuf = NULL;
		bool *_waitingSend = NULL, *_waitingRecv = NULL;
		float *_pingValue = NULL;
		std::mutex *_sendMutex = NULL, *_recvMutex = NULL, *_pingMutex = NULL;

		int iResult =0;
		struct sockaddr_in _serverAddr;
		SOCKET _clientSocket = NULL;
		ZeroMemory(&_serverAddr, sizeof(_serverAddr));
		_engine->GetConectionData(_clientSocket, _serverAddr, _sendBuf, _recvBuf, _waitingSend, _waitingRecv, _sendMutex, _recvMutex, _pingValue, _pingMutex);
		if(! _engine || ! _clientSocket || ! _sendBuf || ! _recvBuf || ! _waitingSend || ! _waitingRecv || ! _sendMutex || !_recvMutex || !_pingValue || !_pingMutex)
			return;//some pointer is null
		//Check
		if(_engine->GetStatus()==ENG_NOTCONNECTED || _clientSocket==NULL){
			_engine->Disconnect();
			return;
		}
		// Connect to server
		iResult = connect(_clientSocket, (sockaddr*) &_serverAddr, sizeof(_serverAddr));

		if (iResult == SOCKET_ERROR)
		{
			Log::Write("Error connect socket");
			_engine->Disconnect();
			return;
		}


		if (_clientSocket == INVALID_SOCKET)
		{
			Log::Write("Error connect socket");
			_engine->Disconnect();
			return;
		}
		_engine->SetStatus(ENG_CONNECTED);
		//Connected!
		char buf[DEFAULT_BUF_SIZE];
		float ping1, ping2;
		while(_engine->GetStatus() != ENG_NOTCONNECTED){
			if(*_waitingSend){//send
				*_waitingSend = false;
				//ping
				ping1 = CEngine::GetTime();

				_sendMutex->lock();
				iResult = send(_clientSocket, _sendBuf, DEFAULT_BUF_SIZE, 0);
				//if(iResult == 0) ;//bad recv
				_sendMutex->unlock();

				//Recv
				iResult = recv(_clientSocket, buf, DEFAULT_BUF_SIZE, 0);
				
				//ping
				ping2 = CEngine::GetTime();
				_pingMutex->lock();
				(*_pingValue) = ping2 - ping1;
				_pingMutex->unlock();

				if(iResult>0){
					*_waitingRecv = true;
					_recvMutex->lock();
					CopyMemory(_recvBuf, buf, DEFAULT_BUF_SIZE);
					_recvMutex->unlock();
				}else{
					break;//disconnect
				}
			}else{//not waiting for send
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				//Sleep(5);
			}

		}
		_engine->Disconnect();
		return;
	}

	void CNetworkEngine::GetConectionData(SOCKET & _clintSocket, struct sockaddr_in & _serverAddr, char *& _sendBuf, char *& _recvBuf, bool *& _waitingSend, bool *& _waitingRecv, std::mutex *& _sendMutex, std::mutex *& _recvMutex, float *& _pingValue, std::mutex *& _pingMutex){
		//Send socket, serverAddr, 2 buffers, 2 flags, 2 mutexes
		_clintSocket = clientSocket;
		_serverAddr = serverAddr;
		_sendBuf = sendBuf;
		_recvBuf = recvBuf;
		_waitingSend = &waitingSend;
		_waitingRecv = &waitingRecv;
		_sendMutex = &sendMutex;
		_recvMutex = &recvMutex;
		_pingValue = &pingValue;
		_pingMutex = &pingMutex;

	}
	void CNetworkEngine::Disconnect(){
		//vars
		status = ENG_NOTCONNECTED;
		waitingSend = false;
		waitingRecv = false;
		//close shit
		if(clientThread){
			//close thread
			//delete clientThread;
			clientThread = NULL;
		}

		if(clientSocket){
			//Close socket
			int iResult = shutdown(clientSocket, SD_SEND);
			if (iResult == SOCKET_ERROR){
				Log::Write("Socket shutdown failed. WsaError: ", WSAGetLastError());
			}
			closesocket(clientSocket);
			clientSocket = NULL;
		}
		WSACleanup();

	}
	bool CNetworkEngine::Send(std::string str){
		if(str.length() == 0) return false;
		str.resize(DEFAULT_BUF_SIZE, '\0');
		waitingSend = false;
		sendMutex.lock();
		CopyMemory(sendBuf, str.c_str(), DEFAULT_BUF_SIZE);
		sendMutex.unlock();
		waitingSend = true;

		return true;
	}
	bool CNetworkEngine::Read(std::string &res){
		if(!waitingRecv) return false;
		waitingRecv = false;
		recvMutex.lock();
		res = std::string(recvBuf);
		recvMutex.unlock();
		return true;
	}
}