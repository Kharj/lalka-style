#include "stdafx.h"
#include "LolEngine.h"
#include "GraphicsEngine.h"

namespace LolEngine{
	CGraphicsEngine::CGraphicsEngine(void)
	{
		
	pSettings = NULL;
	pD3D = NULL;
	d3ddev = NULL;
	ZeroMemory(&deviceCaps, sizeof(deviceCaps));
	deviceLost = false;
	status = ENG_NOTINIT;
	}


	CGraphicsEngine::~CGraphicsEngine(void)
	{
	}

	bool CGraphicsEngine::Init (HWND hWnd, CSettings *_pSetings){
		pSettings=_pSetings;
		//Create device
		HRESULT hr;
		pD3D = Direct3DCreate9(D3D_SDK_VERSION);
		if(!pD3D) return false;
		deviceLost = false;
		D3DDISPLAYMODE displayMode;
		ZeroMemory(&d3dpp, sizeof(d3dpp));//memclear
		ZeroMemory(&displayMode, sizeof(displayMode));//memclear
		//ZeroMemory(&windowRect, sizeof(windowRect));//memclear
		//Get device caps
		hr = pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT , D3DDEVTYPE_HAL, &deviceCaps);
		if(FAILED(hr)){
			Log::Write("Error get device caps.");
			return false;
		}
		

		hr = pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode);	
		//auto resolution full screen
		if(pSettings->AutoResolution && pSettings->Fullscreen){
			pSettings->Width=displayMode.Width;
			pSettings->Height=displayMode.Height;
		}
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		D3DFORMAT DisplayFormat;
		D3DFORMAT BackBufferFormat;
		DisplayFormat = displayMode.Format;
		BackBufferFormat=displayMode.Format;
		d3dpp.BackBufferFormat=displayMode.Format;
		d3dpp.EnableAutoDepthStencil = TRUE;													
		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.Windowed = !pSettings->Fullscreen;
		d3dpp.BackBufferHeight=pSettings->Height;
		d3dpp.BackBufferWidth=pSettings->Width;
		if(pSettings->FpsNoLimit){
			d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;										
		}else{
			d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
		}
		//test texture filter capability
		if((!(deviceCaps.TextureFilterCaps & D3DPTFILTERCAPS_MINFANISOTROPIC) && (pSettings->TextureMinFilter == D3DTEXF_ANISOTROPIC))
			|| (!(deviceCaps.TextureFilterCaps & D3DPTFILTERCAPS_MINFLINEAR) && (pSettings->TextureMinFilter == D3DTEXF_LINEAR))){
				pSettings->TextureMinFilter = D3DTEXF_POINT;
			Log::Write("This texture filter not supported. Using D3DTEXF_POINT.");
		}
		if(pSettings->TextureMaxAnisotropy > deviceCaps.MaxAnisotropy){
			pSettings->TextureMaxAnisotropy = deviceCaps.MaxAnisotropy;
			Log::Write("This anisotropy not supported. Set max available.");
		}

		//Multisample
		hr=pD3D->CheckDeviceMultiSampleType( D3DADAPTER_DEFAULT, 	D3DDEVTYPE_HAL , DisplayFormat, !pSettings->Fullscreen, (D3DMULTISAMPLE_TYPE)pSettings->MultiSamplingType, NULL ) ;
		if(SUCCEEDED(hr)){
			d3dpp.MultiSampleType = (D3DMULTISAMPLE_TYPE)pSettings->MultiSamplingType;
		}else{
			d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
			Log::Write("This Multisampling not supported. Multisampling disabled.");
		}
		hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &d3ddev);
		if(FAILED(hr)) return false;
		status = ENG_OK;
		return true;
	}
	void CGraphicsEngine::Dispose (){
		if(pD3D) pD3D->Release();
		if(d3ddev) d3ddev->Release();
	}
	void CGraphicsEngine::BeginDraw(){
		HRESULT hr;
		//Lost
		if(deviceLost){
			if(D3DERR_DEVICENOTRESET == d3ddev->TestCooperativeLevel()){//reset if focus
				hr = d3ddev->Reset(&d3dpp);
				hr=hr;
				if(FAILED(hr)){
					Log::Write("Failed reset device!");
					return;
				}
				
				//ALL RESET!
				if(!Reset()) return;
				deviceLost=false;
			}else{
				return;
			}
		}
		d3ddev->Clear(0,NULL,D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 0.0f,0);
		d3ddev->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
		//begin
		hr = d3ddev->BeginScene();

	}
	void CGraphicsEngine::EndDraw(){
		HRESULT hr = 0;		
		//end
		hr = d3ddev->EndScene();
		//present
		hr = d3ddev->Present(NULL,NULL,NULL,NULL);
		if(hr==D3DERR_DEVICELOST || hr==D3DERR_INVALIDCALL){
			deviceLost=true;
		}
	}
	bool CGraphicsEngine::Reset(){
		return true;
	}

	void CGraphicsEngine::Update(float deltaTime){

	}
	LPDIRECT3DDEVICE9 CGraphicsEngine::GetDevice(){
		return d3ddev;
	}
	D3DCAPS9 CGraphicsEngine::GetDeviceCaps(){
		return deviceCaps;
	}
}
