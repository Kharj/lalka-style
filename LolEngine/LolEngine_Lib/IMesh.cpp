#include "stdafx.h"
#include "LolEngine.h"
#include "IMesh.h"

namespace LolEngine{
	IMesh::IMesh(void)
	{
		numMaterials=0;
		meshMaterials=NULL;
		meshTextures= NULL;
		materialBuffer=NULL;
		mesh=NULL;

		//vectors
		position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
		visible = true;
		textureState_colorarg1 = D3DTA_TEXTURE;
		textureState_colorarg2 = D3DTA_DIFFUSE;
		textureState_colorop = D3DTOP_SELECTARG1;
		textureState_alphaarg1 = D3DTA_TEXTURE;
		textureState_alphaarg2 = D3DTA_DIFFUSE;
		textureState_alphaop = D3DTOP_MODULATE;
	}


	IMesh::~IMesh(void)
	{
	}

	void IMesh::Dispose(){
		if(meshMaterials){
			delete [] meshMaterials;
			meshMaterials=NULL;
		}
		if(meshTextures){
			for(DWORD i=0;i<(int)numMaterials;++i)
				if(meshTextures[i]) meshTextures[i]->Release();
			delete [] meshTextures;
			meshTextures= NULL;
		}
		if(mesh){
			mesh->Release();
			mesh=NULL;
		}
		if(materialBuffer){
			materialBuffer->Release();
			materialBuffer=NULL;
		}
		numMaterials=0;
	}
#pragma region Transform
	D3DXVECTOR3 IMesh::GetPosition(){
		return position;
	}
	D3DXVECTOR3 IMesh::GetRotation(){
		return rotation;
	}
	D3DXVECTOR3 IMesh::GetScale(){
		return scale;
	}
	void IMesh::SetPosition(D3DXVECTOR3 _position){
		position = _position;
	}
	void IMesh::SetRotation(D3DXVECTOR3 _rotation){
		rotation = _rotation;
	}
	void IMesh::SetScale(D3DXVECTOR3 _scale){
		scale = _scale;
	}
#pragma endregion

	bool IMesh::Draw(LPDIRECT3DDEVICE9 d3ddev, D3DXVECTOR3 objectPosition, D3DXVECTOR3 objectRotation, D3DXVECTOR3 objectScale){
		HRESULT hr=0;
		if(!visible) return true;//невидимый
		//Texture render State
		//texture
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, textureState_colorarg1);
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2, textureState_colorarg2);
		d3ddev->SetTextureStageState(0, D3DTSS_COLOROP,  textureState_colorop);
		//alpha
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, textureState_alphaarg1);
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, textureState_alphaarg2);
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAOP,  textureState_alphaop);
		//d3ddev->SetTextureStageState( 0, D3DTSS_TEXCOORDINDEX, 0 );
		//1 text

		D3DXMATRIX matRotateY, matRotateX, matRotateZ, matTranslate, matScale;

		D3DXMatrixScaling(&matScale, objectScale.x * scale.x, objectScale.y * scale.y, objectScale.z * scale.z);
		D3DXMatrixRotationX(&matRotateX, objectRotation.x + rotation.x);
		D3DXMatrixRotationY(&matRotateY, objectRotation.y + rotation.y);
		D3DXMatrixRotationZ(&matRotateZ, objectRotation.z + rotation.z);
		D3DXMatrixTranslation(&matTranslate, objectPosition.x + position.x, objectPosition.y + position.y, objectPosition.z + position.z);

		D3DXMATRIX matRes=(matScale * matRotateX * matRotateY * matRotateZ * matTranslate);

		for (DWORD j=0; j<numMaterials; j++)
		{
			// Set the material and texture for this subset
			if(&meshMaterials[j]){
				hr = d3ddev->SetMaterial(&meshMaterials[j]);
				hr=hr;
			}
			if(meshTextures[j]){
				hr = d3ddev->SetTexture(0, meshTextures[j]);
				hr=hr;
			}else{
				d3ddev->SetTexture(0, NULL);
			}
			d3ddev->SetTransform(D3DTS_WORLD, &matRes);
			
			hr = mesh->DrawSubset(j);// Draw the mesh subset
			if(FAILED(hr)) return false;//IF ERROR DRAW
			d3ddev->SetTexture(0, NULL);

		}
		return true;
	}
	bool IMesh::LoadFromMesh(LPD3DXMESH m, LPDIRECT3DDEVICE9 d3ddev){

		Dispose();
		HRESULT hr = D3DXCreateMeshFVF(0,0,D3DPOOL_MANAGED,m->GetFVF(),d3ddev, &mesh);
		hr = m->CloneMeshFVF(D3DPOOL_MANAGED,m->GetFVF(),d3ddev,&mesh);
		if(FAILED(hr)){
			return false;
		}
		numMaterials=0;
		meshMaterials = NULL;
		meshTextures  = NULL;
		return true;
	}
	bool IMesh::LoadMaterials(int num, D3DMATERIAL9 * mi){
		if(meshMaterials){//release materials
			delete [] meshMaterials;
			meshMaterials=NULL;
		}
		if(meshTextures){//release textures
			for(DWORD i=0;i<(int)numMaterials;++i)
				if(meshTextures[i]) meshTextures[i]->Release();
			delete [] meshTextures;
			meshTextures= NULL;
		}
		if(num==0 || num<0) return false;
		numMaterials=num;
		meshMaterials = new D3DMATERIAL9 [numMaterials];
		meshTextures  = new LPDIRECT3DTEXTURE9[numMaterials];
		for(DWORD i=0;i<numMaterials;++i){
			meshTextures[i]=NULL;
			meshMaterials[i]=mi[i];
			//meshMaterials[i].Ambient = meshMaterials[i].Diffuse;
			//load textures
		}
		return true;

	}
	bool IMesh::LoadMaterials(LPDIRECT3DDEVICE9 d3ddev, int num, D3DXMATERIAL * mi){//WITH TEXTURE FILENAMES
		HRESULT hr=0;
		if(meshMaterials){//release materials
			delete [] meshMaterials;
			meshMaterials=NULL;
		}
		if(meshTextures){//release textures
			for(DWORD i=0;i<(int)numMaterials;++i)
				if(meshTextures[i]) meshTextures[i]->Release();
			delete [] meshTextures;
			meshTextures= NULL;
		}
		if(num==0 || num<0) return false;
		numMaterials=num;
		meshMaterials = new D3DMATERIAL9 [numMaterials];
		meshTextures  = new LPDIRECT3DTEXTURE9[numMaterials];
		for(DWORD i=0;i<numMaterials;++i){
			meshTextures[i]=NULL;
			meshMaterials[i]=mi[i].MatD3D;
			//meshMaterials[i].Ambient = meshMaterials[i].Diffuse;
			//load textures
			textureState_colorop = D3DTOP_SELECTARG2;
			if(mi[i].pTextureFilename){
				textureState_colorop = D3DTOP_SELECTARG1;
				hr = D3DXCreateTextureFromFileA(d3ddev, mi[i].pTextureFilename, &meshTextures[i]); 
				if(hr!=D3D_OK){
					Log::Write(std::string("Using checker because error load texture ")+mi[i].pTextureFilename);//log
					meshTextures[i]=NULL;
					hr = D3DXCreateTextureFromFileInMemory(d3ddev, (LPCVOID)Default_Texture_Mas,(UINT)Default_Texture_Mas_Size, &meshTextures[i]);
				}
			}
		}
		return true;

	}

	bool IMesh::LoadFromX(LPCSTR filename, LPDIRECT3DDEVICE9 d3ddev){
		Dispose();
		HRESULT hr = D3DXLoadMeshFromXA(filename, D3DXMESH_SYSTEMMEM, 
			d3ddev, NULL, 
			&materialBuffer,NULL, &numMaterials, 
			&mesh );
		if(FAILED(hr)){
			Log::Write(std::string("Error load mesh ")+filename);
			return false;
		}
		D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)materialBuffer->GetBufferPointer();
		meshMaterials = new D3DMATERIAL9[numMaterials];
		meshTextures  = new LPDIRECT3DTEXTURE9[numMaterials];
		for (DWORD i=0; i<numMaterials; i++){
			meshMaterials[i] = d3dxMaterials[i].MatD3D;
			//meshMaterials[i].Ambient = meshMaterials[i].Diffuse;
			meshTextures[i] = NULL;
			if (d3dxMaterials[i].pTextureFilename){
				hr = D3DXCreateTextureFromFileA(d3ddev, d3dxMaterials[i].pTextureFilename, &meshTextures[i]); 
				if(hr!=D3D_OK){
					Log::Write(std::string("Using checker because error load texture ")+d3dxMaterials[i].pTextureFilename);//log
					meshTextures[i]=NULL;
					hr = D3DXCreateTextureFromFileInMemory(d3ddev, (LPCVOID)Default_Texture_Mas,(UINT)Default_Texture_Mas_Size, &meshTextures[i]);
				}

			}
		}
		//meshMaterials->Release();
		return true;
	}
	bool IMesh::LoadTexture(LPCSTR filename, LPDIRECT3DDEVICE9 d3ddev, int textureNum, int subset){
		HRESULT hr=0;
		if(filename==NULL){
			return false;
		}
		if(!numMaterials || !meshMaterials){//Materials not loaded!
			return false;
		}
		if(textureNum>=(int)numMaterials){
			return false;
		}
		if(meshTextures[textureNum]){
			meshTextures[textureNum]->Release();
			meshTextures[textureNum] = NULL;
		}
		hr = D3DXCreateTextureFromFileA(d3ddev, filename, &meshTextures[textureNum]); 
		if(hr!=D3D_OK){
			meshTextures[textureNum] = NULL;
			Log::Write(std::string("Error load texture \'")+filename+"\' if U want to use checker, edit  IMesh::LoadTexture");
			return false;
		}
		return true;
	}
	bool IMesh::SetTexture(int num, LPDIRECT3DTEXTURE9 tex){
		if(num >= (int)numMaterials) return false;
		if(meshTextures[num]){
			meshTextures[num]->Release();
			meshTextures[num] = NULL;
		}
		meshTextures[num] = tex;
		return true;
	}
	D3DMATERIAL9 * IMesh::GetMaterial(int num){
		if(num >= (int)numMaterials) return NULL;
		if(meshMaterials == NULL) return NULL;
		return &meshMaterials[num];
	}
	
	void IMesh::SetStateColor(DWORD colorarg1, DWORD colorarg2, DWORD colorop){
		textureState_colorarg1 = colorarg1;
		textureState_colorarg2 = colorarg2;
		textureState_colorop = colorop;
	}
	void IMesh::SetStateAlpha(DWORD alphaarg1, DWORD alphaarg2, DWORD alphaop){
		textureState_alphaarg1 = alphaarg1;
		textureState_alphaarg2 = alphaarg2;
		textureState_alphaop = alphaop;
	}
	void IMesh::SetName(std::string str){
		name = str;
	}
	std::string IMesh::GetName(){
		return name;
	}
}