#include "stdafx.h"
#include "LolEngine.h"
#include "Settings.h"

namespace LolEngine{
	
}
namespace LolEngine{

	CSettings::CSettings(void){
		newSettings=NULL;

		Fullscreen=false;
		AutoResolution=true;
		XInput=true;
		SplitscreenVertical=true;
		FpsNoLimit=false;
		Height=480;
		Width=640;
		MultiSamplingType=0;
		TextureFilter=-2;
		MouseScale=1.0f;
		Volume=100;
		TextureMinFilter=D3DTEXF_LINEAR;
		TextureMaxAnisotropy=1;
		Port = 27015;


	}


	CSettings::~CSettings(void)
	{
		if(newSettings){
			delete newSettings;
			newSettings = NULL;
		}
	}


	bool CSettings::LoadFromFile(){
		FILE* fp;
		fopen_s(&fp,SETTINGS_FILENAME,"r");
		if(!fp) return false;
		int fs, ar, h, w, mt, tf, sx, vol, por, xin, ss, fps;
		if(12 != fscanf_s(fp,"Fullscreen %d\nAutoResolution %d\nUse_XInput %d\nSplitscreenVertical %d\nFpsNoLimit %d\nWidth %d\nHeight %d\nMultiSamplingType %d\nTextureFilter %d\nMouseScale %d\nVolume %d\nPort %d\nEnd", &fs, &ar, &xin, &ss, &fps,&w, &h, &mt, &tf, &sx, &vol, &por)){
			return false;
		}
		if(fs)
			Fullscreen=TRUE;
		else
			Fullscreen=FALSE;
		if(ar)
			AutoResolution=true;
		else
			AutoResolution=false;
		if(xin)
			XInput=true;
		else
			XInput=false;
		if(ss)
			SplitscreenVertical=true;
		else
			SplitscreenVertical=false;
		if(fps)
			FpsNoLimit=true;
		else
			FpsNoLimit=false;
		if(h>0)
			Height=h;
		if(w>0)
			Width=w;
		Volume = vol;
		if(mt>1)
			MultiSamplingType=mt;
		if(tf>-3 && tf<17){
			TextureFilter=tf;
			TextureMinFilter=D3DTEXF_POINT;
			TextureMaxAnisotropy=1;
			switch(TextureFilter){
			case 0:
				//TextureMinFilter = D3DTEXF_NONE; break;
			case -1:
				TextureMinFilter = D3DTEXF_POINT; break;
			case -2:
				TextureMinFilter = D3DTEXF_LINEAR; break;
			case 1: case 2: case 4: case 8: case 16: 
				TextureMinFilter = D3DTEXF_ANISOTROPIC; 
				TextureMaxAnisotropy = TextureFilter; break;
			}
			
		}
		if(sx>0 && sx<1001){
			MouseScale=(float)sx/100.0f;
		}
		if(por>0){
			Port=por;
		}
		fclose(fp);
		return true;
	}

	bool CSettings::SaveToFile(){
		FILE* fp;
		fopen_s(&fp,SETTINGS_FILENAME,"w");
		if(!fp) return false;
		int fs=0;
		if(Fullscreen)
			fs=1;
		int ar=0;
		if(AutoResolution)
			ar=1;
		int xin = 0;
		if(XInput)
			xin=1;
		int ss=0;
		if(SplitscreenVertical)
			ss=1;
		int fps=0;
		if(FpsNoLimit)
			fps=1;
		int sx=100;
		if(MouseScale>0.01f && MouseScale<10.1f){
			sx=(MouseScale<1.01f && MouseScale>0.99f)?100:(int)(MouseScale*100.0f+0.51f);
		}
		int texF=-1;
		if(TextureMinFilter == D3DTEXF_POINT) texF = -1;
		else if(TextureMinFilter == D3DTEXF_LINEAR) texF = -2;
		else if(TextureMinFilter == D3DTEXF_ANISOTROPIC) texF = TextureMaxAnisotropy;

		fprintf(fp,"Fullscreen %d\nAutoResolution %d\nUse_XInput %d\nSplitscreenVertical %d\nFpsNoLimit %d\nWidth %d\nHeight %d\nMultiSamplingType %d\nTextureFilter %d\nMouseScale %d\nVolume %d\nPort %d\nEnd",fs,ar,xin,ss, fps,Width,Height,MultiSamplingType, texF, sx, Volume, Port);
		fclose(fp);
		return true;
	}
	float CSettings::GetAspectRatio(){
		return (float)Width/(float)Height;
	}
}
