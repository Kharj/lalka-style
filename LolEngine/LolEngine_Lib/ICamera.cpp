#include "stdafx.h"
#include "LolEngine.h"
#include "ICamera.h"

namespace LolEngine{
	ICamera::ICamera(void)
	{
		Eye = D3DXVECTOR3 (0.0f, 0.0f, -20.0f);// the camera position
		At = D3DXVECTOR3 (0.0f, 0.0f, 0.0f);// the look-at position
		Up = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);// the up direction
		fovy = D3DXToRadian(45); // the horizontal field of view
		Aspect = 4.0f/ 3.0f; // aspect ratio
		zn = 1.0f; // the near view-plane
		zf = 100.0f; // the far view-plane
		//vectors
		position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	}


	ICamera::~ICamera(void)
	{
	}
	D3DXMATRIX * ICamera::GetViewMatrix(){
		D3DXMatrixLookAtLH(&matView, &Eye, &At, &Up);    // the up direction
		return &matView;
	}
	D3DXMATRIX * ICamera::GetProjectionMatrix(){
		D3DXMatrixPerspectiveFovLH(&matProjection, fovy, Aspect, zn, zf);
		return &matProjection;
	}
	D3DXMATRIX * ICamera::GetTransformedViewMatrix(){
		GetViewMatrix();
		D3DXMATRIX matRotateX, matRotateY, matRotateZ, matScale, matTranslate;

		D3DXMatrixScaling(&matScale, scale.x, scale.y, scale.z);
		D3DXMatrixRotationX(&matRotateX, rotation.x);
		D3DXMatrixRotationY(&matRotateY, rotation.y);
		D3DXMatrixRotationZ(&matRotateZ, rotation.z);
		D3DXMatrixTranslation(&matTranslate, position.x, position.y, position.z);
		D3DXMatrixMultiply(&matTransform, &matScale, &matRotateX);
		D3DXMatrixMultiply(&matTransform, &matTransform, &matRotateY);
		D3DXMatrixMultiply(&matTransform, &matTransform, &matRotateZ);
		D3DXMatrixMultiply(&matTransform, &matTransform, &matTranslate);
		D3DXMatrixMultiply(&matViewTransformed, &matView, &matTransform);
		return &matViewTransformed;
	}
	D3DXVECTOR3  ICamera::GetLookAt(){
		return At;
	}
	D3DXVECTOR3  ICamera::GetEyePosition(){
		return Eye;
	}
	void ICamera::SetView(D3DXVECTOR3 _Eye, D3DXVECTOR3 _At, D3DXVECTOR3 _Up ){
		Eye=_Eye;
		At=_At;
		Up=_Up;
	}
	void ICamera::SetLookAt(D3DXVECTOR3 _At){
		At=_At;
	}
	void ICamera::SetEyePosition(D3DXVECTOR3 _Eye){
		Eye=_Eye;
	}
	void ICamera::SetUpDirection(D3DXVECTOR3 _Up){
		Up=_Up;
	}
	void ICamera::SetProjection(FLOAT _fovy, FLOAT _Aspect, FLOAT _zn, FLOAT _zf ){
		fovy=_fovy;
		Aspect=_Aspect;
		zn=_zn;
		zf=_zf;
	}
	void ICamera::SetFovy(FLOAT _fovy){
		fovy=_fovy;
	}
	void ICamera::Reset(){
		Eye = D3DXVECTOR3 (0.0f, 0.0f, -20.0f);// the camera position
		At = D3DXVECTOR3 (0.0f, 0.0f, 0.0f);// the look-at position
		Up = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);// the up direction
		fovy = D3DXToRadian(45); // the horizontal field of view
		Aspect = 4.0f/ 3.0f; // aspect ratio
		zn = 1.0f; // the near view-plane
		zf = 100.0f; // the far view-plane
		//vectors
		position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	}
#pragma region Transform
	D3DXVECTOR3 ICamera::GetPosition(){
		return position;
	}
	D3DXVECTOR3 ICamera::GetRotation(){
		return rotation;
	}
	D3DXVECTOR3 ICamera::GetScale(){
		return scale;
	}
	void ICamera::SetPosition(D3DXVECTOR3 _position){
		position = _position;
	}
	void ICamera::SetRotation(D3DXVECTOR3 _rotation){
		rotation = _rotation;
	}
	void ICamera::SetScale(D3DXVECTOR3 _scale){
		scale = _scale;
	}

#pragma endregion
}