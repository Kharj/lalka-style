#pragma once
namespace LolEngine{
class ILight
{
protected:
	D3DLIGHT9 dLight;
public:
	bool On;
	//nul onstructor
	ILight(void);
	//full constructor
	ILight(D3DLIGHTTYPE Type, D3DVECTOR Position, D3DVECTOR Direction, D3DCOLORVALUE Diffuse=D3DXCOLOR(1.0f,1.0f,1.0f,1.0f), D3DCOLORVALUE Specular=D3DXCOLOR(1.0f,1.0f,1.0f,1.0f), D3DCOLORVALUE Ambient=D3DXCOLOR(1.0f,1.0f,1.0f,1.0f), float Falloff=1.0f, float Range=10000.0f, float Phi=1.0f, float Theta=1.0f);
	//simple directional
	ILight(D3DVECTOR Position, D3DVECTOR Direction, D3DCOLORVALUE allcolors=D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
	virtual ~ILight(void);
	D3DLIGHT9 * GetLightPointer();
	void SetAmbientColor(D3DXCOLOR color);
	void SetDiffuseColor(D3DXCOLOR color);
	void SetSpecularColor(D3DXCOLOR color);
	void SetAllColors(D3DXCOLOR color);

	void SetPosition(D3DXVECTOR3 pos);
	void SetDirection(D3DXVECTOR3 dir);
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetDirection();
};
}
