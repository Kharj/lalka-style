#include "stdafx.h"
#include "LolEngine.h"

namespace LolEngine{

	//Our FVF
#define FVF_NORMAL_TEX (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)
	struct NormalTexVertex
	{
		float x, y, z;    // местоположение
		float nx, ny, nz; // вектор нормали
		float u, v;        // координаты текстуры
		NormalTexVertex(float _x, float _y, float _z, float _nx, float _ny, float _nz, float _u, float _v){
			x=_x; y=_y; z=_z; nx=_nx; ny=_ny; nz=_nz; u=_u; v=_v;
		}
	};
	DWORD CEngine::timeStart = 0;
	CEngine::CEngine(void){
		fpsCounterTime=0;
		fpsCounterFrames=0;
		fpsCounterResult=0;
		currentScene = NULL;
		currentSceneNum = 0;
		status = ENG_OK;
		pSettings = NULL;
		currentScene = NULL;
		currentSceneNum = 0;
	}
	CEngine::~CEngine(void){	}

	bool CEngine::Init (HWND hWnd, CSettings *_pSetings){
		pSettings=_pSetings;
		//engines
		if(!graphicsEngine.Init(hWnd, pSettings)){
			Log::Write("Error init Graphics engine");
			return false;
		}
		if(!inputEngine.Init(hWnd, pSettings)){
			Log::Write("Error init Input engine");
			return false;
		}
		if(!soundEngine.Init(hWnd, pSettings)){
			Log::Write("Error init Sound engine");
			return false;
		}
		if(!networkEngine.Init(hWnd, pSettings)){
			Log::Write("Error init Network engine");
			return false;
		}
		//Scenes
		if(!CreateScenes()){
			Log::Write("Error create scenes");
			return false;
		}
		if(!SelectScene(0)){
			Log::Write("Error select 0 scene");
			return false;
		}
		return true;
	}
	void CEngine::Dispose (){
		//dispose scenes
		if(currentScene){
			currentScene->Dispose();
		}
		currentScene = NULL;
		scenes.clear();
		//Dispose all part of engine
		graphicsEngine.Dispose();
		inputEngine.Dispose();
		soundEngine.Dispose();
		networkEngine.Dispose();

	}

	float CEngine::GetTime(){
		if(!timeStart) timeStart = timeGetTime();
		DWORD t = timeGetTime() - timeStart;
		return ((float)t)/1000.0f;
	}
	int CEngine::GetFPS(){
		return fpsCounterResult;
	}
	int CEngine::GetPing(){
		return networkEngine.GetPing();
	}
	void CEngine::Draw(){
		fpsCounterFrames++;
		if(GetTime()-fpsCounterTime > 1.0f){//1 second
			fpsCounterResult=(int)(((float)fpsCounterFrames/(GetTime()-fpsCounterTime))+0.5f);
			fpsCounterTime=GetTime();
			fpsCounterFrames=0;
		}
		//
		//graphicsEngine.Draw();
		if(currentScene){
			currentScene->Draw();
		}
	}
	void CEngine::Update(){
		//delta time work
		static float lastFlameTime;
		float deltaTime = GetTime()-lastFlameTime;
		lastFlameTime = GetTime();
		//
		graphicsEngine.Update(deltaTime);
		inputEngine.Update();
		if(currentScene){
			currentScene->Update(deltaTime);
		}
	}
	bool CEngine::SelectScene(unsigned int n){

		if(currentScene){
			currentScene->Dispose();
		}
		if(scenes.empty() || scenes.size()<n+1){
			currentScene = NULL;
			return false;//out of array
		}
		LoadingScreen(); //blank the screen and show loading message
		currentScene = scenes[n];

		if(! currentScene) return false;//scene is null
		if(! currentScene->Init(&graphicsEngine, &soundEngine, &inputEngine, &networkEngine, pSettings)){
			currentScene = NULL;
			return false;//error init
		}
		currentSceneNum = n;
		return true;
	}
	//to override
	bool CEngine::CreateScenes(){//Tipa abstract
		return true;
	}
	ENG_STATUS CEngine::GetStatus(){
		return status;
	}
	void CEngine::LoadingScreen(){

	}
	IMesh * CreateSkyBoxMesh(LPDIRECT3DDEVICE9 d3ddev, D3DXVECTOR3 size, LPSTR texTop, LPSTR texLeft, LPSTR texFront, LPSTR texRight, LPSTR texBack, LPSTR texBottom){
		HRESULT hr=0;
		LPD3DXMESH resultMesh;
		size *= 0.5f;
		hr = D3DXCreateMeshFVF(12, 24, D3DXMESH_MANAGED, FVF_NORMAL_TEX, d3ddev, &resultMesh);
		if(FAILED(hr)) return NULL;
		NormalTexVertex* vertices = NULL;
		//Write vertices
		hr = resultMesh->LockVertexBuffer(0, (void**)&vertices);
		if(FAILED(hr)){
			resultMesh->Release();
			return NULL;
		}
		//-z
		vertices[0] = NormalTexVertex(-size.x, -size.y, -size.z,  0.0f,  0.0f, 1.0f,  1.0f,  1.0f);
		vertices[1] = NormalTexVertex(size.x,  -size.y, -size.z,  0.0f,  0.0f, 1.0f,  0.0f,  1.0f); 
		vertices[2] = NormalTexVertex(-size.x,  size.y, -size.z,  0.0f,  0.0f, 1.0f,  1.0f,  0.0f); 
		vertices[3] = NormalTexVertex(size.x,  size.y, -size.z,  0.0f,  0.0f, 1.0f,  0.0f,  0.0f); 
		//z
		vertices[4] = NormalTexVertex(-size.x, -size.y, size.z,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f);
		vertices[5] = NormalTexVertex(size.x,  -size.y, size.z,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f); 
		vertices[6] = NormalTexVertex(-size.x,  size.y, size.z,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f); 
		vertices[7] = NormalTexVertex(size.x,  size.y, size.z,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f); 
		//y
		vertices[8] = NormalTexVertex(-size.x, size.y, -size.z,  0.0f,  -1.0f, 0.0f,  0.0f,  1.0f);
		vertices[9] = NormalTexVertex(size.x,  size.y, -size.z,  0.0f,  -1.0f, 0.0f,  1.0f,  1.0f); 
		vertices[10] = NormalTexVertex(-size.x,  size.y, size.z,  0.0f,  -1.0f, 0.0f,  0.0f,  0.0f); 
		vertices[11] = NormalTexVertex(size.x,  size.y, size.z,  0.0f,  -1.0f, 0.0f,  1.0f,  0.0f);
		//-y
		vertices[12] = NormalTexVertex(-size.x, -size.y, -size.z,  0.0f,  1.0f, 0.0f,  0.0f,  1.0f);
		vertices[13] = NormalTexVertex(size.x,  -size.y, -size.z,  0.0f,  1.0f, 0.0f,  1.0f,  1.0f); 
		vertices[14] = NormalTexVertex(-size.x,  -size.y, size.z,  0.0f,  1.0f, 0.0f,  0.0f,  0.0f); 
		vertices[15] = NormalTexVertex(size.x,  -size.y, size.z,  0.0f,  1.0f, 0.0f,  1.0f,  0.0f); 
		//x
		vertices[16] = NormalTexVertex(size.x, -size.y, -size.z,  -1.0f,  0.0f, 0.0f,  1.0f,  1.0f);
		vertices[17] = NormalTexVertex(size.x,  size.y, -size.z,  -1.0f,  0.0f, 0.0f,  1.0f,  0.0f); 
		vertices[18] = NormalTexVertex(size.x,  -size.y, size.z,  -1.0f,  0.0f, 0.0f,  0.0f,  1.0f); 
		vertices[19] = NormalTexVertex(size.x,  size.y, size.z,  -1.0f,  0.0f, 0.0f,  0.0f,  0.0f); 
		//-x
		vertices[20] = NormalTexVertex(-size.x, -size.y, -size.z,  1.0f,  0.0f, 0.0f,  0.0f,  1.0f);
		vertices[21] = NormalTexVertex(-size.x,  size.y, -size.z,  1.0f,  0.0f, 0.0f,  0.0f,  0.0f); 
		vertices[22] = NormalTexVertex(-size.x,  -size.y, size.z,  1.0f,  0.0f, 0.0f,  1.0f,  1.0f); 
		vertices[23] = NormalTexVertex(-size.x,  size.y, size.z,  1.0f,  0.0f, 0.0f,  1.0f,  0.0f); 
		resultMesh->UnlockVertexBuffer();
		//Write indexes
		WORD* indexes = NULL;
		hr = resultMesh->LockIndexBuffer(0, (void**)&indexes);
		if(FAILED(hr)){
			resultMesh->Release();
			return NULL;
		}
		indexes[0]=0; indexes[1]=1; indexes[2]=2; indexes[3]=1; indexes[4]=2; indexes[5]=3;
		indexes[6]=4; indexes[7]=5; indexes[8]=6; indexes[9]=5; indexes[10]=6; indexes[11]=7;
		indexes[12]=8; indexes[13]=9; indexes[14]=10; indexes[15]=9; indexes[16]=10; indexes[17]=11;
		indexes[18]=12; indexes[19]=13; indexes[20]=14; indexes[21]=13; indexes[22]=14; indexes[23]=15;
		indexes[24]=16; indexes[25]=17; indexes[26]=18; indexes[27]=17; indexes[28]=18; indexes[29]=19;
		indexes[30]=20; indexes[31]=21; indexes[32]=22; indexes[33]=21; indexes[34]=22; indexes[35]=23;
		resultMesh->UnlockIndexBuffer();
		//attrib

		// lock the attribute buffer
		DWORD *attribs;
		if (SUCCEEDED(hr = resultMesh->LockAttributeBuffer(D3DLOCK_DISCARD,&attribs))) {
			if(resultMesh->GetNumFaces()==12){//all 36 loaded
				attribs[0] = 0;
				attribs[1] = 0;
				attribs[2] = 1;
				attribs[3] = 1;
				attribs[4] = 2;
				attribs[5] = 2;
				attribs[6] = 3;
				attribs[7] = 3;
				attribs[8] = 4;
				attribs[9] = 4;
				attribs[10] = 5;
				attribs[11] = 5;
			}
			resultMesh->UnlockAttributeBuffer();
		}
		//mat
		D3DXMATERIAL materials[6] = {0};
		materials[0].MatD3D.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		materials[0].MatD3D.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		materials[1] = materials[0];
		materials[2] = materials[0];
		materials[3] = materials[0];
		materials[4] = materials[0];
		materials[5] = materials[0];
		materials[0].pTextureFilename = texBack;
		materials[1].pTextureFilename = texFront;
		materials[2].pTextureFilename = texTop;
		materials[3].pTextureFilename = texBottom;
		materials[4].pTextureFilename = texRight;
		materials[5].pTextureFilename = texLeft;
		//create engine's mesh
		IMesh *result = new IMesh();
		if(!result->LoadFromMesh(resultMesh, d3ddev)){
			result->Dispose();
			return NULL;
		}
		result->LoadMaterials(d3ddev, 6, materials);
		result->SetName("Skybox");
		return result;
	}
	IMesh * CreatePlaneMesh(LPDIRECT3DDEVICE9 d3ddev, float sizeX, float sizeY, LPSTR texture, D3DXCOLOR color){
		HRESULT hr=0;
		LPD3DXMESH resultMesh;
		sizeX *= 0.5f;
		sizeY *= 0.5f;
		hr = D3DXCreateMeshFVF(2, 4, D3DXMESH_MANAGED, FVF_NORMAL_TEX, d3ddev, &resultMesh);
		if(FAILED(hr)) return NULL;
		NormalTexVertex* vertices = NULL;
		//Write vertices
		hr = resultMesh->LockVertexBuffer(0, (void**)&vertices);
		if(FAILED(hr)){
			resultMesh->Release();
			return NULL;
		}
		vertices[0] = NormalTexVertex(-sizeX, -sizeY,  0.0f,  0.0f,  0.0f, 1.0f,  1.0f,  1.0f);
		vertices[1] = NormalTexVertex(sizeX,  -sizeY,  0.0f,  0.0f,  0.0f, 1.0f,  0.0f,  1.0f); 
		vertices[2] = NormalTexVertex(-sizeX,  sizeY,  0.0f,  0.0f,  0.0f, 1.0f,  1.0f,  0.0f); 
		vertices[3] = NormalTexVertex(sizeX,  sizeY,  0.0f,  0.0f,  0.0f, 1.0f,  0.0f,  0.0f); 
		resultMesh->UnlockVertexBuffer();
		//Write indexes
		WORD* indexes = NULL;
		hr = resultMesh->LockIndexBuffer(0, (void**)&indexes);
		if(FAILED(hr)){
			resultMesh->Release();
			return NULL;
		}
		indexes[0]=0; indexes[1]=1; indexes[2]=2; indexes[3]=1; indexes[4]=2; indexes[5]=3;
		//attrib
		//mat
		D3DXMATERIAL materials[1] = {0};
		materials[0].MatD3D.Ambient = color;
		materials[0].MatD3D.Diffuse = color;
		if(texture && texture[0]){
			materials[0].pTextureFilename = texture;
		}
		//create engine's mesh
		IMesh *result = new IMesh();
		if(!result->LoadFromMesh(resultMesh, d3ddev)){
			result->Dispose();
			return NULL;
		}
		result->LoadMaterials(d3ddev, 1, materials);
		result->SetName("Plane");
		return result;
	}
}