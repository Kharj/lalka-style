#pragma once
#include "stdafx.h"
#include "LolEngine.h"

#define LOG_FILENAME "lolengine_log.txt"

namespace LolEngine{

	class Log
	{
	//private:
	//	Log(); //private constructor 
	public:

		static void Write(std::string text){
			std::ofstream log_fs(LOG_FILENAME, std::ios_base::app);
			if(!log_fs) return;
			time_t rawtime;
			struct tm  timeinfo;
			time ( &rawtime );
			localtime_s (&timeinfo, &rawtime );
			char tmpTime[100];
			asctime_s(tmpTime, &timeinfo);
			if(strlen(tmpTime)) tmpTime[strlen(tmpTime)-1]='\0';//to remove newline
			log_fs<<tmpTime<<" : "<<text<<std::endl;
			log_fs.close();
		}
		static void Write(std::wstring text){
			std::wofstream log_fs(LOG_FILENAME, std::ios_base::app);
			if(!log_fs) return;
			time_t rawtime;
			struct tm  timeinfo;
			time ( &rawtime );
			localtime_s (&timeinfo, &rawtime );
			char tmpTime[100];
			asctime_s(tmpTime, &timeinfo);
			if(strlen(tmpTime)) tmpTime[strlen(tmpTime)-1]='\0';//to remove newline
			log_fs<<tmpTime<<" : "<<text<<std::endl;
			log_fs.close();
		}
		static void Write(std::string text, int number){
			char tmp[100];
			sprintf_s(tmp, "%d",number);
			text.append(tmp);
			Write(text);
		}
		static void Write(std::wstring text, int number){
			wchar_t tmp[100];
			wsprintfW(tmp, L"%d",number);
			text.append(tmp);
			Write(text);
		}
		static void Write(std::string text, float number){
			char tmp[100];
			sprintf_s(tmp, "%f",number);
			text.append(tmp);
			Write(text);
		}
		static void Write(std::wstring text, float number){
			wchar_t tmp[100];
			wsprintfW(tmp, L"%f",number);
			text.append(tmp);
			Write(text);
		}

	};
}

