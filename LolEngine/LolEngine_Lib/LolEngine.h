#pragma once
//Includes

#include "Log.h"
#include "Settings.h"
#include "GraphicsEngine.h"
#include "SoundEngine.h"
#include "XInputController.h"
#include "InputEngine.h"
#include "NetworkEngine.h"
#include "ILight.h"
#include "ICamera.h"
#include "IMesh.h"
#include "IScene.h"
#include "ISceneObject.h"
#include "LolEngine.h"
//#include "stdafx.h"



namespace LolEngine{
	
	//Checker texture
	const unsigned char Default_Texture_Mas[104]={66,77,104,0,0,0,0,0,0,0,54,0,0,0,40,0,0,0,4,0,0,0,4,0,0,0,1,0,24,0,0,0,0,0,50,0,0,0,18,11,0,0,18,11,0,0,0,0,0,0,0,0,0,0,0,0,0,255,0,255,0,0,0,255,0,255,255,0,255,0,0,0,255,0,255,0,0,0,0,0,0,255,0,255,0,0,0,255,0,255,255,0,255,0,0,0,255,0,255,0,0,0,0,0};
	const unsigned int Default_Texture_Mas_Size=104;

	class CEngine
	{
	protected:
		float fpsCounterTime;
		int fpsCounterFrames;
		int fpsCounterResult;
		static DWORD timeStart;
		ENG_STATUS status;
		//settings
		CSettings *pSettings;
		//engines
		CGraphicsEngine graphicsEngine;
		CSoundEngine soundEngine;
		CInputEngine inputEngine;
		CNetworkEngine networkEngine;
		//scene
		IScene *currentScene;
		unsigned int currentSceneNum;
		std::vector <IScene*> scenes;
		//to override
		virtual bool CreateScenes();

	public:
		CEngine(void);
		virtual ~CEngine(void);
		virtual bool Init(HWND hWnd, CSettings *_pSetings);
		virtual void Dispose();
		void Draw();
		virtual void Update();
		static float GetTime();
		int GetFPS();
		int GetPing();
		bool SelectScene(unsigned int n);
		virtual void LoadingScreen();
		ENG_STATUS GetStatus();

	};

	IMesh * CreateSkyBoxMesh(LPDIRECT3DDEVICE9 d3ddev, D3DXVECTOR3 size, LPSTR texTop, LPSTR texLeft, LPSTR texFront, LPSTR texRight, LPSTR texBack, LPSTR texBottom);
	IMesh * CreatePlaneMesh(LPDIRECT3DDEVICE9 d3ddev, float sizeX, float sizeY, LPSTR texture, D3DXCOLOR color = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
}

