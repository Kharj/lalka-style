#pragma once

namespace LolEngine{
	const char SETTINGS_FILENAME[] = "settings.txt";
	class CSettings
	{
	public:
		CSettings(void);
		~CSettings(void);
		CSettings *newSettings;//copy of this instance, to be saved separately
		bool LoadFromFile();
		bool SaveToFile();
		float GetAspectRatio();
	protected:
		int TextureFilter;
	public:	
		DWORD startTime;
		bool Fullscreen;
		bool AutoResolution;
		bool XInput;
		bool SplitscreenVertical;
		bool FpsNoLimit;
		int Height;
		int Width;
		float MouseScale;
		int Volume;
		DWORD MultiSamplingType;
		DWORD TextureMinFilter;
		DWORD TextureMaxAnisotropy;
		int Port;
	};
}

