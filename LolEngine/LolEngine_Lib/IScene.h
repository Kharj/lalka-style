#pragma once
namespace LolEngine{
class IScene
{
protected:
	CGraphicsEngine* graphicsEngine;
	CSoundEngine* soundEngine;
	CInputEngine* inputEngine;
	CNetworkEngine* networkEngine;
	CSettings *settings;
	//ICamera *currentCamera;
	ENG_STATUS status;
	int lightCounter;
	void SetCamera(ICamera* pCamera);
	void SetLight(ILight* iLight);
	void SetLight(D3DLIGHT9* d3dLight);
public:
	IScene(void);
	virtual ~IScene(void);
	//bool loaded;

	virtual bool Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, CNetworkEngine* _networkEngine, CSettings* _pSettings);
	virtual void Update(float deltaTime);
	//ABSTRACT 
	virtual void Draw() = 0;
	virtual void Dispose();
	ENG_STATUS GetStatus();
};
}

