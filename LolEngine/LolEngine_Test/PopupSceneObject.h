#pragma once
class PopupSceneObject :
	public ISceneObject
{
protected:
	IMesh *popupMesh;
	float timer;
	float fadeInTime;
	float fadeOutTime;
	float showTime;
	bool toHide;
	bool toShow;

public:
	PopupSceneObject(void);
	virtual ~PopupSceneObject(void);
	virtual void Update(float deltaTime);
	virtual void Dispose();
	bool Create(float _width, float _height, LPSTR textureName, float _fadeInTime = 0.2f, float _showTime = 3.0f, float _fadeOutTime = 0.6f);
	void Show(bool autoHide = false);
	void Hide();
};

