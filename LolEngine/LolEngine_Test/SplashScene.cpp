#include "stdafx.h"
#include "Main.h"
#include "SplashScene.h"


SplashScene::SplashScene(void)
{
	imageObject = NULL;
	fadeObject = NULL;
	sceneTime = 0.0f;
}


SplashScene::~SplashScene(void)
{
}
bool SplashScene::Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, CNetworkEngine* _networkEngine, CSettings* _pSettings){
		if(!IScene::Init(_graphicsEngine, _soundEngine, _inputEngine, _networkEngine, _pSettings)){//inil from base class
			return false;
		}
		float splashSize = 10.0f;
		sceneTime = 0.0f;
		//Image
		imageObject = new ISceneObject();
		if(!imageObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
			imageObject->Dispose();
			delete imageObject;
			return false;
		}
		IMesh * backMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), splashSize*2.0f, splashSize*2.0f, NULL, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
		if(backMesh){
			backMesh->SetPosition(D3DXVECTOR3(0.0f, 0.0f, -0.01f));
			imageObject->AddMesh(backMesh);
		}
		IMesh * imageMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), splashSize, splashSize, "..\\Data\\Splash.png");
		if(imageMesh){
			imageObject->AddMesh(imageMesh);
		}
		//Fade to black
		fadeObject = new ISceneObject();
		if(!fadeObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
			fadeObject->Dispose();
			delete fadeObject;
			return false;
		}
		IMesh * fadeMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), splashSize, splashSize, NULL, D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
		if(fadeMesh){
			fadeMesh->SetStateColor(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_SELECTARG2);
			fadeMesh->SetStateAlpha(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_SELECTARG2);
			fadeObject->AddMesh(fadeMesh);
		}
		fadeObject->SetPosition(D3DXVECTOR3(0.0f, 0.0f, 0.01f));
		//Camera
		//camera = new ICamera();
		camera.SetEyePosition(D3DXVECTOR3(0.0f,0.0f,10.0f));
		camera.SetLookAt(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		camera.SetProjection(D3DXToRadian(45.0f), settings->GetAspectRatio(), 1.0f, 1000.0f);
		//light
		light = ILight(D3DXVECTOR3(0.0f,0.0f,10.0f), D3DXVECTOR3(0.0f,0.0f,-1.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		status = ENG_OK;
		return true;
	}
	void SplashScene::Update(float deltaTime){
		if(status == ENG_NOTINIT) return; //No update till scenenot inited
		IScene::Update(deltaTime);//Update base class
		sceneTime += deltaTime;
		if(sceneTime>1.0f){
			if(sceneTime<1.7f){
				fadeObject->GetMesh(0)->GetMaterial(0)->Diffuse = D3DXCOLOR(0.0f, 0.0f, 0.0f, (sceneTime-1.0f)/0.7f);
			}else{
				status = ENG_NEXTSCENE;
			}
		}
		if(inputEngine->IsKeyPressed(DIK_RETURN) || inputEngine->IsKeyPressed(DIK_ESCAPE) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_A) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_B) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_START)){
			status = ENG_NEXTSCENE;
		}
		

	}
	void SplashScene::Draw(){
		lightCounter = 0;//to proper set lights
		//IScene::Draw();//Draw base class NO!
		//Set shit
		LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();

		
		SetLight(&light); //turn on da light
#pragma region SetStates
		//Light
		//d3ddev->SetRenderState(D3DRS_LIGHTING, true);    // turn off the 3D lighting
		//d3ddev->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
		//d3ddev->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_MATERIAL);
		//d3ddev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
		d3ddev -> SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); // ��������� ��������� Direct3D
		d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
		d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
		d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
		d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		//filter
		d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); 
		d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); 
		d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, settings->TextureMinFilter); 
		d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, settings->TextureMaxAnisotropy);
#pragma endregion
		SetCamera(&camera);
		//Begin
		graphicsEngine->BeginDraw();
		//Objects
		imageObject->Draw();
		fadeObject->Draw();
		//End
		
		//d3ddev->SetRenderState(D3DRS_LIGHTING, false);    // turn off the 3D lighting
		graphicsEngine->EndDraw();
	}
	void SplashScene::Dispose(){
		IScene::Dispose();//dispose base class
		//Objects
		imageObject->Dispose();
		delete imageObject;
		imageObject = NULL;

		fadeObject->Dispose();
		delete fadeObject;
		fadeObject = NULL;
		//Camera
	}

