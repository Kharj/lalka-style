#pragma once

class TestScene : public IScene
{
protected:
	ISceneObject *testObject;
	ISceneObject *environmentObject;
	ICamera camera;
	ILight light;
public:
	TestScene(void);
	virtual ~TestScene(void);
	virtual bool Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, LolEngine::CNetworkEngine* _networkEngine, CSettings* _pSettings);
	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void Dispose();

};

