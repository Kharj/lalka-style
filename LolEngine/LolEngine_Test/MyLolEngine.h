#pragma once

class CMyLolEngine :	public CEngine
{
public:
	CMyLolEngine(void);
	virtual ~CMyLolEngine(void);
	void Update();	
	void Dispose();

protected:
	//to override
	bool CreateScenes();

	GamePlayScene* gamePlayScene;
	void LoadingScreen();
	IMesh *loadingScreenMesh;
};

