#include "StdAfx.h"
#include "Main.h"
#include "MenuPage.h"


MenuPage::MenuPage(void)
{
	count=0;
}


MenuPage::~MenuPage(void)
{
	count =0;
	entries.clear();
}
void MenuPage::SetVisible(bool a){
	if(entries.empty()) return;
	for(std::vector<MenuEntrySceneObject*>::iterator i=entries.begin(); i!=entries.end(); ++i){
			if(*i){
				(*i)->visible = a;
			}
		}
}
void MenuPage::MoveEntries(float height, float distance, float x, float z){
	if(entries.empty()) return;
	int count = entries.size();
	float startY = ((float)count*height + (float)(count-1)*distance) * 0.5f;
	for(int i=0; i<count;++i){
		MenuEntrySceneObject* e = entries[i];
		if(e){
			float y = startY - (height*0.5f) - ((float)i*(height+distance));
			e->SetPosition(D3DXVECTOR3(x, y, z));
		}
	}
}
void MenuPage::Add(MenuEntrySceneObject* a){
	entries.push_back(a);
	count = entries.size();
}
void MenuPage::Clear(){
	entries.clear();
}

MenuEntrySceneObject* MenuPage::GetEntry(unsigned int a){
	if(a >= entries.size()) return NULL;
	return entries[a];
}
int MenuPage::GetCount(){
	return count;
}
void MenuPage::SetAllDefault(){
	if(entries.empty()) return;
	for(std::vector<MenuEntrySceneObject*>::iterator i=entries.begin(); i!=entries.end(); ++i){
			if(*i){
				(*i)->Default();
			}
		}
}