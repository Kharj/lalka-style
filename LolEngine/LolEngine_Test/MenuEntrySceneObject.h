#pragma once
class MenuEntrySceneObject :	public ISceneObject
{
protected:
	IMesh * mainBackMesh;
	IMesh * hoverBackMesh;
	IMesh * activeBackMesh;
	IMesh * textMesh;
	static LPD3DXFONT pFont; //<== static!!!
	int index;
	float width, height;
	bool onceCreated;
	unsigned int fontHeight;

public:
	MenuEntrySceneObject(void);
	virtual ~MenuEntrySceneObject(void);
	virtual void Dispose();
	bool Create(int _index, float _width, float _height, D3DXCOLOR mainColor, D3DXCOLOR hoverColor, D3DXCOLOR activeColor, LPCSTR text, unsigned int fontHeight, D3DXCOLOR textColor);
	bool SetText(LPCSTR text, D3DXCOLOR textColor = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	float GetWidth();
	float GetHeight();
	int GetIndex();
	void Active();
	void Hover();
	void Default();
	bool enable;
};

