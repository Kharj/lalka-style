#include "stdafx.h"
#include "Main.h"
#include "MyLolEngine.h"

CMyLolEngine::CMyLolEngine(void)
{
}


CMyLolEngine::~CMyLolEngine(void)
{
}

bool CMyLolEngine::CreateScenes(){
	//Clean vector
	scenes.clear();
	//loading screen
	loadingScreenMesh = NULL;
	//splash screen scene
	IScene* splashScene;
	splashScene = new SplashScene();
	scenes.push_back(splashScene);
	//menu scene
	IScene* menuScene;
	menuScene = new MenuScene();
	scenes.push_back(menuScene);

	//tmp scene
	//GamePlayScene* tmpScene;
	gamePlayScene = new GamePlayScene();
	//gamePlayScene->splitScreen = true; //activate split screen
	scenes.push_back(gamePlayScene);
	//test selects
	return true;
}
void CMyLolEngine::LoadingScreen(){
	if(!loadingScreenMesh)
		loadingScreenMesh = CreatePlaneMesh(graphicsEngine.GetDevice(), 10.0f, 2.5f, "..\\Data\\Loading.png");
	D3DXMATRIX matView, matProjection;
	D3DXVECTOR3 a(15.0f*pSettings->GetAspectRatio(), 18.75f, 20.0f), b(15.0f*pSettings->GetAspectRatio(), 18.75f, 0.0f), c(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&matView, &a, &b, &c);
	D3DXMatrixPerspectiveFovLH(&matProjection, D3DXToRadian(90), pSettings->GetAspectRatio(), 1.0f, 100.0f);
	graphicsEngine.GetDevice()->SetTransform(D3DTS_VIEW, &matView);
	graphicsEngine.GetDevice()->SetTransform(D3DTS_PROJECTION, &matProjection);
	graphicsEngine.GetDevice() -> SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); // ��������� ��������� Direct3D

	graphicsEngine.BeginDraw();
	graphicsEngine.GetDevice()->Clear(0,NULL,D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 0.0f,0);
	loadingScreenMesh->Draw(graphicsEngine.GetDevice(),D3DXVECTOR3(0.0f,0.0f,0.0f),D3DXVECTOR3(0.0f,0.0f,0.0f), D3DXVECTOR3(1.0f,1.0f,1.0f));
	graphicsEngine.EndDraw();
}
void CMyLolEngine::Dispose(){
	CEngine::Dispose();
	if(loadingScreenMesh){
		loadingScreenMesh->Dispose(); 
		loadingScreenMesh = NULL;
	}
}
void CMyLolEngine::Update(){

	//call base update
	CEngine::Update();
	ENG_STATUS sceneStatus = ENG_OK;
	if(currentScene) sceneStatus = currentScene->GetStatus();

	switch(sceneStatus){
	case ENG_EXITAPP:
		status = ENG_EXITAPP;
		break;
	case ENG_NEXTSCENE:
		//X3
		gamePlayScene->physicsServer = true;
		SelectScene(currentSceneNum+1);
		break;
	case ENG_NEXTSCENE_TYPE1:
		//Network
		gamePlayScene->networkGame = true;
		gamePlayScene->physicsServer = false;
		SelectScene(currentSceneNum+1);
		break;
	case ENG_NEXTSCENE_TYPE2:
		//Split screen
		gamePlayScene->physicsServer = true;
		gamePlayScene->splitScreen = true;
		SelectScene(currentSceneNum+1);
		break;
	case ENG_TOMENU:
		SelectScene(1);//menu
		break;
	}

}

