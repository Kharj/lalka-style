#include "stdafx.h"
#include "Main.h"
#include "PlayerDataStruct.h"

PlayerDataStruct::PlayerDataStruct(void)
{
	PositionX = 0.0f;
	PositionY = 0.0f;
	Score = 0;
	Num = 0;
}

PlayerDataStruct::~PlayerDataStruct(void)
{
}

std::string PlayerDataStruct::ToString(){
	std::ostringstream ss;
	ss<<"Player "<<Num;
	ss<<" Score "<<Score;
	ss<<" PositionX "<<PositionX;
	ss<<" PositionY "<<PositionY;
	ss<<" End";
	std::string res(ss.str());
	return res;
}
bool PlayerDataStruct::ReadString(std::string str){
	PositionX = 0.0f;
	PositionY = 0.0f;
	Score = 0;
	Num = 0;
	if(str.empty()) return false;
	std::istringstream ss(str);
	if(!ss) return false;
	std::string trash;
	ss>>trash;
	ss>>Num;
	ss>>trash;
	ss>>Score;
	ss>>trash;
	ss>>PositionX;
	ss>>trash;
	ss>>PositionY;
	std::string end("");
	ss>>end;
	if(end != "End") return false;
	this->Num = Num;
	this->Score = Score;
	this->PositionX = PositionX;
	this->PositionY = PositionY;
	return true;
}

void PlayerDataStruct::Reset(){
	PositionX = 0.0f;
	PositionY = 0.0f;
	Score = 0;
	Num = 0;
}