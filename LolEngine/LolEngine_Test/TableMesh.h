#pragma once
class TableMesh :
	public IMesh
{
public:
	virtual bool Draw(LPDIRECT3DDEVICE9 d3ddev, D3DXVECTOR3 objectPosition, D3DXVECTOR3 objectRotation, D3DXVECTOR3 objectScale);
};

