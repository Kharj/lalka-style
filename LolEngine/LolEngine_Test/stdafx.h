// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // ��������� ����� ������������ ���������� �� ���������� Windows
// ����� ���������� Windows:
#include <windows.h>

// ����� ���������� C RunTime
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
//STL
#include <vector>
#include <string>
#include <sstream>
//Engine
#include "..\\LolEngine_Lib\\stdafx.h"
#include "..\\LolEngine_Lib\\LolEngine.h"
//Physics Engine
#include "..\\Box2D\\Box2D.h"

//#pragma comment(lib, "..\\Debug\\LolEngine_Lib.lib")
//#pragma comment(lib, "..\\Debug\\Box2D.lib")
using namespace LolEngine;