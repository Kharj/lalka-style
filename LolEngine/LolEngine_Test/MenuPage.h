#pragma once
class MenuPage
{
protected:
	std::vector<MenuEntrySceneObject*> entries;
	int count;
public:
	MenuPage(void);
	~MenuPage(void);
	void SetVisible(bool a);
	MenuEntrySceneObject* GetEntry(unsigned int a);
	void MoveEntries(float height, float distance, float x, float z);
	void Add(MenuEntrySceneObject* a);
	int GetCount();
	void Clear();
	void SetAllDefault();
};

