#include "StdAfx.h"
#include "Main.h"
#include "Physics2D.h"


Physics2D::Physics2D(void) 
	: world(b2Vec2(0.0f, 0.0f)) {
	//Positions
	tableLength = 8.72f;//length of play field
	tableWidth = 5.34f;//width of play field
	malledWidth = 0.62f;//Diameter of mallet
	puckWidth = malledWidth;//Diameter of puck

	//create world, bodies


	//Create borders
	//bot
	b2BodyDef tableBodyDef;
	tableBodyDef.position.Set(0.0f, -tableLength*0.5f);
	tabelBodies[0] = world.CreateBody(&tableBodyDef);
	b2PolygonShape tableBox;
	tableBox.SetAsBox(tableWidth*1.1f, 0.01f);
	tabelBodies[0]->CreateFixture(&tableBox, 0.0f);
	//top
	tableBodyDef.position.Set(0.0f, tableLength*0.5f);
	tabelBodies[1] = world.CreateBody(&tableBodyDef);
	tableBox.SetAsBox(tableWidth*1.1f, 0.01f);
	tabelBodies[1]->CreateFixture(&tableBox, 0.0f);
	//left
	tableBodyDef.position.Set(-tableWidth*0.5f, 0.0f);
	tabelBodies[2] = world.CreateBody(&tableBodyDef);
	tableBox.SetAsBox(0.01f, tableLength*1.1f);
	tabelBodies[2]->CreateFixture(&tableBox, 0.0f);
	//right
	tableBodyDef.position.Set(tableWidth*0.5f, 0.0f);
	tabelBodies[3] = world.CreateBody(&tableBodyDef);
	tableBox.SetAsBox(0.01f, tableLength*1.1f);
	tabelBodies[3]->CreateFixture(&tableBox, 0.0f);
	
	//Dyamic objects
	//mallet1
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(0.0f, 0.0f);
	mallet1Body = world.CreateBody(&bodyDef);
	b2CircleShape circleShape;
	circleShape.m_p.Set(0.0f, 0.0f);
	circleShape.m_radius = malledWidth;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circleShape;
	fixtureDef.density = 10.0f;
	fixtureDef.friction = 1.0f;
	mallet1Body->CreateFixture(&fixtureDef);
	b2MassData mass;
	ZeroMemory(&mass, sizeof(mass));
	mass.mass = 0.3f;
	mallet1Body->SetMassData(&mass);

	//mallet2
	mallet2Body = world.CreateBody(&bodyDef);
	mallet2Body->CreateFixture(&fixtureDef);
	mallet2Body->SetMassData(&mass);

	//Puck
	puckBody = world.CreateBody(&bodyDef);
	b2FixtureDef puckFixtureDef;
	puckFixtureDef.shape = &circleShape;
	puckFixtureDef.density = 10.0f;
	puckFixtureDef.friction = 1.1f;
	puckBody->CreateFixture(&puckFixtureDef);
	mass.mass = 0.1f;
	puckBody->SetMassData(&mass);
	//Reset(); 	//set all 0
}


Physics2D::~Physics2D(void){
	//delete world, bodies
}

void Physics2D::SetMallet1Position(float x, float y){
	//mallet1Body->SetLinearVelocity(b2Vec2( 0,0));
	mallet1Body->SetTransform(b2Vec2(x, y), 0.0f);
	//b2Vec2 force(x - mallet1Body->GetPosition().x, y - mallet1Body->GetPosition().y);
	//mallet1Body->ApplyForce(force, b2Vec2(x, y), true);

	//b2Vec2 vel = mallet1Body->GetPosition() - b2Vec2(x, y);
	//mallet1Body->SetLinearVelocity( vel );
}

void Physics2D::SetMallet2Position(float x, float y){
	mallet2Body->SetTransform(b2Vec2(x, y), 0.0f);
	//b2Vec2 force(x - mallet2Body->GetPosition().x, y - mallet2Body->GetPosition().y);
	//mallet2Body->ApplyForce(force, b2Vec2(x, y), true);
}

void Physics2D::GetPuckPosition(float &x, float &y){
	x = puckBody->GetPosition().x;
	y = puckBody->GetPosition().y;
}
void Physics2D::SetPuckPosition(float x, float y){
	puckBody->SetTransform(b2Vec2(x, y), 0.0f);
}

void Physics2D::Reset(){
	//Place all in start position
	fullTime = 0.0f;
	//Pos
	mallet1Body->SetTransform(b2Vec2(0.0f, 0.0f), 0.0f);
	mallet2Body->SetTransform(b2Vec2(0.0f, 0.0f), 0.0f);
	puckBody->SetTransform(b2Vec2(0.0f, 0.0f), 0.0f);
}

void Physics2D::Update(float deltaTime){
	//Update world afterdeltaTime seconds
	fullTime+=deltaTime;

	world.Step(deltaTime, 10, 10);
}