#include "stdafx.h"
#include "Main.h"
#include "MenuScene.h"


MenuScene::MenuScene(void)
{
	menuZ = 100.0f;
	backgroundObject = NULL;
	popupControls = NULL;
	popupSettings = NULL;
	popupConnect = NULL;
	sceneTime = 0.0f;
	fontHeight = 30;
	entryDistance = 2.0f;
	entryHeight = 5.0f;
	entryWidth = 56.0f;
	pagesNum = 10;
	pages = NULL;
	currentPage=0;
	currentEntry = -1;
	pressTimer = 0.0f;
	defaultPressTime = 0.21f;
}


MenuScene::~MenuScene(void)
{
}
bool MenuScene::Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, CNetworkEngine* _networkEngine, CSettings* _pSettings){
	if(!IScene::Init(_graphicsEngine, _soundEngine, _inputEngine, _networkEngine, _pSettings)){//inil from base class
		return false;
	}
	sceneTime = 0.0f;
	ipAddr = networkEngine->GetServerAddress();
	//Image
	backgroundObject = new ISceneObject();
	if(!backgroundObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
		backgroundObject->Dispose();
		delete backgroundObject;
		return false;
	}
	IMesh * backgroundMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), menuZ*2.0f, menuZ*2.0f, "..\\Data\\menuBack.png");
	if(backgroundMesh){
		backgroundMesh->SetPosition(D3DXVECTOR3(0.0f, 0.0f, -1.0f));
		backgroundObject->AddMesh(backgroundMesh);
	}
	//CONTENT
	pages = new MenuPage[pagesNum];
	float menuX = 0.415f*menuZ*settings->GetAspectRatio() - entryWidth*0.5f;// magic, must be negative 0_o
	/*//page 0 entry 0
	MenuEntrySceneObject * tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.0f, 1.0f, 0.0f, 0.0f), D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.0f), D3DXCOLOR(0.1f, 0.1f, 0.1f, 0.1f), " Page 1", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	tmpObject->enable = false;
	pages[0].Add(tmpObject);
	AddObject(tmpObject);
	*/

	//page 0 entry 0
	MenuEntrySceneObject * tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " Play Online", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[0].Add(tmpObject);
	AddObject(tmpObject);

	//page 0 entry 1
	/*
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " Play __", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[0].Add(tmpObject);
	AddObject(tmpObject);\
	*/


	//page 0 entry 1
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " Play Split-screen", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[0].Add(tmpObject);
	AddObject(tmpObject);

	//page 0 entry 2
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " Settings", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[0].Add(tmpObject);
	AddObject(tmpObject);

	//page 0 entry 3
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " Exit", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[0].Add(tmpObject);
	AddObject(tmpObject);
	pages[0].MoveEntries(entryHeight, entryDistance, menuX, 0.0f);

	//page 1 entry 0 - FULLSCREEN
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), "", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[1].Add(tmpObject);
	AddObject(tmpObject);

	//page 1 entry 1 - Multisampling
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), 
		"", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[1].Add(tmpObject);
	AddObject(tmpObject);

	//page 1 entry 2 - Filter
	/*case 0:
	TextureMinFilter = D3DTEXF_NONE; break;
	case -1:
	TextureMinFilter = D3DTEXF_POINT; break;
	case -2:
	TextureMinFilter = D3DTEXF_LINEAR; break;
	case 1: case 2: case 4: case 8: case 16: 
	TextureMinFilter = D3DTEXF_ANISOTROPIC; 
	TextureMaxAnisotropy = TextureFilter; break;*/
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;

	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), 
		"", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[1].Add(tmpObject);
	AddObject(tmpObject);

	//page 1 entry 3 - MOUSE
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;

	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), "", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[1].Add(tmpObject);
	AddObject(tmpObject);

	//page 1 entry 4 - BACK
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " Save", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[1].Add(tmpObject);
	AddObject(tmpObject);

	pages[1].MoveEntries(entryHeight, entryDistance, menuX, 0.0f);
	
	//Connect page 2 

	//page 2 entry 0 connect
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " CONNECT", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[2].Add(tmpObject);
	AddObject(tmpObject);

	//page 2 entry 1 ip
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	//IP text
	std::string tmpStr(" IP: ");
	tmpStr.append(networkEngine->GetServerAddress());
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), tmpStr.c_str(), fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[2].Add(tmpObject);
	AddObject(tmpObject);

	//page 2 entry 2 port
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	//IP text
	tmpStr = " Port: ";
	char tmpChr[100];
	_itoa_s(settings->Port, tmpChr, 10);
	tmpStr.append(tmpChr);
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), tmpStr.c_str(), fontHeight, D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f));
	pages[2].Add(tmpObject);
	AddObject(tmpObject);

	

	//page 2 entry 3 back
	tmpObject = new MenuEntrySceneObject();
	if(!tmpObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpObject->Create(0, entryWidth, entryHeight, D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f), D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f), D3DXCOLOR(0.2f, 1.0f, 0.2f, 1.0f), " Back", fontHeight, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
	pages[2].Add(tmpObject);
	AddObject(tmpObject);

	pages[2].MoveEntries(entryHeight, entryDistance, menuX, 0.0f);

	//set settings text
	SetSettingsPageText();
	//select page
	SelectPage(0);
	//END CONTENT

	//Popup controls help
	popupControls = new PopupSceneObject();
	if(!popupControls->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	popupControls->Create(8.0f* 8.0f, 8.0f, "..\\Data\\MenuPopupControls.png", 0.5f, 4.0f, 1.0f);
	popupControls->SetPosition(D3DXVECTOR3(0.0f, -0.414f*menuZ + 8.0f, 0.0f));
	popupControls->Show(true);
	AddObject(popupControls);
	//Popup settings help
	popupSettings = new PopupSceneObject();
	if(!popupSettings->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	popupSettings->Create(8.0f* 8.0f, 8.0f, "..\\Data\\MenuPopupSettings.png", 0.3f, 4.5f, 1.0f);
	popupSettings->SetPosition(D3DXVECTOR3(0.0f, -0.414f*menuZ + 8.0f, 0.0f));
	AddObject(popupSettings);
	//Popup connect help
	popupConnect = new PopupSceneObject();
	if(!popupConnect->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	popupConnect->Create(8.0f* 8.0f, 8.0f, "..\\Data\\MenuPopupConnect.png", 0.3f, 4.5f, 1.0f);
	popupConnect->SetPosition(D3DXVECTOR3(0.0f, -0.414f*menuZ + 8.0f, 0.0f));
	AddObject(popupConnect);

	//Logo
	ISceneObject *tmpLogo = new ISceneObject();
	if(!tmpLogo->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	tmpLogo->SetPosition(D3DXVECTOR3(0.0f, 0.415f*menuZ - 12.0f ,0.0f));
	IMesh *logoMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), 80.0f, 20.0f, "..\\Data\\MenuLogo.png");
	if(logoMesh) tmpLogo->AddMesh(logoMesh);
	AddObject(tmpLogo);
	//Camera
	//camera = new ICamera();
	camera.SetEyePosition(D3DXVECTOR3(0.0f,0.0f,menuZ));
	camera.SetLookAt(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	camera.SetProjection(D3DXToRadian(45.0f), settings->GetAspectRatio(), 1.0f, 150.0f);
	//light
	light = ILight(D3DXVECTOR3(0.0f,0.0f,10.0f), D3DXVECTOR3(0.0f,0.0f,-1.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	status = ENG_OK;
	return true;
}

void MenuScene::SetSettingsPageText(){
	//0
	pages[1].GetEntry(0)->SetText((settings->newSettings->Fullscreen)?" Fullscreen: On":" Fullscreen: Off");
	//1
	char tmpText [100] = " Multisampling: ";
	char tmpTextNum [10] = "";
	_itoa_s(settings->newSettings->MultiSamplingType, tmpTextNum, 10);
	strcat_s(tmpText, tmpTextNum);
	pages[1].GetEntry(1)->SetText(tmpText);
	//2
	strcpy_s(tmpText, " Texture filter: ");
	if(settings->newSettings->TextureMinFilter == D3DTEXF_POINT){
		strcat_s(tmpText, "POINT");
	}else if(settings->newSettings->TextureMinFilter == D3DTEXF_LINEAR){
		strcat_s(tmpText, "LINEAR");
	}else if(settings->newSettings->TextureMinFilter == D3DTEXF_ANISOTROPIC){
		strcat_s(tmpText, "ANI X");
		char tmpTextNum [10] = "";
		_itoa_s(settings->newSettings->TextureMaxAnisotropy, tmpTextNum, 10);
		strcat_s(tmpText, tmpTextNum);
	}
	pages[1].GetEntry(2)->SetText(tmpText);
	//3
	strcpy_s(tmpText, " < Mouse sense: ");
	int mouseInt = (int)(settings->newSettings->MouseScale * 100.0f + 0.5f);
	//char tmpTextNum[10] = "";
	_itoa_s(mouseInt, tmpTextNum, 10);
	strcat_s(tmpText, tmpTextNum);
	strcat_s(tmpText, " >");
	pages[1].GetEntry(3)->SetText(tmpText);
}
void MenuScene::Update(float deltaTime){
	if(status == ENG_NOTINIT) return; //No update till scenenot inited
	IScene::Update(deltaTime);//Update base class
	pressTimer -= deltaTime;
	sceneTime += deltaTime;
	//Update objects
	for(std::vector<ISceneObject*>::iterator i=objects.begin();i!=objects.end();i++){
		if(*i){
			(*i)->Update(deltaTime);
		}
	}
	if(pressTimer < 0.01f){
		bool isDown = inputEngine->IsKeyPressed(DIK_DOWN) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_DPAD_DOWN);
		bool isUp = inputEngine->IsKeyPressed(DIK_UP) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_DPAD_UP);
		bool isLeft = inputEngine->IsKeyPressed(DIK_LEFT) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_DPAD_LEFT);
		bool isRight = inputEngine->IsKeyPressed(DIK_RIGHT) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_DPAD_RIGHT);
		if(isDown || isUp){
			//left ip box
			if(ipAddr.empty()){
					ipAddr = networkEngine->GetServerAddress();
					std::string tmpStr(" IP: ");
					tmpStr += ipAddr;
					pages[currentPage].GetEntry(currentEntry)->SetText(tmpStr.c_str());
			}
			//down
			if(isDown){
				currentEntry++;
				if(currentEntry > pages[currentPage].GetCount() - 1) currentEntry = 0;
			}else{ //up
				currentEntry--;
				if(currentEntry < 0) currentEntry = pages[currentPage].GetCount() - 1;
			}
			//IP
			if(currentPage==2 && currentEntry==1){
				ipAddr.clear();
				pages[currentPage].GetEntry(currentEntry)->SetText(" IP:");
			}
			pages[currentPage].SetAllDefault();
			pages[currentPage].GetEntry(currentEntry)->Hover();
			pressTimer = defaultPressTime;
		}

		else if(isLeft || isRight){
			if(currentPage==1 && currentEntry==3){//mouse sense
				if(settings->newSettings->MouseScale < 9.95f && isRight){
					settings->newSettings->MouseScale += 0.1f;
				}else if(settings->newSettings->MouseScale > 0.15f && isLeft){
					settings->newSettings->MouseScale -= 0.1f;
				}
				if(settings->newSettings->MouseScale<0.1f) settings->newSettings->MouseScale = 0.1f;
				if(settings->newSettings->MouseScale>10.0f) settings->newSettings->MouseScale = 10.0f;
				char tmpText[100] = " < Mouse sense: ";
				int mouseInt = (int)(settings->newSettings->MouseScale * 100.0f + 0.5f);
				char tmpTextNum[10] = "";
				_itoa_s(mouseInt, tmpTextNum, 10);
				strcat_s(tmpText, tmpTextNum);
				strcat_s(tmpText, " >");
				pages[currentPage].GetEntry(currentEntry)->SetText(tmpText);
				pages[currentPage].GetEntry(currentEntry)->Hover();
			}
			pressTimer = defaultPressTime;
		}
		else if(inputEngine->IsKeyPressed(DIK_RETURN) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_A)){
			if(currentEntry==-1) return;
			//ok
			pages[currentPage].GetEntry(currentEntry)->Active();
			//Play Online
			if(currentPage==0 && currentEntry==0){
				//status = ENG_NEXTSCENE_TYPE1;
				SelectPage(2);
				popupControls->Hide();
				popupSettings->Hide();
				popupConnect->Show(true);
			}
			//Play offline
			//if(currentPage==0 && currentEntry==1) status = ENG_NEXTSCENE;
			//Play split-screen
			if(currentPage==0 && currentEntry==1) status = ENG_NEXTSCENE_TYPE2;

			//Page 2
			if(currentPage==0 && currentEntry==2){
				SelectPage(1);
				popupControls->Hide();
				popupConnect->Hide();
				popupSettings->Show(true);
			}
			//Exit
			if(currentPage==0 && currentEntry==3) status = ENG_EXITAPP;
			//Page 2 - entry 0 - fullscreen
			if(currentPage==1 && currentEntry==0){
				settings->newSettings->Fullscreen = !settings->newSettings->Fullscreen;
				pages[currentPage].GetEntry(currentEntry)->SetText(((settings->newSettings->Fullscreen)?" Fullscreen: On":" Fullscreen: Off"), D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
				pages[currentPage].GetEntry(currentEntry)->Hover();
			}
			//page 2 entry 1 - multisampling
			if(currentPage==1 && currentEntry==1){
				switch (settings->newSettings->MultiSamplingType)
				{
				case D3DMULTISAMPLE_NONE:
					settings->newSettings->MultiSamplingType = D3DMULTISAMPLE_2_SAMPLES;
					break;
				case D3DMULTISAMPLE_2_SAMPLES:
					settings->newSettings->MultiSamplingType = D3DMULTISAMPLE_4_SAMPLES;
					break;
				case D3DMULTISAMPLE_4_SAMPLES:
					settings->newSettings->MultiSamplingType = D3DMULTISAMPLE_8_SAMPLES;
					break;
				case D3DMULTISAMPLE_8_SAMPLES:
				default:
					settings->newSettings->MultiSamplingType = D3DMULTISAMPLE_NONE;
					break;
				}

				char tmpText [100] = " Multisampling: ";
				char tmpTextNum [10] = "";
				_itoa_s(settings->newSettings->MultiSamplingType, tmpTextNum, 10);
				strcat_s(tmpText, tmpTextNum);
				pages[currentPage].GetEntry(currentEntry)->SetText(tmpText);
				pages[currentPage].GetEntry(currentEntry)->Hover();
			}
			//page 2 entry 2 - filter
			if(currentPage==1 && currentEntry==2){
				if(settings->newSettings->TextureMinFilter == D3DTEXF_POINT) settings->newSettings->TextureMinFilter = D3DTEXF_LINEAR;
				else if(settings->newSettings->TextureMinFilter == D3DTEXF_LINEAR){
					settings->newSettings->TextureMinFilter = D3DTEXF_ANISOTROPIC;
					settings->newSettings->TextureMaxAnisotropy = 1;
				}else if(settings->newSettings->TextureMinFilter == D3DTEXF_ANISOTROPIC){
					switch(settings->newSettings->TextureMaxAnisotropy){
					case 1:
						settings->newSettings->TextureMaxAnisotropy = 2;
						break;
					case 2:
						settings->newSettings->TextureMaxAnisotropy = 4;
						break;
					case 4:
						settings->newSettings->TextureMaxAnisotropy = 8;
						break;
					case 8:
						settings->newSettings->TextureMaxAnisotropy = 16;
						break;
					default:
						settings->newSettings->TextureMinFilter = D3DTEXF_POINT;
						settings->newSettings->TextureMaxAnisotropy = 1;
						break;
					}
				}
				//label
				char tmpText [100] = " Texture filter: ";
				if(settings->newSettings->TextureMinFilter == D3DTEXF_POINT){
					strcat_s(tmpText, "POINT");
				}else if(settings->newSettings->TextureMinFilter == D3DTEXF_LINEAR){
					strcat_s(tmpText, "LINEAR");
				}else if(settings->newSettings->TextureMinFilter == D3DTEXF_ANISOTROPIC){
					strcat_s(tmpText, "ANI X");
					char tmpTextNum [10] = "";
					_itoa_s(settings->newSettings->TextureMaxAnisotropy, tmpTextNum, 10);

					strcat_s(tmpText, tmpTextNum);
				}
				pages[currentPage].GetEntry(currentEntry)->SetText(tmpText);
				pages[currentPage].GetEntry(currentEntry)->Hover();
			}
			//Page 2 entry 3
			//using arrows
			//page 2 entry 4 - save
			if(currentPage==1 && currentEntry==4){
				//save new settings when exit from this page
				settings->newSettings->SaveToFile();
				//apply sense and texture filter imediantly without restart
				settings->MouseScale = settings->newSettings->MouseScale;
				settings->TextureMinFilter = settings->newSettings->TextureMinFilter;
				settings->TextureMaxAnisotropy = settings->newSettings->TextureMaxAnisotropy;

				popupSettings->Hide();
				popupConnect->Hide();
				SelectPage(0);
			}
			if(currentPage==2 && currentEntry==0){//connect
				networkEngine->Connect(ipAddr);
				status = ENG_NEXTSCENE_TYPE1;
			}
			if(currentPage==2 && currentEntry==3){//back
				popupSettings->Hide();
				popupConnect->Hide();
				SelectPage(0);
			}


			pressTimer = defaultPressTime;

		}
		else if(inputEngine->IsKeyPressed(DIK_ESCAPE) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_START)|| inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_B)){
			//esc
			if(currentPage==1){//escape from setings
				settings->newSettings->LoadFromFile();
				SetSettingsPageText();//rebuuild labels
				SelectPage(0);
			}
			pressTimer = defaultPressTime;
		}
		if(currentPage==2 && currentEntry==1){//IP address input
			int length = ipAddr.length();
			if(length==3 || length==7 || length==11) ipAddr+=".";
			else if(length < 15){
				if(inputEngine->IsKeyPressed(DIK_0)) ipAddr+="0";
				else if(inputEngine->IsKeyPressed(DIK_1)) ipAddr+="1";
				else if(inputEngine->IsKeyPressed(DIK_2)) ipAddr+="2";
				else if(inputEngine->IsKeyPressed(DIK_3)) ipAddr+="3";
				else if(inputEngine->IsKeyPressed(DIK_4)) ipAddr+="4";
				else if(inputEngine->IsKeyPressed(DIK_5)) ipAddr+="5";
				else if(inputEngine->IsKeyPressed(DIK_6)) ipAddr+="6";
				else if(inputEngine->IsKeyPressed(DIK_7)) ipAddr+="7";
				else if(inputEngine->IsKeyPressed(DIK_8)) ipAddr+="8";
				else if(inputEngine->IsKeyPressed(DIK_9)) ipAddr+="9";
			}else if(inputEngine->IsKeyPressed(DIK_ESCAPE))
				ipAddr = networkEngine->GetServerAddress();

			if(length != ipAddr.length()){
				std::string tmpStr(" IP: ");
				tmpStr += ipAddr;
				pages[currentPage].GetEntry(currentEntry)->SetText(tmpStr.c_str());
				pressTimer = defaultPressTime;
			}
		}
	}



}
void MenuScene::Draw(){
	lightCounter = 0;//to proper set lights
	//IScene::Draw();//Draw base class NO!
	//Set shit
	LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();

	SetLight(&light); //turn on da light
#pragma region SetStates
	//Light
	//d3ddev->SetRenderState(D3DRS_LIGHTING, true);    // turn off the 3D lighting
	//d3ddev->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
	//d3ddev->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_MATERIAL);
	//d3ddev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
	d3ddev -> SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); // ��������� ��������� Direct3D
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	//filter
	d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, settings->TextureMinFilter); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, settings->TextureMaxAnisotropy);
#pragma endregion
	SetCamera(&camera);
	//Begin
	graphicsEngine->BeginDraw();
	//Objects
	backgroundObject->Draw();
	//End
	for(std::vector<ISceneObject*>::iterator i=objects.begin();i!=objects.end();i++){
		if(*i){
			(*i)->Draw();
		}
	}
	//d3ddev->SetRenderState(D3DRS_LIGHTING, false);    // turn off the 3D lighting
	graphicsEngine->EndDraw();
}
void MenuScene::Dispose(){
	IScene::Dispose();//dispose base class

	//Objects
	backgroundObject->Dispose();
	delete backgroundObject;
	backgroundObject = NULL;
	//menu objects
	for(std::vector<ISceneObject*>::iterator i=objects.begin();i!=objects.end();i++){
		if(*i){
			(*i)->Dispose();
		}
	}
	if(pages) delete [] pages;
	//Camera
}
void MenuScene::SelectPage(int num){
	if(num >= pagesNum || num<0) return;
	currentPage = num;
	currentEntry = -1;
	pages[currentPage].SetAllDefault();
	for(int i=0; i<pagesNum;++i)
		if(i != num) pages[i].SetVisible(false);
	pages[num].SetVisible(true);
	if(pages[num].GetCount()){//not empty
		//Select first ERROR
		//currentEntry = 0;
		//pages[num].GetEntry(currentEntry)->Hover();
		//pressTimer = defaultPressTime*2.0f;
	}

}

void MenuScene::AddObject(ISceneObject * a){
	objects.push_back(a);
}

