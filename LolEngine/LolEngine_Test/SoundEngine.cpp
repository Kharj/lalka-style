#include "SoundEngine.h"

#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <mmsystem.h>
#include <iostream>
#include <fstream>
#include <conio.h>
#pragma comment (lib, "Winmm.lib")
using namespace std;

SoundEngine::SoundEngine(char * filename)
{
    ok = false;
    buffer = 0;
    HInstance = GetModuleHandle(0);
    ifstream infile(filename, ios::binary);
    if (!infile)
    {
         std::cout << "Wave::file error: "<< filename << std::endl;
        return;
    }
    infile.seekg (0, ios::end);   // get length of file
    int length = infile.tellg();
    buffer = new char[length];    // allocate memory
    infile.seekg (0, ios::beg);   // position to start of file
    infile.read (buffer,length);  // read entire file
    infile.close();
    ok = true;
}

SoundEngine::~SoundEngine()
{
    PlaySound(NULL, 0, 0); // STOP ANY PLAYING SOUND
    delete [] buffer;      // before deleting buffer.
}

void SoundEngine::playSound(bool async)
{
    if (!ok)
        return;
    if (async)
        PlaySound((LPCWSTR)buffer, HInstance, SND_MEMORY | SND_ASYNC);
    else
        PlaySound((LPCWSTR)buffer, HInstance, SND_MEMORY);
}

bool SoundEngine::isok()
{
    return ok;
}