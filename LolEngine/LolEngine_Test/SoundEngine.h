#pragma once
#include "stdafx.h"

class SoundEngine
{
public:
	SoundEngine(char * filename);
	~SoundEngine();
	void playSound(bool async = true);
	bool isok();
private:
	char * buffer;
	bool ok;
	HINSTANCE HInstance;
};