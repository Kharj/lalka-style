#include "stdafx.h"
#include "Main.h"
#include "TableMesh.h"


	bool TableMesh::Draw(LPDIRECT3DDEVICE9 d3ddev, D3DXVECTOR3 objectPosition, D3DXVECTOR3 objectRotation, D3DXVECTOR3 objectScale){
		HRESULT hr=0;
		if(!visible) return true;//���������
		//Texture render State
		//texture
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, textureState_colorarg1);
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2, textureState_colorarg2);
		d3ddev->SetTextureStageState(0, D3DTSS_COLOROP,  textureState_colorop);
		//alpha
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, textureState_alphaarg1);
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, textureState_alphaarg2);
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAOP,  textureState_alphaop);
		d3ddev->SetTextureStageState( 0, D3DTSS_TEXCOORDINDEX, 0 );
		//1 text
		//texture
		//d3ddev->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 0 );
		//d3ddev->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		//d3ddev->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_CURRENT);
		//d3ddev->SetTextureStageState(1, D3DTSS_COLOROP,  D3DTOP_MODULATE);


		D3DXMATRIX matRotateY, matRotateX, matRotateZ, matTranslate, matScale;

		D3DXMatrixScaling(&matScale, objectScale.x * scale.x, objectScale.y * scale.y, objectScale.z * scale.z);
		D3DXMatrixRotationX(&matRotateX, objectRotation.x + rotation.x);
		D3DXMatrixRotationY(&matRotateY, objectRotation.y + rotation.y);
		D3DXMatrixRotationZ(&matRotateZ, objectRotation.z + rotation.z);
		D3DXMatrixTranslation(&matTranslate, objectPosition.x + position.x, objectPosition.y + position.y, objectPosition.z + position.z);

		D3DXMATRIX matRes=(matScale * matRotateX * matRotateY * matRotateZ * matTranslate);

		for (DWORD j=0; j<numMaterials; j++)
		{
			//���� ��������� ��������� �������
			// Set the material and texture for this subset
			if(&meshMaterials[j]){
				hr = d3ddev->SetMaterial(&meshMaterials[j]);
				hr=hr;
			}
			if(meshTextures[j]){
				hr = d3ddev->SetTexture(0, meshTextures[j]);
				hr=hr;
			}else{
				d3ddev->SetTexture(0, NULL);
			}
			d3ddev->SetTransform(D3DTS_WORLD, &matRes);
			
			hr = mesh->DrawSubset(j);// Draw the mesh subset
			if(FAILED(hr)) return false;//IF ERROR DRAW
			d3ddev->SetTexture(0, NULL);

		}
		return true;
	}