#include "StdAfx.h"
#include "Main.h"
#include "PopupSceneObject.h"


PopupSceneObject::PopupSceneObject(void)
{
	popupMesh = NULL;
	timer = 0.0f;
	toShow = false;
	toHide = false;
}


PopupSceneObject::~PopupSceneObject(void)
{
}

bool PopupSceneObject::Create(float _width, float _height, LPSTR textureName,  float _fadeInTime, float _showTime, float _fadeOutTime){
	Dispose(); //clear
	showTime = _showTime;
	fadeInTime = _fadeInTime;
	fadeOutTime = _fadeOutTime;
	popupMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), _width, _height, textureName, D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	if(!popupMesh) return false;
	popupMesh->SetStateColor(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_MODULATE);
	AddMesh(popupMesh);
	return true;
}
void PopupSceneObject::Show(bool autoHide){
	float alpha = popupMesh->GetMaterial(0)->Diffuse.a;
	if(alpha > 0.99f && alpha < 1.01f) return;
	timer = 0.0f;
	toShow = true;
	toHide = autoHide;
}
void PopupSceneObject::Hide(){
	/*
	float alpha = popupMesh->GetMaterial(0)->Diffuse.a;
	if(alpha > -0.01f && alpha < 0.01f) return;
	timer = fadeInTime + showTimef;

	toShow = false;
	toHide = true;
	*/
	toShow = false;
	toHide = false;
	timer = 0.0f;
	popupMesh->GetMaterial(0)->Diffuse.a = 0.0f;
	int a=0;
}
void PopupSceneObject::Update(float deltaTime){
	ISceneObject::Update(deltaTime);//base
	if(!popupMesh) return;
	if(toShow || toHide){
		float alpha = 0.0f;
		if(timer > 0.0f && timer<fadeInTime) alpha = timer/fadeInTime;
		if(timer > fadeInTime){
			alpha = 1.0f;
			if(!toHide) 
				toShow = false;//stop
		}
		if(toHide && timer > showTime+fadeInTime && timer < fadeOutTime+showTime+fadeInTime)
			alpha = 1.0f - (timer-fadeInTime-showTime)/fadeOutTime;
		if(timer >= fadeOutTime+showTime+fadeInTime){
			alpha = 0.0f;
			toShow = false;
			toHide = false;
		}
		popupMesh->GetMaterial(0)->Diffuse.a  = alpha;
		timer += deltaTime;
	}	

}
void PopupSceneObject::Dispose(){
	ISceneObject::Dispose();//base
	timer = 0.0f;
	if(popupMesh){
		popupMesh->Dispose();
		popupMesh = NULL;
	}
}