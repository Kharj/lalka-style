#pragma once

class SplashScene :	public IScene
{
protected:
	ISceneObject *imageObject;
	ISceneObject *fadeObject;
	ICamera camera;
	ILight light;
	float sceneTime;
public:
	SplashScene(void);
	virtual ~SplashScene(void);
	virtual bool Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, LolEngine::CNetworkEngine* _networkEngine, CSettings* _pSettings);
	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void Dispose();
};

