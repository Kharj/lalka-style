#include "stdafx.h"
#include "Main.h"
#include "MenuEntrySceneObject.h"


LPD3DXFONT MenuEntrySceneObject::pFont = NULL; //<== static!!!
MenuEntrySceneObject::MenuEntrySceneObject(void)
{
	//pFont = NULL;
	mainBackMesh = NULL;
	hoverBackMesh = NULL;
	activeBackMesh = NULL;
	textMesh = NULL;
	index = 0;
	enable = true;
	onceCreated = false;
	fontHeight=32;
}


MenuEntrySceneObject::~MenuEntrySceneObject(void)
{
}

bool MenuEntrySceneObject::Create(int _index, float _width, float _height, D3DXCOLOR mainColor, D3DXCOLOR hoverColor, D3DXCOLOR activeColor, LPCSTR text, unsigned int _fontHeight, D3DXCOLOR textColor){
	if(!graphicsEngine) return false;
	HRESULT hr = 0;
	fontHeight = _fontHeight;
	this->Dispose();//clear
	index = _index;
	width = _width;
	height = _height;
	mainBackMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), width, height, "..\\Data\\MenuAlpha.png", mainColor);
	if(!mainBackMesh){
		Dispose();
		return false;
	}
	mainBackMesh->SetStateColor(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_MODULATE);
	mainBackMesh->SetPosition(D3DXVECTOR3(0.0f, 0.0f, -0.03f));
	hoverBackMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), width, height, "..\\Data\\MenuAlpha.png", hoverColor);
	if(!hoverBackMesh){
		Dispose();
		return false;
	}
	hoverBackMesh->SetStateColor(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_MODULATE);
	hoverBackMesh->SetPosition(D3DXVECTOR3(0.0f, 0.0f, -0.02f));
	activeBackMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), width, height, NULL, activeColor);
	if(!activeBackMesh){
		Dispose();
		return false;
	}
	activeBackMesh->SetPosition(D3DXVECTOR3(0.0f, 0.0f, -0.01f));

	onceCreated = true;
	if(!SetText(text, textColor)){
		Log::Write(std::string("can't SetText in menu") + text);
		Dispose();
		return false;
	}
	return true;
}
bool MenuEntrySceneObject::SetText(LPCSTR text, D3DXCOLOR textColor){
	if(!onceCreated) return false; //never created before - no needed meshes
	if(!graphicsEngine) return false;
	HRESULT hr = 0;
	//delete old text if exist
	if(textMesh){
		textMesh->Dispose();
		textMesh = NULL;
	}
	//clear vector of meshes
	meshes.clear();
	//new text mesh
	textMesh = CreatePlaneMesh(graphicsEngine->GetDevice(), width, height, NULL, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	if(!textMesh){
		//Dispose();
		return false;
	}
	textMesh->SetPosition(D3DXVECTOR3(0.0f,0.0f,0.0f));
	//Create text
	//get size in pixel
	unsigned int textHeight = fontHeight;
	unsigned int textHeightPowerOfTwo = 1;
	while(textHeightPowerOfTwo < textHeight){
		textHeightPowerOfTwo *= 2;
	}
	unsigned int textWidth = (unsigned int)((((float)textHeight*width*1.3f)/height)+0.5f);
	unsigned int textWidthPowerOfTwo = 1;
	while(textWidthPowerOfTwo < textWidth){
		textWidthPowerOfTwo *= 2;
	}
	LPDIRECT3DTEXTURE9 textTexture = 0;
	hr = D3DXCreateTexture(graphicsEngine->GetDevice(), textWidthPowerOfTwo, textHeightPowerOfTwo, 0, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &textTexture);
	if(FAILED(hr)){
		//Dispose();
		return false;
	}
	//set rendertarget
	IDirect3DSurface9* pSurface, *pOldTarget;
	hr = textTexture->GetSurfaceLevel(0, &pSurface);
	hr = graphicsEngine->GetDevice()->GetRenderTarget(0, &pOldTarget);
	if(!pSurface || !pOldTarget){
		textTexture->Release();
		//Dispose();
		return false;
	}
	hr = graphicsEngine->GetDevice()->SetRenderTarget(0, pSurface);
	//draw
	
	// init font
	if(pFont == NULL){
		D3DXCreateFont(graphicsEngine->GetDevice(), textHeight, 0, FW_NORMAL, 1, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Arial"), &pFont);
	}
	if(pFont == NULL){
		textTexture->Release();
		//Dispose();
		return false;
	}
	RECT textRect;
	textRect.left=0;
	textRect.right=textWidthPowerOfTwo*2;
	textRect.top=0;
	textRect.bottom=textHeightPowerOfTwo;
	//DRAW
	hr = graphicsEngine->GetDevice()->Clear( 0, NULL, D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET, 0x00000000, 1.0f, 0);
	hr = graphicsEngine->GetDevice()->BeginScene();
	if(!pFont->DrawTextA(NULL, text, -1, &textRect, DT_LEFT | DT_SINGLELINE | DT_VCENTER, textColor)){//nothing drawn
		textTexture->Release();
		//Dispose();
		return false;
	}
	hr = graphicsEngine->GetDevice()->EndScene();
	//return old rendertarget
	hr = graphicsEngine->GetDevice()->SetRenderTarget(0, pOldTarget);
	pSurface->Release();
	pOldTarget->Release();

	if(!textMesh->SetTexture(0, textTexture)){
		textTexture->Release();
		//Dispose();
		return false;
	}
	textMesh->SetStateColor(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_SELECTARG1);
	textMesh->SetStateAlpha(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_MODULATE);
	//reload vector
	
	AddMesh(mainBackMesh);
	AddMesh(hoverBackMesh);
	AddMesh(activeBackMesh);
	AddMesh(textMesh);
	Default();
	return true;
}
float MenuEntrySceneObject::GetWidth(){
	return width;
}
float MenuEntrySceneObject::GetHeight(){
	return height;
}
int MenuEntrySceneObject::GetIndex(){
	return index;
}

void MenuEntrySceneObject::Dispose(){
	//base dispose - dispose meshes
	ISceneObject::Dispose();
	if(pFont){
		pFont->Release();
		pFont = NULL;
	}
	mainBackMesh = NULL;
	hoverBackMesh = NULL;
	activeBackMesh = NULL;
	textMesh = NULL;
	onceCreated = false;
}

void MenuEntrySceneObject::Active(){
	if(enable){
		if(mainBackMesh) mainBackMesh->visible = false;
		if(hoverBackMesh) hoverBackMesh->visible = false;
		if(activeBackMesh) activeBackMesh->visible = true;
	}
}
void MenuEntrySceneObject::Hover(){
	if(enable){
		if(mainBackMesh) mainBackMesh->visible = false;
		if(hoverBackMesh) hoverBackMesh->visible = true;
		if(activeBackMesh) activeBackMesh->visible = false;
	}
}
void MenuEntrySceneObject::Default(){
	if(enable){
		if(mainBackMesh) mainBackMesh->visible = true;
		if(hoverBackMesh) hoverBackMesh->visible = false;
		if(activeBackMesh) activeBackMesh->visible = false;
	}
}