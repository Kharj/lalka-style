// LolEngine_Test.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "Main.h"


#define MAX_LOADSTRING 100
#define WS_MY (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX)

// ���������� ����������:
HINSTANCE hInst;								// ������� ���������
HWND hWnd;
TCHAR szTitle[MAX_LOADSTRING];					// ����� ������ ���������
TCHAR szWindowClass[MAX_LOADSTRING];			// ��� ������ �������� ����
CMyLolEngine engine;
LolEngine::CSettings settings;

// ��������� ���������� �������, ���������� � ���� ������ ����:
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: ���������� ��� �����.
	MSG msg;
	HACCEL hAccelTable;

	// ������������� ���������� �����
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_LOLENGINE_TEST, szWindowClass, MAX_LOADSTRING);
	//Wnd Class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LOLENGINE_TEST));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	if(!RegisterClassEx(&wcex)){
		return 1;
	}
	//Load Settings
	if(!settings.LoadFromFile()){
		settings.SaveToFile();//Create if not exist
		LolEngine::Log::Write("settings file not found, created default");
	}
	//Create copy of setting newSettings
	settings.newSettings = new CSettings(settings);
	// ��������� ������������� ����������:
	hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������
	RECT wndRect;
	wndRect.top=0;
	wndRect.bottom=settings.Height;
	wndRect.left=0;
	wndRect.right=settings.Width;
	AdjustWindowRect(&wndRect, WS_MY,NULL);
	//create wnd
	hWnd = CreateWindow(szWindowClass, szTitle, WS_MY, CW_USEDEFAULT, 0, wndRect.right, wndRect.bottom, NULL, NULL, hInstance, NULL);
	//create shit
	if (!hWnd)
	{
			LolEngine::Log::Write("Error create window! Shutdown");
			MessageBox(NULL,_T("Error create window"),_T(""), MB_OK|MB_ICONERROR);
			return FALSE;
	}
	//ShowCursor(FALSE); //hide cursor
		//Init engine
	if(!engine.Init(hWnd, &settings)){
		LolEngine::Log::Write("Error init LolEngine! Shutdown");
		MessageBox(NULL,_T("Error init engine. See log."),_T(""), MB_OK|MB_ICONERROR);
		SendMessage(hWnd, WM_CLOSE, 0, 0);
		//DestroyWindow(hWnd);
		return 0;
	}
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LOLENGINE_TEST));

	// ���� ��������� ���������:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	//PAINTSTRUCT ps;
	//HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_ERASEBKGND: 
		return 1;//��� �� ������� ����� �������������� ���
	case WM_PAINT:
		//hdc = BeginPaint(hWnd, &ps);
		//EndPaint(hWnd, &ps);
		//Update
		engine.Update();
		//Status read
		if(engine.GetStatus()==LolEngine::ENG_EXITAPP){
			SendMessage(hWnd, WM_CLOSE, 0, 0);//exit
		}
		//FPS counter work
		char fpsText[100];
		char fpsVal[50];
		_itoa_s(engine.GetFPS(), fpsVal, 10);
		
		strcpy_s(fpsText, "Title");
		strcat_s(fpsText, "  fps:");
		strcat_s(fpsText, fpsVal);
		strcat_s(fpsText, "  ping:");
		_itoa_s(engine.GetPing(), fpsVal, 10);
		strcat_s(fpsText, fpsVal);
		SetWindowTextA(hWnd, fpsText);
		//Draw
		engine.Draw();

		//InvalidateRect( hWnd, NULL,false );
		break;
	case WM_MOUSEMOVE:
		//engine.Update();
		break;
	case WM_CREATE:
		//create

		break;
	case WM_DESTROY:
		//Dispose
		engine.Dispose();
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
