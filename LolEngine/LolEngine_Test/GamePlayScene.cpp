#include "stdafx.h"
#include "Main.h"
#include "GamePlayScene.h"


GamePlayScene::GamePlayScene(void)
{
	tableLength = 8.72f;//length of play field
	tableWidth = 5.34f;//width of play field
	tableHeight = 4.414f;//height(position) of play field
	tableRad = 0.65f;//radius of table border
	malledWidth = 0.62f;//Diameter of mallet
	malletHeight = 0.43f;//Height of mallet
	puckWidth = malledWidth;//Diameter of puck
	puckHeight = 0.05f;//Height of puck
	malletMoveSpeed = 0.008f;//Multiplier for mallet movement
	cameraRotateSpeed = 0.006f;//Mul for camera rotation
	xinputMul = 0.022f;//speed multiplier for xinput controller
	gotPlayer = false;

	splitScreen = false;
	networkGame = false;
	physicsServer = false;
	tableObject = NULL;
	mallet1Object = NULL;
	mallet2Object = NULL;
	puckObject = NULL;
	environmentObject = NULL;
}

GamePlayScene::~GamePlayScene(void){
}

bool GamePlayScene::Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, CNetworkEngine* _networkEngine, CSettings* _pSettings){
	if(!IScene::Init(_graphicsEngine, _soundEngine, _inputEngine, _networkEngine, _pSettings)){//inil from base class
		return false;
	}
	//Environment
	environmentObject = new ISceneObject();
	if(!environmentObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
		environmentObject->Dispose();
		delete environmentObject;
		return false;
	}
	IMesh * skyboxMesh = CreateSkyBoxMesh(graphicsEngine->GetDevice(), D3DXVECTOR3(100.0f, 100.0f, 100.0f), "..\\Data\\top.png", "..\\Data\\left.png","..\\Data\\front.png","..\\Data\\right.png","..\\Data\\back.png","..\\Data\\bottom.png");
	if(skyboxMesh){
		environmentObject->AddMesh(skyboxMesh);
	}
	//Objects

	//Table
	tableObject = new ISceneObject();
	if(!tableObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
		tableObject->Dispose();
		delete tableObject;
		return false;
	}
	IMesh * tmpMesh = new TableMesh();
	if(!tmpMesh->LoadFromX("..\\Data\\table.X", graphicsEngine->GetDevice())){//noloaded model
		return false;
	}
	tableObject->AddMesh(tmpMesh);

	//Mallet 1
	mallet1Object = new ISceneObject();
	if(!mallet1Object->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
		mallet1Object->Dispose();
		delete mallet1Object;
		return false;
	}
	tmpMesh = new IMesh();
	if(!tmpMesh->LoadFromX("..\\Data\\mallet.X", graphicsEngine->GetDevice())){//noloaded model
		return false;
	}
	//Red
	tmpMesh->GetMaterial(0)->Diffuse = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);
	mallet1Object->AddMesh(tmpMesh);

	//Mallet 2
	mallet2Object = new ISceneObject();
	if(!mallet2Object->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
		mallet2Object->Dispose();
		delete mallet2Object;
		return false;
	}
	tmpMesh = new IMesh();
	if(!tmpMesh->LoadFromX("..\\Data\\mallet.X", graphicsEngine->GetDevice())){//noloaded model
		return false;
	}
	//Blue
	tmpMesh->GetMaterial(0)->Diffuse = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);
	mallet2Object->AddMesh(tmpMesh);

	//Puck
	puckObject = new ISceneObject();
	if(!puckObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
		puckObject->Dispose();
		delete puckObject;
		return false;
	}
	tmpMesh = new IMesh();
	if(!tmpMesh->LoadFromX("..\\Data\\puck.X", graphicsEngine->GetDevice())){//noloaded model
		return false;
	}
	puckObject->AddMesh(tmpMesh);



	//structs
	player1.Reset();
	player1.Num = 0;
	player1.PositionX = -1.0f;
	player1.PositionY = 0.0f;

	player2.Reset();
	player2.Num = 1;
	player1.PositionX = 1.0f;
	player1.PositionY = 0.0f;

	playerPuck.Reset();
	playerPuck.Num = -1;

	//Objects positions
	tableObject->SetPosition(D3DXVECTOR3(playerPuck.PositionX, 0.0f, playerPuck.PositionY));
	mallet1Object->SetPosition(D3DXVECTOR3(player1.PositionX, tableHeight - malletHeight*0.5f, player1.PositionY));
	mallet2Object->SetPosition(D3DXVECTOR3(player2.PositionX, tableHeight - malletHeight*0.5f, player2.PositionY));
	puckObject->SetPosition(D3DXVECTOR3(1.0f, tableHeight - puckHeight*0.5f, 0.0f));
	//Cameras
	float cameraAspectRatio = settings->GetAspectRatio();
	if(splitScreen){
		if(settings->SplitscreenVertical)
			cameraAspectRatio *= 2.0f; 
		else
			cameraAspectRatio *= 0.5f;
	}
	//reset cameras
	camera1.Reset();
	camera2.Reset();

	//player1
	camera1.SetEyePosition(D3DXVECTOR3(0.0f,9.0f,8.0f));
	camera1.SetLookAt(D3DXVECTOR3(0.0f, 0.0f, -4.0f));
	camera1.SetProjection(D3DXToRadian(45), cameraAspectRatio, 1.0f, 1000.0f);
	//player 2
	camera2.SetEyePosition(D3DXVECTOR3(0.0f,9.0f,-8.0f));
	camera2.SetLookAt(D3DXVECTOR3(0.0f, 0.0f, 4.0f));
	camera2.SetProjection(D3DXToRadian(45), cameraAspectRatio, 1.0f, 1000.0f);
	//splitscreen
	if(!settings->SplitscreenVertical && splitScreen){
		camera1.SetEyePosition(D3DXVECTOR3(0.0f,10.0f,9.0f));
		camera2.SetEyePosition(D3DXVECTOR3(0.0f,10.0f,-9.0f));
	}
	//Popup
	popupControls = new PopupSceneObject();
	if(!popupControls->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	popupControls->Create(8.0f* 0.1f, 0.1f, "..\\Data\\GameplayPopupControls.png", 0.5f, 0.8f, 0.5f);
	popupControls->SetPosition(D3DXVECTOR3(0.0f, camera1.GetEyePosition().y - 0.8f, camera1.GetEyePosition().z - 0.8f));
	popupControls->SetRotation(D3DXVECTOR3(-0.8f, 0.0f, 0.0f));

	if(!splitScreen) 	popupControls->Show(true);
	//Popup
	popupWaitServer = new PopupSceneObject();
	if(!popupWaitServer->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	popupWaitServer->Create(8.0f* 0.1f, 0.1f, "..\\Data\\GameplayPopupWaitServer.png");//, 0.5f, 1.0f, 0.5f);
	popupWaitServer->SetPosition(D3DXVECTOR3(0.0f, camera1.GetEyePosition().y - 1.0f, camera1.GetEyePosition().z - 0.7f));
	popupWaitServer->SetRotation(D3DXVECTOR3(-0.8f, 0.0f, 0.0f));
	if(networkGame) 	popupWaitServer->Show(false);

	//Popup
	popupWaitPlayer = new PopupSceneObject();
	if(!popupWaitPlayer->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)) return false;
	popupWaitPlayer->Create(8.0f* 0.1f, 0.1f, "..\\Data\\GameplayPopupWaitPlayer.png");//, 0.2f, 1.0f, 0.5f);
	popupWaitPlayer->SetPosition(D3DXVECTOR3(0.0f, camera1.GetEyePosition().y - 1.0f, camera1.GetEyePosition().z - 0.7f));
	popupWaitPlayer->SetRotation(D3DXVECTOR3(-0.8f, 0.0f, 0.0f));
	//light
	light = ILight(D3DXVECTOR3(5.0f,10.0f,0.0f), D3DXVECTOR3(-0.5f,-1.0f,0.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	//light = ILight(D3DLIGHT_POINT, D3DXVECTOR3(2.0f,10.0f,0.0f), D3DXVECTOR3(-0.25f,-1.0f,0.0f));



	//Network
	if(networkGame){
		gotPlayer = false;
		gotConnection = false;
	}
	//Physics
	if(physicsServer){
		physics.Reset();
		physics.SetMallet1Position(player1.PositionX, player1.PositionY);
		physics.SetMallet1Position(player2.PositionX, player2.PositionY);
		physics.SetPuckPosition(playerPuck.PositionX, playerPuck.PositionY);
	}

	status = ENG_OK;
	return true;
}
void GamePlayScene::Update(float deltaTime){
	if(status == ENG_NOTINIT) return; //No update till scenenot inited
	IScene::Update(deltaTime);//Update base class

	//Physics
	//maybe need to move this IF after read devices
	if(physicsServer){
		physics.Update(deltaTime);
		physics.SetMallet1Position(player1.PositionX, player1.PositionY);
		physics.SetMallet2Position(player2.PositionX, player2.PositionY);
		physics.GetPuckPosition(playerPuck.PositionX, playerPuck.PositionY);
	}

	//Normal update
	popupControls->Update(deltaTime);
	popupWaitServer->Update(deltaTime);
	popupWaitPlayer->Update(deltaTime);

	if(inputEngine->IsKeyPressed(DIK_ESCAPE) || inputEngine->IsXInputButtonPressed(0, XINPUT_GAMEPAD_START)){
		networkEngine->Disconnect();
		status = ENG_TOMENU;
		return;
	}

	//Network
	//Send - then - Recv
	if(networkGame){
		//connect broken
		if(networkEngine->GetStatus() == ENG_NOTCONNECTED){
			Log::Write("Connection was closed.");
			networkEngine->Disconnect();
			status = ENG_TOMENU;
			return;
		}
		//waiting server
		if(networkEngine->GetStatus() == ENG_CONNECTWAIT){
			return;
		}
		//connected ok
		if(networkEngine->GetStatus() == ENG_CONNECTED){
			if(!gotPlayer){
				if(!gotConnection){//first time
					//show next popup
					popupWaitServer->Hide();
					popupWaitPlayer->Show(false);
					//Send my info		
					networkEngine->Send("Connect");
					gotConnection = true;
				}else{//not first
					std::string recvString("");
					if(networkEngine->Read(recvString)){
						//Got answer for hello
						Log::Write(recvString);
						//Read my number
						if(recvString == "Start0" || recvString == "Start1"){//start game
							popupWaitPlayer->Hide();
							gotPlayer = true;
							if(recvString == "Start0"){//  am 0 - i run physics
								physicsServer = true;
								//reset physics
								physics.Reset();
								physics.SetMallet1Position(player1.PositionX, player1.PositionY);
								physics.SetMallet1Position(player2.PositionX, player2.PositionY);
								physics.SetPuckPosition(playerPuck.PositionX, playerPuck.PositionY);
							}else{//�� ������ �������� ������
								physicsServer = false;
							}
						}else if(recvString == "Wait"){//wait for 1 player
							//nope
							//send next request
							networkEngine->Send("Connect");
						}
					}
				}

				return; //no normal update

			}else{//Got player
				//Normal dialog
				std::string toSend("");
				toSend += player1.ToString();
				toSend += "\n";
				toSend += player2.ToString();
				toSend += "\n";
				toSend += playerPuck.ToString();
				//Send all i know
				//enter saparated
				networkEngine->Send(toSend);
				//read
				std::string readStr("");
				if(networkEngine->Read(readStr)){//got something
					if(readStr == "" || readStr == "Wait"){//no useful info
						//nope
					}else{//got useful info
						//parse
						std::istringstream ss(readStr);
						char line[1024];
						while(ss.getline(line, 1024)){
							PlayerDataStruct tmpStruct;
							tmpStruct.ReadString(line);
							tmpStruct.PositionY = -tmpStruct.PositionY;
							tmpStruct.PositionX = -tmpStruct.PositionX;
							if(physicsServer){//read only second mallet data
								if(tmpStruct.Num == 0){
									tmpStruct.Num = 1;
									player2 = tmpStruct;
								}
							}else{//read all except my mallet
								if(tmpStruct.Num == 0){
									tmpStruct.Num = 1;
									player2 = tmpStruct;
								}
								if(tmpStruct.Num == -1)
									playerPuck = tmpStruct;
							}
						}


					}
				}
			}
		}
	}

	//MOUSE
	//static int lastMouseX = 0, lastMouseY = 0;
	int deltaMouseX = 0, deltaMouseY = 0;
	inputEngine->GetMouseLocation(deltaMouseX, deltaMouseY);

	//inputEngine->XInputVibrate(0, xinput1.Gamepad.bLeftTrigger*256, xinput1.Gamepad.bRightTrigger*256);
	//deltaMouseX += lastMouseX;
	//deltaMouseY += lastMouseY;
	//inputEngine->GetMouseLocation(lastMouseX, lastMouseY);

	//XINPUT
	float xinputLX_0 = 0.0f, xinputLY_0 = 0.0f, xinputRX_0 = 0.0f, xinputRY_0 = 0.0f;
	float xinputLX_1 = 0.0f, xinputLY_1 = 0.0f, xinputRX_1 = 0.0f, xinputRY_1 = 0.0f;
	XINPUT_STATE xinputState0, xinputState1;
	ZeroMemory(&xinputState0, sizeof(xinputState0));
	ZeroMemory(&xinputState1, sizeof(xinputState1));

	bool xinputConnected0=inputEngine->IsXInputDeviceConnected(0), xinputConnected1=inputEngine->IsXInputDeviceConnected(1);

	if(splitScreen && !networkGame){
		//smart gamepad select
		if(xinputConnected0 && !xinputConnected1){ //only 1
			xinputState1 = inputEngine->GetXInputState(0);
		}else if(!xinputConnected0 && xinputConnected1){ //only 2
			xinputState1 = inputEngine->GetXInputState(1);
		}else if(xinputConnected0 && xinputConnected1){//both
			xinputState0 = inputEngine->GetXInputState(0);
			xinputState1 = inputEngine->GetXInputState(1);
		}
	}else{//network
		//simple one player - one gamepad
		xinputState0 = inputEngine->GetXInputState(0);
	}
	//if(xinputConnected0 && xinputConnected1){
	if(xinputState0.Gamepad.sThumbLX > CInputEngine::xinput_dead_zone || xinputState0.Gamepad.sThumbLX < -CInputEngine::xinput_dead_zone)
		xinputLX_0 = (float)xinputState0.Gamepad.sThumbLX*xinputMul;
	if(xinputState0.Gamepad.sThumbLY > CInputEngine::xinput_dead_zone || xinputState0.Gamepad.sThumbLY < -CInputEngine::xinput_dead_zone)
		xinputLY_0 = (float)xinputState0.Gamepad.sThumbLY*xinputMul;
	if(xinputState0.Gamepad.sThumbRX > CInputEngine::xinput_dead_zone || xinputState0.Gamepad.sThumbRX < -CInputEngine::xinput_dead_zone)
		xinputRX_0 = (float)xinputState0.Gamepad.sThumbRX*xinputMul;
	if(xinputState0.Gamepad.sThumbRY > CInputEngine::xinput_dead_zone || xinputState0.Gamepad.sThumbRY < -CInputEngine::xinput_dead_zone)
		xinputRY_0 = (float)xinputState0.Gamepad.sThumbRY*xinputMul;
	//}
	//if((xinputConnected0 || xinputConnected1)){
	if(xinputState1.Gamepad.sThumbLX > CInputEngine::xinput_dead_zone || xinputState1.Gamepad.sThumbLX < -CInputEngine::xinput_dead_zone)
		xinputLX_1 = (float)xinputState1.Gamepad.sThumbLX*xinputMul;
	if(xinputState1.Gamepad.sThumbLY > CInputEngine::xinput_dead_zone || xinputState1.Gamepad.sThumbLY < -CInputEngine::xinput_dead_zone)
		xinputLY_1 = (float)xinputState1.Gamepad.sThumbLY*xinputMul;
	if(xinputState1.Gamepad.sThumbRX > CInputEngine::xinput_dead_zone || xinputState1.Gamepad.sThumbRX < -CInputEngine::xinput_dead_zone)
		xinputRX_1 = (float)xinputState1.Gamepad.sThumbRX*xinputMul;
	if(xinputState1.Gamepad.sThumbRY > CInputEngine::xinput_dead_zone || xinputState1.Gamepad.sThumbRY < -CInputEngine::xinput_dead_zone)
		xinputRY_1 = (float)xinputState1.Gamepad.sThumbRY*xinputMul;
	//}



	D3DXVECTOR3 mallet1Position =  mallet1Object->GetPosition();
	D3DXVECTOR3 mallet2Position =  mallet2Object->GetPosition();
	D3DXVECTOR3 puckPosition =  puckObject->GetPosition();
	D3DXVECTOR3 camera1LookAt = camera1.GetLookAt();
	D3DXVECTOR3 camera2LookAt = camera2.GetLookAt();

	//Gamepad:
	//Mallet 1
	player1.PositionX -= settings->MouseScale  * malletMoveSpeed * (deltaTime * xinputLX_0);
	player1.PositionY -= settings->MouseScale  * malletMoveSpeed * (deltaTime * xinputLY_0);
	//Mallet 2
	player2.PositionX += settings->MouseScale  * malletMoveSpeed * (deltaTime * xinputLX_1);
	player2.PositionY += settings->MouseScale  * malletMoveSpeed * (deltaTime * xinputLY_1);
	//Camera 1
	camera1LookAt.x -= settings->MouseScale  * cameraRotateSpeed * (deltaTime * xinputRX_0);
	camera1LookAt.y += settings->MouseScale  * cameraRotateSpeed * (deltaTime * xinputRY_0);
	//Camera 2
	camera2LookAt.x += settings->MouseScale  * cameraRotateSpeed * (deltaTime * xinputRX_1);
	camera2LookAt.y += settings->MouseScale  * cameraRotateSpeed * (deltaTime * xinputRY_1);

	//Mouse:
	if(inputEngine->IsMouseButtonPressed(1)){
		//Camera 1
		camera1LookAt.x -= settings->MouseScale  * cameraRotateSpeed * ((float)deltaMouseX);
		camera1LookAt.y -= settings->MouseScale  * cameraRotateSpeed * ((float)deltaMouseY);
	}else{
		//Mallet 2
		player1.PositionX -= settings->MouseScale  * malletMoveSpeed * ((float)deltaMouseX);
		player1.PositionY -= settings->MouseScale  * malletMoveSpeed * (-(float)deltaMouseY);

	}
	//Borders mallet1
	BorderBound(player1.PositionX, player1.PositionY, malledWidth, 0);
	//Borders mallet 2
	BorderBound(player2.PositionX, player2.PositionY, malledWidth, 1);
	//Borders puck
	BorderBound(playerPuck.PositionX, playerPuck.PositionY, puckWidth, -1);


	//Write positions
	mallet1Position.x = player1.PositionX;
	mallet1Position.z = player1.PositionY;

	mallet2Position.x = player2.PositionX;
	mallet2Position.z = player2.PositionY;

	puckPosition.x = playerPuck.PositionX;
	puckPosition.z = playerPuck.PositionY;

	//Set vectors
	mallet1Object->SetPosition(mallet1Position);
	mallet2Object->SetPosition(mallet2Position);
	puckObject->SetPosition(puckPosition);
	camera1.SetLookAt(camera1LookAt);
	camera2.SetLookAt(camera2LookAt);



}
void GamePlayScene::Draw(){
	lightCounter = 0;//to proper set lights
	//IScene::Draw();//Draw base class NO!
	//Set shit
	LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();

	//Light
	d3ddev->SetRenderState(D3DRS_LIGHTING, false);    // turn off the 3D lighting

	d3ddev -> SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); // ��������� ��������� Direct3D
	SetLight(&light);	
#pragma region SetStates
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	//texture
	//filter
	d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); 
	d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, settings->TextureMinFilter); 
	d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, settings->TextureMaxAnisotropy);
#pragma endregion
	//Begin
	graphicsEngine->BeginDraw();
	if(splitScreen){
		D3DVIEWPORT9 oldViewport;
		d3ddev->GetViewport(&oldViewport);
		D3DVIEWPORT9 newViewport1 = oldViewport;
		D3DVIEWPORT9 newViewport2 = oldViewport;
		if(settings->SplitscreenVertical){//by Y
			newViewport1.Y = 0;
			newViewport1.Height /= 2;
			newViewport2.Y = newViewport2.Height/2;
			newViewport2.Height /= 2;
		}else{//by X
			newViewport1.X = 0;
			newViewport1.Width /= 2;
			newViewport2.X = newViewport2.Width/2;
			newViewport2.Width /= 2;
		}
		//1
		d3ddev->SetViewport(&newViewport1);
		SetCamera(&camera1);
		DrawObjects();
		//2
		d3ddev->SetViewport(&newViewport2);
		SetCamera(&camera2);
		DrawObjects();
		d3ddev->SetViewport(&oldViewport);
	}else{
		SetCamera(&camera1);
		DrawObjects();
	}

	//End
	graphicsEngine->EndDraw();
}
void GamePlayScene::DrawObjects(){
	//Objects
	environmentObject->Draw();

	tableObject->Draw();
	mallet1Object->Draw();
	mallet2Object->Draw();
	puckObject->Draw();
	popupControls->Draw();
	popupWaitServer->Draw();
	popupWaitPlayer->Draw();
}
void GamePlayScene::Dispose(){
	IScene::Dispose();//dispose base class
	this->splitScreen = false;
	this->networkGame = false;
	this->physicsServer = false;
	//Objects
	//table
	tableObject->Dispose();
	delete tableObject;
	tableObject = NULL;
	//mallet1
	mallet1Object->Dispose();
	delete mallet1Object;
	mallet1Object = NULL;
	//mallet2
	mallet2Object->Dispose();
	delete mallet2Object;
	mallet2Object = NULL;
	//puck
	puckObject->Dispose();
	delete puckObject;
	puckObject = NULL;
	//environment
	environmentObject->Dispose();
	delete environmentObject;
	environmentObject = NULL;
	//popup
	popupControls->Dispose();
	delete popupControls;
	popupControls = NULL;
	//popup
	popupWaitServer->Dispose();
	delete popupWaitServer;
	popupWaitServer = NULL;
	//popup
	popupWaitPlayer->Dispose();
	delete popupWaitPlayer;
	popupWaitPlayer = NULL;
}
bool GamePlayScene::BorderBound(float &x, float &y, float diam, int playerNum){//0-player1, 1-player2, -1 - puck
	bool res = false;
	//X
	if(x < -(0.5f*tableWidth - diam)){
		x = -(0.5f*tableWidth - diam);
		res = true;
	}
	if(x > (0.5f*tableWidth - diam)){
		x = (0.5f*tableWidth - diam);
		res = true;
	}
	//Y
	if(y < ( (playerNum==0)?(0.0f + diam*0.5f):(-(0.5f*tableLength - diam)) )){
		y = ( (playerNum==0)?(0.0f + diam*0.5f):(-(0.5f*tableLength - diam)) );
		res = true;
	}
	if(y > ( (playerNum==1)?(0.0f - diam*0.5f):((0.5f*tableLength - diam)) )){
		y = ( (playerNum==1)?(0.0f - diam*0.5f):((0.5f*tableLength - diam)) );
		res = true;
	}
	//Diagonal shit border
	//if(x > (0.5f*tableWidth - diam - tableRad) && y > (0.5f*tableLength - diam - tableRad)){// + +
	//we re in rect
	/*
	//shit
	float deltaX = x - (0.5f*tableWidth - diam - tableRad);
	float deltaY = y - (0.5f*tableLength - diam - tableRad);
	if(deltaX + deltaY > tableRad){
	float midDelta = (deltaX + deltaY)/2.0f;
	x -= (tableRad*0.5 - midDelta);
	y -= (tableRad*0.5 - midDelta);
	}
	res = true;
	*/
	//}




	/*
	//Radius
	// x^2 + y^2 = R^2
	// x^2 = R^2 - y^2
	// y^2 = R^2 - x^2

	//Near to corners
	//float deltaX = (x > 0.1f) ? (0.5f*tableWidth - diam - x) : (0.5f*tableWidth - diam - x)
	if(x < -(0.5f*tableWidth - tableRad - diam)){// -x
	float deltaX = -x - (0.5f*tableWidth - diam);
	if(y < -(0.5f*tableLength - tableRad - diam)){// -x -y
	float deltaY = -y - (0.5f*tableLength - diam);
	float borderX = sqrt(tableRad*tableRad - deltaY*deltaY);
	float borderY = sqrt(tableRad*tableRad - deltaX*deltaX);
	if(deltaX < borderX) x = -borderX - (0.5f*tableWidth - diam);
	if(deltaY < borderY) y = - borderY - (0.5f*tableLength - diam);
	}else if(y > (0.5f*tableLength - diam)){// -x +y
	float deltaY = y - (0.5f*tableLength - diam);

	}
	}else if(x > (0.5f*tableWidth - tableRad - diam)){// +x
	float deltaX = x - (0.5f*tableWidth - diam);
	if(y < -(0.5f*tableLength - tableRad - diam)){// +x -y
	float deltaY = - y - (0.5f*tableLength - diam);

	}else if(y > (0.5f*tableLength - tableRad - diam)){// +x +y
	float deltaY = y - (0.5f*tableLength - diam);

	}

	}*/
	return res;
}