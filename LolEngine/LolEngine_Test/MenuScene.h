#pragma once
class MenuScene : public IScene
{
protected:

	ISceneObject *backgroundObject;
	PopupSceneObject *popupControls;
	PopupSceneObject *popupSettings;
	PopupSceneObject *popupConnect;
	ICamera camera;
	ILight light;
	float sceneTime;
	float menuZ;
	float entryDistance;
	float entryHeight;
	float entryWidth;
	float pressTimer;
	float defaultPressTime;
	unsigned int fontHeight;//font height for all entries
	int pagesNum;
	int currentPage;
	int currentEntry;
	MenuPage * pages;
	std::vector<ISceneObject *> objects;
	std::string ipAddr;

	//methods
	void SelectPage(int num);
	void AddObject(ISceneObject * a);
	void SetSettingsPageText();
public:
	MenuScene(void);
	virtual ~MenuScene(void);
	virtual bool Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, LolEngine::CNetworkEngine* _networkEngine, CSettings* _pSettings);
	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void Dispose();
};

