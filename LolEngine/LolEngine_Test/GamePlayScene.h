#pragma once

class GamePlayScene :	public IScene
{
protected:

	ISceneObject *tableObject;
	ISceneObject *mallet1Object;
	ISceneObject *mallet2Object;
	ISceneObject *puckObject;
	ISceneObject *environmentObject;
	PopupSceneObject *popupControls;
	PopupSceneObject *popupWaitServer;
	PopupSceneObject *popupWaitPlayer;

	ICamera camera1;
	ICamera camera2;
	ILight light;

	//consts 
	float tableWidth;
	float tableLength;
	float tableHeight;
	float tableRad;
	float malledWidth;
	float malletHeight;
	float puckWidth;
	float puckHeight;
	float malletMoveSpeed;
	float cameraRotateSpeed;
	float xinputMul;

	//players
	PlayerDataStruct player1, player2, playerPuck;

	Physics2D physics;

	bool gotPlayer, gotConnection;

	//Methods
	bool BorderBound(float &x, float &y, float diam, int playerNum);//object it table bounds, return true if collide. 0-player1, 1-player2, -1 - puck
public:
	GamePlayScene(void);
	virtual ~GamePlayScene(void);
	virtual bool Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, LolEngine::CNetworkEngine* _networkEngine, CSettings* _pSettings);
	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void Dispose();
	void DrawObjects();

	bool splitScreen;//tthis game in splitscreen mode
	bool networkGame;//this game is over network
	bool physicsServer;//this client calculates physics
};

