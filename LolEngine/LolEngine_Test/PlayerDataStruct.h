#pragma once
class PlayerDataStruct
{
public:
	PlayerDataStruct(void);
	~PlayerDataStruct(void);
	float PositionX, PositionY;
	int Score;
	int Num;
	std::string ToString();// Serialize "Player 0 Score 0 PositionX 0.0 PositionY 0.0 End"
	bool ReadString(std::string str);//Deserialize "Player 0 Score 0 PositionX 0.0 PositionY 0.0 End"
	void Reset();
};

