#pragma once
class Physics2D
{
protected:
	//const
	float tableWidth;
	float tableLength;
	float malledWidth;
	float puckWidth;
	//var
	float fullTime;
	//b2Vec2 mallet1Position, mallet2Position, puckPosition;
	b2World world;
	b2Body *tabelBodies[4], *mallet1Body, *mallet2Body, *puckBody;
public:
	Physics2D(void);
	~Physics2D(void);
	void SetMallet1Position(float x, float y);
	void SetMallet2Position(float x, float y);
	void GetPuckPosition(float &x, float &y);
	void SetPuckPosition(float x, float y);
	void Reset();
	void Update(float deltaTime);


};
/*
tableLength = 8.72f;//length of play field
	tableWidth = 5.34f;//width of play field
	tableHeight = 4.414f;//height(position) of play field
	tableRad = 0.65f;//radius of table border
	malledWidth = 0.62f;//Diameter of mallet
	malletHeight = 0.43f;//Height of mallet
	puckWidth = malledWidth;//Diameter of puck
	puckHeight = 0.05f;//Height of puck
	malletMoveSpeed = 0.008f;//Multiplier for mallet movement
	cameraRotateSpeed = 0.006f;//Mul for camera rotation
	xinputMul = 0.022f;//speed multiplier for xinput controller
*/

