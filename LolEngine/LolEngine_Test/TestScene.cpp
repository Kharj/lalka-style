#include "stdafx.h"
#include "Main.h"
#include "TestScene.h"

	TestScene::TestScene(void)
	{
	}


	TestScene::~TestScene(void)
	{
	}
	bool TestScene::Init(CGraphicsEngine* _graphicsEngine, CSoundEngine* _soundEngine, CInputEngine* _inputEngine, CNetworkEngine* _networkEngine, CSettings* _pSettings){
		if(!IScene::Init(_graphicsEngine, _soundEngine, _inputEngine, _networkEngine, _pSettings)){//inil from base class
			return false;
		}
		//Environment
		environmentObject = new ISceneObject();
		if(!environmentObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
			environmentObject->Dispose();
			delete environmentObject;
			return false;
		}
		IMesh * skyboxMesh = CreateSkyBoxMesh(graphicsEngine->GetDevice(), D3DXVECTOR3(100.0f, 100.0f, 100.0f), "..\\Data\\top.bmp", "..\\Data\\left.bmp","..\\Data\\front.bmp","..\\Data\\right.bmp","..\\Data\\back.bmp","..\\Data\\bottom.bmp");
		if(skyboxMesh){
			environmentObject->AddMesh(skyboxMesh);
		}
		//Objects
		testObject = new ISceneObject();
		if(!testObject->Init(graphicsEngine, soundEngine, inputEngine, networkEngine)){
			testObject->Dispose();
			delete testObject;
			return false;
		}
		IMesh * tmpMesh = new IMesh();
		tmpMesh->SetName("lalka");
		if(!tmpMesh->LoadFromX("..\\Data\\full_test.X", graphicsEngine->GetDevice())){//noloaded model
			return false;
		}
		//tmpMesh->SetRotation(D3DXVECTOR3(D3DXToRadian(-90.0f),0.0f,0.0f));
		tmpMesh->SetScale(D3DXVECTOR3(10.0f,10.0f,10.0f));
		testObject->AddMesh(tmpMesh);
		//Camera
		//camera = new ICamera();
		camera.SetEyePosition(D3DXVECTOR3(0.0f,5.0f,5.0f));
		camera.SetLookAt(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		camera.SetProjection(D3DXToRadian(45), 4.0f/ 3.0f, 1.0f, 1000.0f);
		//light
		light = ILight(D3DXVECTOR3(5.0f,10.0f,0.0f), D3DXVECTOR3(-0.5f,-1.0f,0.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		status = ENG_OK;
		return true;
	}
	void TestScene::Update(float deltaTime){
		if(status == ENG_NOTINIT) return; //No update till scenenot inited
		IScene::Update(deltaTime);//Update base class
		//Update Input
		inputEngine->Update();
		if(inputEngine->IsKeyPressed(DIK_ESCAPE)){
			status = ENG_EXITAPP;
		}
		//Update environment
		environmentObject->Update(deltaTime);
		//D3DXVECTOR3 tmpVector = camera.GetRotation();
		//tmpVector+=D3DXVECTOR3(0.0f, deltaTime * 0.2f,0.0f);
		//camera.SetRotation(tmpVector);
		//Update objects
		testObject->Update(deltaTime);
		D3DXVECTOR3 tmpVector1 = testObject->GetRotation();
		tmpVector1+=D3DXVECTOR3(0.0f, deltaTime * 0.4f,0.0f);
		testObject->SetRotation(tmpVector1);

	}
	void TestScene::Draw(){
		lightCounter = 0;//to proper set lights
		//IScene::Draw();//Draw base class NO!
		//Set shit
		LPDIRECT3DDEVICE9 d3ddev = graphicsEngine->GetDevice();

		//Light
		d3ddev->SetRenderState(D3DRS_LIGHTING, false);    // turn off the 3D lighting
		 
		d3ddev -> SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); // ��������� ��������� Direct3D
		SetLight(&light);	
#pragma region SetStates
		d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer
		d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
		d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		//texture
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1,D3DTA_TEXTURE);
		d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);
		d3ddev->SetTextureStageState(0, D3DTSS_COLOROP,  D3DTOP_SELECTARG1);
		//alpha
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
		d3ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );
		d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP,  D3DTOP_SELECTARG1);
		//filter
		d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); 
		d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); 
		d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, settings->TextureMinFilter); 
		d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, settings->TextureMaxAnisotropy);
#pragma endregion
		SetCamera(&camera);
		//Begin
		graphicsEngine->BeginDraw();
		//Objects
		testObject->Draw();
		environmentObject->Draw();
		//End
		graphicsEngine->EndDraw();
	}
	void TestScene::Dispose(){
		IScene::Dispose();//dispose base class
		//Objects
		testObject->Dispose();
		delete testObject;
		testObject = NULL;
		//Camera
	}
