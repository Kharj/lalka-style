#include "stdafx.h"
#include "initDirect.h"

//��������� 
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int iCmdShow); //����� ������ ����������
long WINAPI WndProc(HWND hWnd,UINT iMsg,WPARAM wParam,LPARAM lParam);//���������� ���������
bool InitDirect3D(D3DFORMAT ColorFormat,D3DFORMAT DepthFormat);    //������������� Direct3D
void DrawFrame();                          //������ ����
void Shutdown();                          //����������� ������



int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int iCmdShow)
{  
  g_hInstance = GetModuleHandle(NULL);

  WNDCLASSEX wc;
  wc.cbSize      = sizeof(WNDCLASSEX);        //������ ���������
  wc.style           = CS_HREDRAW|CS_VREDRAW;      //����� ������ ����
  wc.lpfnWndProc    = WndProc;              //������� ��������� ���������
  wc.cbClsExtra    = 0;                //���������� ���������� ������ ��� �������� ����������
  wc.cbWndExtra      = 0;                //���������� ���������� ������ ��� �������� ����������
  wc.hInstance    = g_hInstance;            //���������� ����������
  wc.hIcon           = LoadIcon(NULL,IDI_APPLICATION);  //��������� ����������� ������
  wc.hCursor         = LoadCursor(0,IDC_ARROW);      //��������� ����������� ������
  wc.hbrBackground   = (HBRUSH)GetStockObject(WHITE_BRUSH);//���� ����� ��������� � ����� ����
  wc.lpszMenuName    = 0;                //�� ���������� ����
  wc.lpszClassName   = L"Directx";            //�������� ������
  wc.hIconSm       = LoadIcon(NULL,IDI_APPLICATION);  //��������� ����������� ������

  if(!RegisterClassEx(&wc))                //������������ ����� � Windows
  {
    Shutdown();                    //����������� ������
    MessageBox(NULL,L"Can`t register window class",L"Error",MB_OK|MB_ICONERROR); //������� ���������
    return 0;                    //��������� ������ ����������
  }

  g_hWnd = CreateWindowEx(              //������� ����
    WS_EX_APPWINDOW|WS_EX_WINDOWEDGE,        //����������� ����� ����
    L"Directx",                    //�������� ������ ����
    L"��������",    //�������� ����
    WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,//����� ����
    0,                      //������� ���� �� ��� �
    0,                      //������� ���� �� ��� �
    g_iWindowWidth,                //������ ����
    g_iWindowHeight,              //������ ����
    NULL,                    //��� ���� ������� ����
    NULL,                    //���� ����
    g_hInstance,                //���������� ����������
    NULL);                    //�������������� �������� �� ����������

  if(g_hWnd == NULL)                //���� �� ������� ����
  {
    Shutdown();
    MessageBox(NULL,L"Can`t create window",L"Error",MB_OK|MB_ICONERROR);//������� ���������
    return 0;                  //��������� ������ ����������
  }

  if(!InitDirect3D(D3DFMT_R5G6B5,D3DFMT_D16))    //���� �� ������ ���������������� Direct3D
  {
    Shutdown();
    MessageBox(NULL,L"Can`t create direct3d",L"Error",MB_OK|MB_ICONERROR);//������� ���������
    return 0;                  //��������� ������ ����������
  }

  ShowWindow(g_hWnd,SW_SHOW);            //���������� ����  
  UpdateWindow(g_hWnd);              //��������� ����
  SetFocus(g_hWnd);                //������������� ����� �� ���� ����
  SetForegroundWindow(g_hWnd);          //������������� ��������� ���� ���� ��������

  MSG msg;
  ZeroMemory(&msg,sizeof(msg));

  while(g_bApplicationState)            //�������� ����������� ���� ��������� ���������
  {
    if(PeekMessage(&msg,NULL,NULL,NULL,PM_REMOVE))//�������� ���������
    {
      TranslateMessage(&msg);          //������������ ���������
      DispatchMessage(&msg);          //������������ ���������
    }
    else
      DrawFrame();              //���� ��������� ���� ������ �����
  }

  Shutdown();                    //����������� ������
  return 0;                    //��������� ������ ����������
}

long WINAPI WndProc(HWND hWnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
  switch(iMsg)
  {
    case WM_DESTROY:              //���� �������� ��������� � ���������� ����
    {
      g_bApplicationState = false;      //������������� ��������� ���������� � false (��� ������ ��� ���� ��������� ��������� ������������)
      return 0;                //������� ������� ��� �� ��� ��������� ����������
    }
  }
  return DefWindowProc(hWnd,iMsg,wParam,lParam);  //���� ���� ��� ��� ������ ���������, ����� ��� ������������ �������
}

