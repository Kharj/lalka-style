#include "stdafx.h"

bool InitDirect3D(D3DFORMAT ColorFormat,D3DFORMAT DepthFormat)
{
	if((g_pDirect3D = Direct3DCreate9(D3D_SDK_VERSION)) == NULL)//������� ��������� Direct3D
		return false;											//����� ���������� false

	D3DPRESENT_PARAMETERS PresParam;				//��������� � ������� ������� �������� ���������� ���������� ���������� ��� ��� ��������
	ZeroMemory(&PresParam,sizeof(PresParam));		 //��������

	HRESULT hr = NULL;                //������� ���������� ��� ������ � �� ����������� ������ �������

	D3DDISPLAYMODE DisplayMode;            //��������� ��� ��������� ���������� � ������ ����������� � �������
	hr = g_pDirect3D->GetAdapterDisplayMode(    //�������� ����� �����������
		D3DADAPTER_DEFAULT,              //���������� ��������� ����������
		&DisplayMode);                //���������� ����� ����������� � DisplayMode

	if(FAILED(hr))                  //���� �� ����������
		return false;                //���������� false

	PresParam.hDeviceWindow = g_hWnd;        //���������� ����
	PresParam.Windowed = true;            //������� �����?
	PresParam.BackBufferWidth = g_iWindowWidth;    //������ ������� ������
	PresParam.BackBufferHeight = g_iWindowHeight;  //������ ������� ������
	PresParam.BackBufferCount = 1;          //���������� ������ �������
	PresParam.EnableAutoDepthStencil = true;    //���������� ����� ������� � ������� �����
	PresParam.AutoDepthStencilFormat = DepthFormat;  //������ ������ �������
	PresParam.SwapEffect = D3DSWAPEFFECT_FLIP;    //����� ����� ������
	PresParam.BackBufferFormat = DisplayMode.Format;//������������� ������ ������� ����������� � �������

	hr = g_pDirect3D->CreateDevice(        //������� ���������� ����������
		D3DADAPTER_DEFAULT,              //���������� ��������� ����������
		D3DDEVTYPE_HAL,                //���������� ���������� ���������� ����������� ����������
		g_hWnd,                    //���������� ����
		D3DCREATE_HARDWARE_VERTEXPROCESSING,    //������������ �������� �����������
		&PresParam,                  //������ ��������� ����������
		&g_pDirect3DDevice);            //������� ���������� ����������

	if(SUCCEEDED(hr))                //���� ���������� 
		return true;                //���������� true

	hr = g_pDirect3D->CreateDevice(          //������� ���������� ����������
		D3DADAPTER_DEFAULT,              //���������� ��������� ����������
		D3DDEVTYPE_HAL,                //���������� ���������� ���������� ����������� ����������
		g_hWnd,                    //���������� ����
		D3DCREATE_MIXED_VERTEXPROCESSING,      //������������ �������� ������� (����������� � �����������)
		&PresParam,                  //������ ��������� ����������
		&g_pDirect3DDevice);            //������� ���������� ����������

	if(SUCCEEDED(hr))                //���� ����������
		return true;                //���������� true

	hr = g_pDirect3D->CreateDevice(          //������� ���������� ����������
		D3DADAPTER_DEFAULT,              //���������� ��������� ����������
		D3DDEVTYPE_HAL,                //���������� ���������� ���������� ����������� ����������
		g_hWnd,                    //���������� ����
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,    //������������ �������� ����������
		&PresParam,                  //������ ��������� ����������
		&g_pDirect3DDevice);            //������� ���������� ����������

	if(SUCCEEDED(hr))                //���� ����������
		return true;                //���������� true

	return false;                  //���������� false
}

void DrawFrame()
{
	HRESULT hr = g_pDirect3DDevice->TestCooperativeLevel();//��������� ������� �� Direct3DDevice ����������

	if(hr == D3DERR_DEVICELOST)            //���� �� ��
		return;                   //������ �� �������  

	g_pDirect3DDevice->Clear(            //������� ������ �����
		0L,                     //������ ������, 0 - ���� �����
		NULL,                     //������� ������� ����� �������, NULL - ���� �����
		D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,     //������ ������ ����� � ����� �������
		D3DCOLOR_XRGB(0,0,0),            //���� � ������� ������� ������ �����, � ����� ������ ������
		1.0f,                     //������� ����� �������, �������� ��� ���������
		0L);                    //���� ��������� ������������� ��� ��� �� ��������� �������������� ����

	g_pDirect3DDevice->BeginScene();        //������ �����
	g_pDirect3DDevice->EndScene();          //����� �����
	g_pDirect3DDevice->Present(NULL,NULL,NULL,NULL);//���������� ���� ������ �����
}

void Shutdown()
{
	if(g_pDirect3DDevice != NULL)          //���� �� ��� �� ���������� ��������� ����������
	{
		g_pDirect3DDevice->Release();        //�� ����������� ���
		g_pDirect3DDevice = NULL;          //� ������������� � ����
	}

	if(g_pDirect3D != NULL)              //���� �� ��� �� ���������� ��������� d3d
	{
		g_pDirect3D->Release();            //�� ����������� ���
		g_pDirect3D = NULL;              //� ������������� � ����
	}

	if(!DestroyWindow(g_hWnd))            //���� �� ���������� ��������� ����
		g_hWnd = NULL;                //������������� ���������� ���� � ����

	if(!UnregisterClass(L"Lesson 1",g_hInstance))  //���� �� ���������� ������� ���� ����������������� ����
		g_hInstance = NULL;              //������������� ���������� ���������� � ����
}