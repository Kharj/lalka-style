#pragma once
#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <D3D9.h>
#include <D3DX9Shader.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

HINSTANCE g_hInstance = NULL;      //���������� ����������
HWND g_hWnd = NULL;            //���������� ����
int g_iWindowWidth = 800;        //������ ����
int g_iWindowHeight = 600;        //������ ����
bool g_bApplicationState = true;    //��������� ���������� (true - ��������/false - �� ��������)
IDirect3D9 *g_pDirect3D = NULL;      //��������� ��� �������� ���������� ����������
IDirect3DDevice9 *g_pDirect3DDevice = NULL;  //��������� ���������� ����������
